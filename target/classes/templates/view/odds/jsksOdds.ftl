<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">
    <link href="${ctx}/static/css/oddsbet.css" rel="stylesheet">
    <#--<link href="${ctx}/static/css/table.css" rel="stylesheet">-->
    <link href="${ctx}/static/css/modal.css" rel="stylesheet">
    <link href="${ctx}/static/css/balls.css" rel="stylesheet">
    <link href="${ctx}/static/css/g_PCEGG.css?v=0106" rel="stylesheet">
</head>

<body class="fixed-sidebar">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
            <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <ol class="breadcrumb">
                        <li>
                            <a href="${ctx}/static/index.html">赔率设置</a>
                        </li>
                        <li class="active">
                            重庆时时彩
                        </li>

                    </ol>
                </div>
            </div>

            <div id="bet_panel" class="bet_panel input_panel">

                <table class="table_ball" style="width: 46%" id="one">
                    <tbody><tr class="head"><th colspan="12">三军、大小</th></tr>
                    <tr>
                        <th class="G3G_1 name ballname" id="rule1" title="三军 1/鱼"><span class="b1">1</span></th>
                        <td class="G3G_1 odds ballodds" id="od1" lang="3"></td>
                        <td class="G3G_1 amount ha" ><input id="odds1" class="ba"></td>

                        <th class="G3G_2 name ballname" id="rule2" title="三军 2/虾"><span class="b2">2</span></th>
                        <td class="G3G_2 odds ballodds" id="od2" lang="3"></td>
                        <td class="G3G_2 amount ha" ><input id="odds2" class="ba"></td>

                        <th class="G3G_3 name ballname" id="rule3" title="三军 3/葫芦"><span class="b3">3</span></th>
                        <td class="G3G_3 odds ballodds" id="od3" lang="3"></td>
                        <td class="G3G_3 amount ha" ><input id="odds3" class="ba"></td>

                        <th class="GDX_D name" id="rule4" title=" 大">大</th>
                        <td class="GDX_D odds" id="od4"></td>
                        <td class="GDX_D amount ha"><input id="odds4" class="ba" ></td>
                    </tr>
                    <tr>
                        <th class="G3G_4 name ballname" id="rule5" title="三军 4/金钱" name="三军"><span class="b4" name="三军">4</span></th>
                        <td class="G3G_4 odds ballodds" id="od5" lang="3"></td>
                        <td class="G3G_4 amount ha" ><input id="odds5" class="ba"></td>

                        <th class="G3G_5 name ballname" id="rule6" title="三军 5/螃蟹" name="三军"><span class="b5"  name="三军">5</span></th>
                        <td class="G3G_5 odds ballodds" id="od6" lang="3"></td>
                        <td class="G3G_5 amount ha" ><input id="odds6" class="ba"></td>

                        <th class="G3G_6 name ballname" id="rule7" title="三军 6/鷄"  name="三军"><span class="b6"  name="三军">6</span></th>
                        <td class="G3G_6 odds ballodds" id="od7" lang="3"></td>
                        <td class="G3G_6 amount ha" ><input id="odds7" class="ba"></td>

                        <th class="GDX_X name" id="rule8" title=" 小">小</th>
                        <td class="GDX_X odds" id="od8"></td>
                        <td class="GDX_X amount ha"><input id="odds8" class="ba"></td>
                    </tr>
                    </tbody>
                </table>
                <br>
                <table class="table_ball table_ts table_ts_dpqs" style="width:46%" id="two">
                    <tbody><tr class="head"><th colspan="9">围骰、全骰</th></tr>
                    <tr>
                        <th class="GWS_111 name tsname" id="rule9" title="短牌 111"><span class="b1">1,</span><span class="b1">1,</span><span class="b1">1</span></th>
                        <td class="GWS_111 odds tsodds" id="od9">168</td>
                        <td class="GWS_111 amount ha"><input id="odds9" class="ba"></td>

                        <th class="GWS_222 name tsname" id="rule10" title="短牌 222"><span class="b2">2,</span><span class="b2">2,</span><span class="b2">2</span></th>
                        <td class="GWS_222 odds tsodds" id="od10">168</td>
                        <td class="GWS_222 amount ha"><input id="odds10" class="ba"></td>

                        <th class="GWS_333 name tsname" id="rule11" title="短牌 333"><span class="b3">3,</span><span class="b3">3,</span><span class="b3">3</span></th>
                        <td class="GWS_333 odds tsodds" id="od11">168</td>
                        <td class="GWS_333 amount ha"><input id="odds11" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GWS_444 name tsname" id="rule12" title="短牌 444"><span class="b4">4,</span><span class="b4">4,</span><span class="b4">4</span></th>
                        <td class="GWS_444 odds tsodds" id="od12">168</td>
                        <td class="GWS_444 amount ha"><input id="odds12" class="ba"></td>

                        <th class="GWS_555 name tsname" id="rule13" title="短牌 555"><span class="b5">5,</span><span class="b5">5,</span><span class="b5">5</span></th>
                        <td class="GWS_555 odds tsodds" id="od13">168</td>
                        <td class="GWS_555 amount ha"><input id="odds13" class="ba"></td>

                        <th class="GWS_666 name tsname" id="rule14" title="短牌 666"><span class="b6">6,</span><span class="b6">6,</span><span class="b6">6</span></th>
                        <td class="GWS_666 odds tsodds" id="od14"></td>
                        <td class="GWS_666 amount ha"><input id="odds14" class="ba"></td>
                    </tr>

                    <tr>
                        <th class="GQS_0 name" id="rule15" title=" 全骰">全骰</th>
                        <td class="GQS_0 odds" id="od15"></td>
                        <td class="GQS_0 amount ha"><input id="odds15" class="ba"></td>

                        <th id="e_EP0" class="GEP0 name empty"></th><td class="GEP0 odds empty"></td><td class="GEP0 amount ha"></td>
                        <th id="e_EP1" class="GEP1 name empty"></th><td class="GEP1 odds empty"></td><td class="GEP1 amount ha"></td>
                    </tr>
                    </tbody></table>
                <br>
                <table class="table_ds"  style="width:46%" id="three">
                    <tbody><tr class="head"><th colspan="12">点数</th></tr>
                    <tr>
                        <th class="GDS_4 name" id="rule16" title="点数 4点">4</th>
                        <td class="GDS_4 odds" id="od16"></td>
                        <td class="GDS_4 amount ha"><input id="odds16" class="ba"></td>

                        <th class="GDS_5 name" id="rule17" title="点数 5点">5</th>
                        <td class="GDS_5 odds" id="od17"></td>
                        <td class="GDS_5 amount ha"><input id="odds17" class="ba"></td>

                        <th class="GDS_6 name" id="rule18" title="点数 6点">6</th>
                        <td class="GDS_6 odds" id="od18"></td>
                        <td class="GDS_6 amount ha"><input id="odds18" class="ba"></td>

                        <th class="GDS_7 name" id="rule19" title="点数 7点">7</th>
                        <td class="GDS_7 odds" id="od19"></td>
                        <td class="GDS_7 amount ha"><input id="odds19" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS_8 name" id="rule20" title="点数 8点">8</th>
                        <td class="GDS_8 odds" id="od20"></td>
                        <td class="GDS_8 amount ha"><input id="odds20" class="ba"></td>

                        <th class="GDS_9 name" id="rule21" title="点数 9点">9</th>
                        <td class="GDS_9 odds" id="od21"></td>
                        <td class="GDS_9 amount ha"><input id="odds21" class="ba"></td>

                        <th class="GDS_10 name" id="rule22" title="点数 10点">10</th>
                        <td class="GDS_10 odds" id="od22"></td>
                        <td class="GDS_10 amount ha"><input id="odds22" class="ba"></td>

                        <th class="GDS_11 name" id="rule23" title="点数 11点">11</th>
                        <td class="GDS_11 odds" id="od23"></td>
                        <td class="GDS_11 amount ha"><input id="odds23" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS_12 name" id="rule24" title="点数 12点">12</th>
                        <td class="GDS_12 odds" id="od24"></td>
                        <td class="GDS_12 amount ha"><input id="odds24" class="ba"></td>

                        <th class="GDS_13 name" id="rule25" title="点数 13点">13</th>
                        <td class="GDS_13 odds" id="od25"></td>
                        <td class="GDS_13 amount ha"><input id="odds25" class="ba"></td>

                        <th class="GDS_14 name" id="rule26" title="点数 14点">14</th>
                        <td class="GDS_14 odds" id="od26"></td>
                        <td class="GDS_14 amount ha"><input id="odds26" class="ba"></td>

                        <th class="GDS_15 name" id="rule27" title="点数 15点">15</th>
                        <td class="GDS_15 odds" id="od27"></td>
                        <td class="GDS_15 amount ha"><input id="odds27" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS_16 name" id="rule28" title="点数 16点">16</th>
                        <td class="GDS_16 odds" id="od28"></td>
                        <td class="GDS_16 amount ha"><input id="odds28" class="ba"></td>

                        <th class="GDS_17 name" id="rule29" title="点数 17点">17</th>
                        <td class="GDS_17 odds" id="od29"></td>
                        <td class="GDS_17 amount ha"><input id="odds29" class="ba"></td>

                        <th id="e_EP2" class="GEP2 name empty"></th><td class="GEP2 odds empty"></td><td class="GEP2 amount ha"></td>
                        <th id="e_EP3" class="GEP3 name empty"></th><td class="GEP3 odds empty"></td><td class="GEP3 amount ha"></td>
                    </tr>
                    </tbody></table>
                <br>
                <table class="table_ball table_ts table_ts1"  style="width:46%"  id="four">
                    <tbody><tr class="head"><th colspan="9">长牌</th></tr>
                    <tr>
                        <th class="GCP_12 name tsname" id="rule30" title="长牌 12"><div><span class="b1">1,</span><span class="b2">2</span></div></th>
                        <td class="GCP_12 odds tsodds" id="od30"></td>
                        <td class="GCP_12 amount ha"><input id="odds30" class="ba"></td>

                        <th class="GCP_13 name tsname" id="rule31" title="长牌 13"><div><span class="b1">1,</span><span class="b3">3</span></div></th>
                        <td class="GCP_13 odds tsodds" id="od31"></td>
                        <td class="GCP_13 amount ha"><input id="odds31" class="ba"></td>

                        <th class="GCP_14 name tsname" id="rule32" title="长牌 14"><div><span class="b1">1,</span><span class="b4">4</span></div></th>
                        <td class="GCP_14 odds tsodds" id="od32"></td>
                        <td class="GCP_14 amount ha"><input id="odds32" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GCP_15 name tsname" id="rule33" title="长牌 15"><div><span class="b1">1,</span><span class="b5">5</span></div></th>
                        <td class="GCP_15 odds tsodds" id="od33"></td>
                        <td class="GCP_15 amount ha"><input id="odds33" class="ba"></td>

                        <th class="GCP_16 name tsname" id="rule34" title="长牌 16"><div><span class="b1">1,</span><span class="b6">6</span></div></th>
                        <td class="GCP_16 odds tsodds" id="od34"></td>
                        <td class="GCP_16 amount ha"><input id="odds34" class="ba"></td>

                        <th class="GCP_23 name tsname" id="rule35" title="长牌 23"><div><span class="b2">2,</span><span class="b3">3</span></div></th>
                        <td class="GCP_23 odds tsodds" id="od35"></td>
                        <td class="GCP_23 amount ha"><input id="odds35" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GCP_24 name tsname" id="rule36" title="长牌 24"><div><span class="b2">2,</span><span class="b4">4</span></div></th>
                        <td class="GCP_24 odds tsodds" id="od36"></td>
                        <td class="GCP_24 amount ha"><input id="odds36" class="ba"></td>

                        <th class="GCP_25 name tsname" id="rule37" title="长牌 25"><div><span class="b2">2,</span><span class="b5">5</span></div></th>
                        <td class="GCP_25 odds tsodds" id="od37"></td>
                        <td class="GCP_25 amount ha"><input id="odds37" class="ba"></td>

                        <th class="GCP_26 name tsname" id="rule38" title="长牌 26"><div><span class="b2">2,</span><span class="b6">6</span></div></th>
                        <td class="GCP_26 odds tsodds" id="od38"></td>
                        <td class="GCP_26 amount ha"><input id="odds38" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GCP_34 name tsname" id="rule39" title="长牌 34"><div><span class="b3">3,</span><span class="b4">4</span></div></th>
                        <td class="GCP_34 odds tsodds" id="od39"></td>
                        <td class="GCP_34 amount ha"><input id="odds39" class="ba"></td>

                        <th class="GCP_35 name tsname" id="rule40" title="长牌 35"><div><span class="b3">3,</span><span class="b5">5</span></div></th>
                        <td class="GCP_35 odds tsodds" id="od40"></td>
                        <td class="GCP_35 amount ha"><input id="odds40" class="ba"></td>

                        <th class="GCP_36 name tsname" id="rule41" title="长牌 36"><div><span class="b3">3,</span><span class="b6">6</span></div></th>
                        <td class="GCP_36 odds tsodds" id="od41"></td>
                        <td class="GCP_36 amount ha"><input id="odds41" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GCP_45 name tsname" id="rule42" title="长牌 45"><div><span class="b4">4,</span><span class="b5">5</span></div></th>
                        <td class="GCP_45 odds tsodds" id="od42"></td>
                        <td class="GCP_45 amount ha"><input id="odds42" class="ba"></td>

                        <th class="GCP_46 name tsname" id="rule43" title="长牌 46"><div><span class="b4">4,</span><span class="b6">6</span></div></th>
                        <td class="GCP_46 odds tsodds" id="od43"></td>
                        <td class="GCP_46 amount ha"><input id="odds43" class="ba"></td>

                        <th class="GCP_56 name tsname" id="rule44" title="长牌 56"><div><span class="b5">5,</span><span class="b6">6</span></div></th>
                        <td class="GCP_56 odds tsodds" id="od44"></td>
                        <td class="GCP_56 amount ha"><input id="odds44" class="ba"></td>
                    </tr>
                    </tbody></table>
                <br>
                <table class="table_ball table_ts table_ts1"  style="width:46%" id="five">
                    <tbody><tr class="head"><th colspan="9">短牌</th></tr>
                    <tr>
                        <th class="GDP_11 name tsname" id="rule45" title="短牌 11"><div><span class="b1">1,</span><span class="b1">1</span></div></th>
                        <td class="GDP_11 odds tsodds" id="od45"></td>
                        <td class="GDP_11 amount ha"><input id="odds45" class="ba"></td>

                        <th class="GDP_22 name tsname" id="rule46" title="短牌 22"><div><span class="b2">2,</span><span class="b2">2</span></div></th>
                        <td class="GDP_22 odds tsodds" id="od46"></td>
                        <td class="GDP_22 amount ha"><input id="odds46" class="ba"></td>

                        <th class="GDP_33 name tsname" id="rule47" title="短牌 33"><div><span class="b3">3,</span><span class="b3">3</span></div></th>
                        <td class="GDP_33 odds tsodds" id="od47"></td>
                        <td class="GDP_33 amount ha"><input id="odds47" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDP_44 name tsname" id="rule48" title="短牌 44"><div><span class="b4">4,</span><span class="b4">4</span></div></th>
                        <td class="GDP_44 odds tsodds" id="od48"></td>
                        <td class="GDP_44 amount ha"><input id="odds48" class="ba"></td>

                        <th class="GDP_55 name tsname" id="rule49" title="短牌 55"><div><span class="b5">5,</span><span class="b5">5</span></div></th>
                        <td class="GDP_55 odds tsodds" id="od49"></td>
                        <td class="GDP_55 amount ha"><input id="odds49" class="ba"></td>

                        <th class="GDP_66 name tsname" id="rule50" title="短牌 66"><div><span class="b6">6,</span><span class="b6">6</span></div></th>
                        <td class="GDP_66 odds tsodds" id="od50"></td>
                        <td class="GDP_66 amount ha"><input id="odds50" class="ba"></td>
                    </tr>
                    </tbody>
                </table>


                <div class="control bcontrol">
                <div class="buttons">
                    <a class="sel_btn" onclick="bet()" href="javascript:" style="float: left;margin-left: 20%">提交</a>
                    <a class="sel_btn" href="/odds/JSKSOddsSet?lotteryId=4" style="float: left;margin-left: 1%">重置/刷新</a>
                </div>
                </div>


            </div>

            <div class="footer">
                <div class="pull-right">
                    一只 <strong>咸鱼</strong>
                </div>
                <div>
                    <strong>作者:Eric</strong>  版权所有 &copy; 2017-未来
                </div>
            </div>
        </div>

    </div>

    <script src="${ctx}/static/js/jquery-2.1.1.js"></script>
    <script src="${ctx}/static/js/bootstrap.js"></script>
    <script src="${ctx}/static/js/wuling.js"></script>
    <script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>

    <script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <!-- 插件 scripts -->
    <script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script><!---顶部弹出提示--->
    <script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>  <!---表单验证--->
    <script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script> <!---validate 自定义方法--->
    <script>

        $(document).ready(function(){

            console.log(${json});
            var json=${json};
            for(var i=0;i<json.length;i++){
                for(var j=0;j<52;j++) {
                    var id = "rule" + j;
                    var tile = $("#" + id + "").children("span").text();
                    var text = $("#" + id + "").text();
                    var tt = $("#" + id + "").children("div").text();

                    if (json[i].rule == tile || json[i].rule == text || json[i].rule == tt) {
                        if (json[i].name == "三军" && json[i].rule == 4) {

                            $("#od5").text(json[i].odds);
                            $("#odds5").val(json[i].odds);
                        } else if (json[i].name == "三军" && json[i].rule == 5) {
                            $("#od6").text(json[i].odds);
                            $("#odds6").val(json[i].odds);

                        } else if (json[i].name == "三军" && json[i].rule == 6) {
                            $("#od7").text(json[i].odds);
                            $("#odds7").val(json[i].odds);

                        } else if (json[i].name == "点数" && json[i].rule == 4) {
                            $("#od16").text(json[i].odds);
                            $("#odds16").val(json[i].odds);
                        } else if (json[i].name == "点数" && json[i].rule == 5) {
                            $("#od17").text(json[i].odds);
                            $("#odds17").val(json[i].odds);

                        } else if (json[i].name == "点数" && json[i].rule == 6) {
                            $("#od18").text(json[i].odds);
                            $("#odds18").val(json[i].odds);

                        } else {
                            $("#od" + j + "").text(json[i].odds);
                            $("#odds" + j + "").val(json[i].odds);

                        }


                    }

                    /*  }*/


                }
            }
        });




        function bet(){

            //把table的数据转换成json格式
            var one = document.getElementById("one");
            var two = document.getElementById("two");
            var three = document.getElementById("three");
            var four = document.getElementById("four");
            var five = document.getElementById("five");

            var jsonT = "[";
            for (var i = 1; i < one.rows.length; i++) {
                if(i=2){ //三军的tr节点
                    jsonT += '{"rule":"' + one.rows[i].cells[0].innerText + '","odds":"' + one.rows[i].cells[2].firstChild.value + '","lid":4,"balls":"1,2,3",name:"三军"},'
                    jsonT += '{"rule":"' + one.rows[i].cells[3].innerText + '","odds":"' + one.rows[i].cells[5].firstChild.value + '","lid":4,"balls":"1,2,3",name:"三军"},'
                    jsonT += '{"rule":"' + one.rows[i].cells[6].innerText + '","odds":"' + one.rows[i].cells[8].firstChild.value + '","lid":4,"balls":"1,2,3",name:"三军"},'
                    jsonT += '{"rule":"' + one.rows[i].cells[9].innerText + '","odds":"' + one.rows[i].cells[11].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'

                }else{
                    jsonT += '{"rule":"' + one.rows[i].cells[0].innerText + '","odds":"' + one.rows[i].cells[2].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                    jsonT += '{"rule":"' + one.rows[i].cells[3].innerText + '","odds":"' + one.rows[i].cells[5].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                    jsonT += '{"rule":"' + one.rows[i].cells[6].innerText + '","odds":"' + one.rows[i].cells[8].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                    jsonT += '{"rule":"' + one.rows[i].cells[9].innerText + '","odds":"' + one.rows[i].cells[11].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'

                }

            }

            for (var i = 1; i < two.rows.length; i++) {
                if(i==3){
                    jsonT += '{"rule":"' + two.rows[i].cells[0].innerText + '","odds":"' + two.rows[i].cells[2].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                }else {
                    jsonT += '{"rule":"' + two.rows[i].cells[0].innerText + '","odds":"' + two.rows[i].cells[2].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                    jsonT += '{"rule":"' + two.rows[i].cells[3].innerText + '","odds":"' + two.rows[i].cells[5].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                    jsonT += '{"rule":"' + two.rows[i].cells[6].innerText + '","odds":"' + two.rows[i].cells[8].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                }
            }

            for (var i = 1; i < three.rows.length; i++) {
                if(i==4){
                    jsonT += '{"rule":"' + three.rows[i].cells[0].innerText + '","odds":"' + three.rows[i].cells[2].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                    jsonT += '{"rule":"' + three.rows[i].cells[3].innerText + '","odds":"' + three.rows[i].cells[5].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                }else if(i==1){  //点数4，5，6
                    jsonT += '{"rule":"' + three.rows[i].cells[0].innerText + '","odds":"' + three.rows[i].cells[2].firstChild.value + '","lid":4,"balls":"1,2,3",name:"点数"},'
                    jsonT += '{"rule":"' + three.rows[i].cells[3].innerText + '","odds":"' + three.rows[i].cells[5].firstChild.value + '","lid":4,"balls":"1,2,3",name:"点数"},'
                    jsonT += '{"rule":"' + three.rows[i].cells[6].innerText + '","odds":"' + three.rows[i].cells[8].firstChild.value + '","lid":4,"balls":"1,2,3",name:"点数"},'
                    jsonT += '{"rule":"' + three.rows[i].cells[9].innerText + '","odds":"' + three.rows[i].cells[11].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'

                }else {
                    jsonT += '{"rule":"' + three.rows[i].cells[0].innerText + '","odds":"' + three.rows[i].cells[2].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                    jsonT += '{"rule":"' + three.rows[i].cells[3].innerText + '","odds":"' + three.rows[i].cells[5].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                    jsonT += '{"rule":"' + three.rows[i].cells[6].innerText + '","odds":"' + three.rows[i].cells[8].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                    jsonT += '{"rule":"' + three.rows[i].cells[9].innerText + '","odds":"' + three.rows[i].cells[11].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                }
            }

            for (var i = 1; i < four.rows.length; i++) {

                jsonT += '{"rule":"' + four.rows[i].cells[0].innerText + '","odds":"' + four.rows[i].cells[2].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                jsonT += '{"rule":"' + four.rows[i].cells[3].innerText + '","odds":"' + four.rows[i].cells[5].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                jsonT += '{"rule":"' + four.rows[i].cells[6].innerText + '","odds":"' + four.rows[i].cells[8].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
            }

            for (var i = 1; i < five.rows.length; i++) {

                jsonT += '{"rule":"' + five.rows[i].cells[0].innerText + '","odds":"' + five.rows[i].cells[2].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                jsonT += '{"rule":"' + five.rows[i].cells[3].innerText + '","odds":"' + five.rows[i].cells[5].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'
                jsonT += '{"rule":"' + five.rows[i].cells[6].innerText + '","odds":"' + five.rows[i].cells[8].firstChild.value + '","lid":4,"balls":"1,2,3",name:""},'


            }



            jsonT= jsonT.substr(0, jsonT.length - 1);

            jsonT += "]";
            console.log(jsonT);
            console.log(jsonT.length);
            $.ajax({
                type: 'post',
                url: '/odds/updateOdds',
                data:{"jsonStr":JSON.stringify(jsonT)},
                success:function(response) {

                    if (response.status==200) {
                        $.dialog.tips("更新成功！");

                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    } else {
                        $.dialog.error(response.message);
                    }
                },
                error:function(response){
                    alert("---error:"+response);
                    $.dialog.error(response.message);
                }
            });

        }

    </script>

    <style>
        .title {
            background: #f9f9f9 url(../../img/bg-content-title.png);
            border-color: #fbfbfb !important;
            color: #111113;
            width: 20%;
        }

        .sel_btn {
            height: 25px;
            line-height: 25px;
            padding: 0 11px;
            background: #a1a9b1;
            border: 1px #abb8bb solid;
            border-radius: 3px;
            /* color: #fff; */
            display: inline-block;
            text-decoration: none;
            font-size: 14px;
            outline: none;
        }
    </style>

</body>
</html>
