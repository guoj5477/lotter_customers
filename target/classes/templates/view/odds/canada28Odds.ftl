<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">
    <link href="${ctx}/static/css/oddsbet.css" rel="stylesheet">
<#--<link href="${ctx}/static/css/table.css" rel="stylesheet">-->
    <link href="${ctx}/static/css/modal.css" rel="stylesheet">
    <link href="${ctx}/static/css/balls.css" rel="stylesheet">
    <link href="${ctx}/static/css/g_PCEGG.css?v=0106" rel="stylesheet">



</head>

<body class="fixed-sidebar">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
            <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <ol class="breadcrumb">
                        <li>
                            <a href="${ctx}/static/index.html">赔率设置</a>
                        </li>
                        <li class="active">
                            加拿大28
                        </li>

                    </ol>
                </div>
            </div>

            <form method="post" action="/odds/submitOdds" id="formId" class="form-inline">
                <div id="bet_panel" class="bet_panel input_panel" style="padding:0px">
                    <table class="table_lm" style="width:1000px;margin:0px 0px 0px 0px">
                        <tbody><tr class="head"><th>特&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;码</th></tr>
                        </tbody></table>
                    <div class="split_panel" style="margin:0px 0px 0px 0px">
                        <table><tbody>
                        <#list list as oddBean>
                            <#if oddBean.rule == "0">
                                <tr><th class="GHZ_0 name" id="t_HZ_0" title=" 0" width="70"><input type="hidden" id="k_HZ_0" value="HZ"><span class="b0">0</span></th>

                                    <td class="GHZ_0 odds" id="o_HZ_0" width="70">#{oddBean.odds}</td>
                                    <td class="GHZ_0 amount ha" width="110"><input name="HZ_0" class="ba"></td>

                                </tr>
                            </#if>
                            <#if oddBean.rule == "1">
                                <tr><th class="GHZ_1 name" id="t_HZ_1" title=" 1"><input type="hidden" id="k_HZ_1" value="HZ"><span class="b1">1</span></th>

                                    <td class="GHZ_1 odds" id="o_HZ_1">#{oddBean.odds}</td>
                                    <td class="GHZ_1 amount ha"><input name="HZ_1" class="ba"></td>

                                </tr>
                            </#if>
                            <#if oddBean.rule == "2">
                                <tr><th class="GHZ_2 name" id="t_HZ_2" title=" 2"><input type="hidden" id="k_HZ_2" value="HZ"><span class="b2">2</span></th>

                                    <td class="GHZ_2 odds" id="o_HZ_2">#{oddBean.odds}</td>
                                    <td class="GHZ_2 amount ha"><input name="HZ_2" class="ba"></td>

                                </tr>
                            </#if>
                            <#if oddBean.rule == "3">
                                <tr><th class="GHZ_3 name" id="t_HZ_3" title=" 3"><input type="hidden" id="k_HZ_3" value="HZ"><span class="b3">3</span></th>

                                    <td class="GHZ_3 odds" id="o_HZ_3">#{oddBean.odds}</td>
                                    <td class="GHZ_3 amount ha"><input name="HZ_3" class="ba"></td>

                                </tr>
                            </#if>
                            <#if oddBean.rule == "4">
                                <tr><th class="GHZ_4 name" id="t_HZ_4" title=" 4"><input type="hidden" id="k_HZ_4" value="HZ"><span class="b4">4</span></th>

                                    <td class="GHZ_4 odds" id="o_HZ_4">#{oddBean.odds}</td>
                                    <td class="GHZ_4 amount ha"><input name="HZ_4" class="ba"></td>

                                </tr>
                            </#if>
                            <#if oddBean.rule == "5">
                                <tr><th class="GHZ_5 name" id="t_HZ_5" title=" 5"><input type="hidden" id="k_HZ_5" value="HZ"><span class="b5">5</span></th>

                                    <td class="GHZ_5 odds" id="o_HZ_5">#{oddBean.odds}</td>
                                    <td class="GHZ_5 amount ha"><input name="HZ_5" class="ba"></td>

                                </tr>
                            </#if>
                            <#if oddBean.rule == "6">
                                <tr><th class="GHZ_6 name" id="t_HZ_6" title=" 6"><input type="hidden" id="k_HZ_6" value="HZ"><span class="b6">6</span></th>

                                    <td class="GHZ_6 odds" id="o_HZ_6">#{oddBean.odds}</td>
                                    <td class="GHZ_6 amount ha"><input name="HZ_6" class="ba"></td>

                                </tr>
                            </#if>
                        </#list>
                        </tbody></table>
                        <table><tbody>
                            <#list list as oddBean>
                                <#if oddBean.rule == "7">
                                    <tr><th class="GHZ_7 name" id="t_HZ_7" title=" 7" width="70"><input type="hidden" id="k_HZ_7" value="HZ"><span class="b7">7</span></th>

                                        <td class="GHZ_7 odds" id="o_HZ_7" width="70">#{oddBean.odds}</td>
                                        <td class="GHZ_7 amount ha" width="110"><input name="HZ_7" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "8">
                                    <tr><th class="GHZ_8 name" id="t_HZ_8" title=" 8"><input type="hidden" id="k_HZ_8" value="HZ"><span class="b8">8</span></th>

                                        <td class="GHZ_8 odds" id="o_HZ_8">#{oddBean.odds}</td>
                                        <td class="GHZ_8 amount ha"><input name="HZ_8" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "9">
                                    <tr><th class="GHZ_9 name" id="t_HZ_9" title=" 9"><input type="hidden" id="k_HZ_9" value="HZ"><span class="b9">9</span></th>

                                        <td class="GHZ_9 odds" id="o_HZ_9">#{oddBean.odds}</td>
                                        <td class="GHZ_9 amount ha"><input name="HZ_9" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "10">
                                    <tr><th class="GHZ_10 name" id="t_HZ_10" title=" 10"><input type="hidden" id="k_HZ_10" value="HZ"><span class="b10">10</span></th>

                                        <td class="GHZ_10 odds" id="o_HZ_10">#{oddBean.odds}</td>
                                        <td class="GHZ_10 amount ha"><input name="HZ_10" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "11">
                                    <tr><th class="GHZ_11 name" id="t_HZ_11" title=" 11"><input type="hidden" id="k_HZ_11" value="HZ"><span class="b11">11</span></th>

                                        <td class="GHZ_11 odds" id="o_HZ_11">#{oddBean.odds}</td>
                                        <td class="GHZ_11 amount ha"><input name="HZ_11" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "12">
                                    <tr><th class="GHZ_12 name" id="t_HZ_12" title=" 12"><input type="hidden" id="k_HZ_12" value="HZ"><span class="b12">12</span></th>

                                        <td class="GHZ_12 odds" id="o_HZ_12">#{oddBean.odds}</td>
                                        <td class="GHZ_12 amount ha"><input name="HZ_12" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "13">
                                    <tr><th class="GHZ_13 name" id="t_HZ_13" title=" 13"><input type="hidden" id="k_HZ_13" value="HZ"><span class="b13">13</span></th>

                                        <td class="GHZ_13 odds" id="o_HZ_13">#{oddBean.odds}</td>
                                        <td class="GHZ_13 amount ha"><input name="HZ_13" class="ba"></td>

                                    </tr>
                                </#if>

                            </#list>
                        </tbody></table>
                        <table><tbody>
                            <#list list as oddBean>
                                <#if oddBean.rule == "14">
                                    <tr><th class="GHZ_14 name" id="t_HZ_14" title=" 14" width="70"><input type="hidden" id="k_HZ_14" value="HZ"><span class="b14">14</span></th>

                                        <td class="GHZ_14 odds" id="o_HZ_14" width="70">#{oddBean.odds}</td>
                                        <td class="GHZ_14 amount ha" width="110"><input name="HZ_14" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "15">
                                    <tr><th class="GHZ_15 name" id="t_HZ_15" title=" 15"><input type="hidden" id="k_HZ_15" value="HZ"><span class="b15">15</span></th>

                                        <td class="GHZ_15 odds" id="o_HZ_15">#{oddBean.odds}</td>
                                        <td class="GHZ_15 amount ha"><input name="HZ_15" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "16">
                                    <tr><th class="GHZ_16 name" id="t_HZ_16" title=" 16"><input type="hidden" id="k_HZ_16" value="HZ"><span class="b16">16</span></th>

                                        <td class="GHZ_16 odds" id="o_HZ_16">#{oddBean.odds}</td>
                                        <td class="GHZ_16 amount ha"><input name="HZ_16" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "17">
                                    <tr><th class="GHZ_17 name" id="t_HZ_17" title=" 17"><input type="hidden" id="k_HZ_17" value="HZ"><span class="b17">17</span></th>

                                        <td class="GHZ_17 odds" id="o_HZ_17">#{oddBean.odds}</td>
                                        <td class="GHZ_17 amount ha"><input name="HZ_17" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "18">
                                    <tr><th class="GHZ_18 name" id="t_HZ_18" title=" 18"><input type="hidden" id="k_HZ_18" value="HZ"><span class="b18">18</span></th>

                                        <td class="GHZ_18 odds" id="o_HZ_18">#{oddBean.odds}</td>
                                        <td class="GHZ_18 amount ha"><input name="HZ_18" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "19">
                                    <tr><th class="GHZ_19 name" id="t_HZ_19" title=" 19"><input type="hidden" id="k_HZ_19" value="HZ"><span class="b19">19</span></th>

                                        <td class="GHZ_19 odds" id="o_HZ_19">#{oddBean.odds}</td>
                                        <td class="GHZ_19 amount ha"><input name="HZ_19" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "20">
                                    <tr><th class="GHZ_20 name" id="t_HZ_20" title=" 20"><input type="hidden" id="k_HZ_20" value="HZ"><span class="b20">20</span></th>

                                        <td class="GHZ_20 odds" id="o_HZ_20">#{oddBean.odds}</td>
                                        <td class="GHZ_20 amount ha"><input name="HZ_20" class="ba"></td>

                                    </tr>
                                </#if>
                            </#list>
                        </tbody></table>
                        <table><tbody>
                            <#list list as oddBean>
                                <#if oddBean.rule == "21">
                                    <tr><th class="GHZ_21 name" id="t_HZ_21" title=" 21" width="70"><input type="hidden" id="k_HZ_21" value="HZ"><span class="b21">21</span></th>

                                        <td class="GHZ_21 odds" id="o_HZ_21" width="70">#{oddBean.odds}</td>
                                        <td class="GHZ_21 amount ha" width="110"><input name="HZ_21" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "22">
                                    <tr><th class="GHZ_22 name" id="t_HZ_22" title=" 22"><input type="hidden" id="k_HZ_22" value="HZ"><span class="b22">22</span></th>

                                        <td class="GHZ_22 odds" id="o_HZ_22">#{oddBean.odds}</td>
                                        <td class="GHZ_22 amount ha"><input name="HZ_22" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "23">
                                    <tr><th class="GHZ_23 name" id="t_HZ_23" title=" 23"><input type="hidden" id="k_HZ_23" value="HZ"><span class="b23">23</span></th>

                                        <td class="GHZ_23 odds" id="o_HZ_23">#{oddBean.odds}</td>
                                        <td class="GHZ_23 amount ha"><input name="HZ_23" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "24">
                                    <tr><th class="GHZ_24 name" id="t_HZ_24" title=" 24"><input type="hidden" id="k_HZ_24" value="HZ"><span class="b24">24</span></th>

                                        <td class="GHZ_24 odds" id="o_HZ_24">#{oddBean.odds}</td>
                                        <td class="GHZ_24 amount ha"><input name="HZ_24" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "25">
                                    <tr><th class="GHZ_25 name" id="t_HZ_25" title=" 25"><input type="hidden" id="k_HZ_25" value="HZ"><span class="b25">25</span></th>

                                        <td class="GHZ_25 odds" id="o_HZ_25">#{oddBean.odds}</td>
                                        <td class="GHZ_25 amount ha"><input name="HZ_25" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "26">
                                    <tr><th class="GHZ_26 name" id="t_HZ_26" title=" 26"><input type="hidden" id="k_HZ_26" value="HZ"><span class="b26">26</span></th>

                                        <td class="GHZ_26 odds" id="o_HZ_26">#{oddBean.odds}</td>
                                        <td class="GHZ_26 amount ha"><input name="HZ_26" class="ba"></td>

                                    </tr>
                                </#if>
                                <#if oddBean.rule == "27">
                                    <tr><th class="GHZ_27 name" id="t_HZ_27" title=" 27"><input type="hidden" id="k_HZ_27" value="HZ"><span class="b27">27</span></th>

                                        <td class="GHZ_27 odds" id="o_HZ_27">#{oddBean.odds}</td>
                                        <td class="GHZ_27 amount ha"><input name="HZ_27" class="ba"></td>

                                    </tr>
                                </#if>
                            </#list>
                        </tbody></table>
                    </div>
                    <table class="table_lm" style="width:1000px;margin:0px 0px 0px 0px">
                        <tbody><tr class="head"><th colspan="16">两&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面</th></tr>
                        <tr>
                            <#list list as oddBean>
                                <#if oddBean.rule == "大">
                                    <th class="GDX_D name" id="t_DX_D" title=" 大"><input type="hidden" id="k_DX_D" value="DX">大</th>

                                    <td class="GDX_D odds" id="o_DX_D">#{oddBean.odds}</td>
                                    <td class="GDX_D amount ha"><input name="DX_D" class="ba"></td>
                                </#if>
                                <#if oddBean.rule == "单">
                                    <th class="GDS_D name" id="t_DS_D" title=" 单"><input type="hidden" id="k_DS_D" value="DX">单</th>

                                    <td class="GDS_D odds" id="o_DS_D">#{oddBean.odds}</td>
                                    <td class="GDS_D amount ha"><input name="DS_D" class="ba"></td>
                                </#if>
                                <#if oddBean.rule == "极大">
                                    <th class="GJZDX_D name" id="t_JZDX_D" title=" 极大"><input type="hidden" id="k_JZDX_D" value="JZDX">极大</th>

                                    <td class="GJZDX_D odds" id="o_JZDX_D">#{oddBean.odds}</td>
                                    <td class="GJZDX_D amount ha"><input name="JZDX_D" class="ba"></td>
                                </#if>
                                <#if oddBean.rule == "大单">
                                    <th class="GDXDS_DD name" id="t_DXDS_DD" title=" 大单"><input type="hidden" id="k_DXDS_DD" value="DXDS">大单</th>

                                    <td class="GDXDS_DD odds" id="o_DXDS_DD">#{oddBean.odds}</td>
                                    <td class="GDXDS_DD amount ha"><input name="DXDS_DD" class="ba"></td>
                                </#if>
                                <#if oddBean.rule == "大双">
                                    <th class="GDXDS_DS name" id="t_DXDS_DS" title=" 大双"><input type="hidden" id="k_DXDS_DS" value="DXDS">大双</th>

                                    <td class="GDXDS_DS odds" id="o_DXDS_DS">#{oddBean.odds}</td>
                                    <td class="GDXDS_DS amount ha"><input name="DXDS_DS" class="ba"></td>
                                </#if>
                            </#list>
                        </tr>
                        <tr>
                            <#list list as oddBean>
                                <#if oddBean.rule == "小">
                                    <th class="GDX_X name" id="t_DX_X" title=" 小"><input type="hidden" id="k_DX_X" value="DX">小</th>

                                    <td class="GDX_X odds" id="o_DX_X">#{oddBean.odds}</td>
                                    <td class="GDX_X amount ha"><input name="DX_X" class="ba"></td>
                                </#if>
                                <#if oddBean.rule == "双">
                                    <th class="GDS_S name" id="t_DS_S" title=" 双"><input type="hidden" id="k_DS_S" value="DS">双</th>

                                    <td class="GDS_S odds" id="o_DS_S">#{oddBean.odds}</td>
                                    <td class="GDS_S amount ha"><input name="DS_S" class="ba"></td>
                                </#if>
                                <#if oddBean.rule == "极小">
                                    <th class="GJZDX_X name" id="t_JZDX_X" title=" 极小"><input type="hidden" id="k_JZDX_X" value="JZDX">极小</th>

                                    <td class="GJZDX_X odds" id="o_JZDX_X">#{oddBean.odds}</td>
                                    <td class="GJZDX_X amount ha"><input name="JZDX_X" class="ba"></td>
                                </#if>
                                <#if oddBean.rule == "小单">
                                    <th class="GDXDS_XD name" id="t_DXDS_XD" title=" 小单"><input type="hidden" id="k_DXDS_XD" value="DXDS">小单</th>

                                    <td class="GDXDS_XD odds" id="o_DXDS_XD">#{oddBean.odds}</td>
                                    <td class="GDXDS_XD amount ha"><input name="DXDS_XD" class="ba"></td>
                                </#if>
                                <#if oddBean.rule == "小双">
                                    <th class="GDXDS_XS name" id="t_DXDS_XS" title=" 小双"><input type="hidden" id="k_DXDS_XS" value="DXDS">小双</th>

                                    <td class="GDXDS_XS odds" id="o_DXDS_XS">#{oddBean.odds}</td>
                                    <td class="GDXDS_XS amount ha"><input name="DXDS_XS" class="ba"></td>
                                </#if>
                            </#list>
                        </tr>
                        </tbody></table>
                    <table class="table_lm" style="width:1000px;margin: 3px 0px 0px 0px">
                        <tbody><tr class="head"><th colspan="12">色波/豹子</th></tr>
                        <tr>
                            <#list list as oddBean>
                                <#if oddBean.rule == "红波">
                                    <th class="GSB_R name redname" id="t_SB_R" title=" 红波"><input type="hidden" id="k_SB_R" value="SB">红波</th>

                                    <td class="GSB_R odds redodds" id="o_SB_R">#{oddBean.odds}</td>
                                    <td class="GSB_R amount ha"><input name="SB_R" class="ba"></td>
                                </#if>
                                <#if oddBean.rule == "绿波">
                                    <th class="GSB_G name" id="t_SB_G" title=" 绿波"><input type="hidden" id="k_SB_G" value="SB"><span class="greenname">绿波</span></th>

                                    <td class="GSB_G odds greenodds" id="o_SB_G">#{oddBean.odds}</td>
                                    <td class="GSB_G amount ha"><input name="SB_G" class="ba"></td>
                                </#if>
                                <#if oddBean.rule == "蓝波">
                                    <th class="GSB_B name" id="t_SB_B" title=" 蓝波"><input type="hidden" id="k_SB_B" value="SB"><span class="bluename">蓝波</span></th>

                                    <td class="GSB_B odds blueodds" id="o_SB_B">#{oddBean.odds}</td>
                                    <td class="GSB_B amount ha"><input name="SB_B" class="ba"></td>
                                </#if>
                                <#if oddBean.rule == "豹子">
                                    <th class="GBZ_0 name" id="t_BZ_0" title=" 豹子"><input type="hidden" id="k_BZ_0" value="BZ">豹子</th>

                                    <td class="GBZ_0 odds" id="o_BZ_0">#{oddBean.odds}</td>
                                    <td class="GBZ_0 amount ha"><input name="BZ_0" class="ba"></td>
                                </#if>
                            </#list>
                        </tr>
                        <tr>
                        </tr></tbody></table>
                </div>
                <div class="control bcontrol" style="width:1000px">
                    <input type="button" class="button" value="确定" onclick="bet()">
                </div>
            </form>
            <div class="footer">
                <div class="pull-right">
                    一只 <strong>咸鱼</strong>
                </div>
                <div>
                    <strong>作者:Eric</strong>  版权所有 &copy; 2017-未来
                </div>
            </div>
        </div>

    </div>

    <script src="${ctx}/static/js/jquery-2.1.1.js"></script>
    <script src="${ctx}/static/js/bootstrap.js"></script>
    <script src="${ctx}/static/js/wuling.js"></script>
    <script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>

    <script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <!-- 插件 scripts -->
    <script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script><!---顶部弹出提示--->
    <script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>  <!---表单验证--->
    <script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script> <!---validate 自定义方法--->
    <script src="${ctx}/static/js/jquery.dialog-4.1.6.js" ></script> <!---validate 自定义方法--->
    <script>
        function bet(){
            var oddsDatas=[];
           var inputs =  $("#formId :text");
           $.each(inputs,function(i,n){
               if($(n).val() != ""){
                   var d = {"name":$(n).attr("name"),"oddVal":$(n).val()};
                   oddsDatas.push(d);
               }
           });
           if(oddsDatas.length == 0){
               alert("没有修改的值！");
           }else{
               var params = {"jsonList": oddsDatas,"lid":10};
               $.ajax({
                   type: 'post',
                   url: '/odds/updateOdds',
                   data:{"jsonStr":JSON.stringify(params)},
                   success:function(response) {
                       if (response.status==200) {
                           alert('更新成功!');
                           location.reload();
                       } else {
                           alert('更新失败!');
                       }
                   },
                   error:function(response){
                       alert("---error:"+response);
                   }
               });
           }
        }

    </script>

    <style>
        /*.split_panel {
            width:1000px
        }*/

        .title {
            background: #f9f9f9 url(../../img/bg-content-title.png);
            border-color: #fbfbfb !important;
            color: #111113;
            width: 20%;
        }

        .sel_btn {
            height: 25px;
            line-height: 25px;
            padding: 0 11px;
            background: #a1a9b1;
            border: 1px #abb8bb solid;
            border-radius: 3px;
            /* color: #fff; */
            display: inline-block;
            text-decoration: none;
            font-size: 14px;
            outline: none;
        }
    </style>

</body>
</html>
