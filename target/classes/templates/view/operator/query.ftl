<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>会员管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">

    <style>
        .modal-footer {
            padding: 15px;
            text-align: right;
            border-top: 0px solid #e5e5e5;

        }
    </style>

</head>

<body class="fixed-sidebar">
<div id="wrapper">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
                <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4" style="width: 24%">
                    <ol class="breadcrumb">
                        <li style="color: #0d8ddb;font-weight: bold">当前位置:</li>
                        <li><a href="${ctx}/static/index.html">后台管理</a></li>
                        <li class="active">管理员查询</li>
                    </ol>
            </div>

            <div id="main-header" class="col-sm-4" style="font-size: 14px;margin-top: 10px;margin-left: 0px;color: #0e9aef">
                <p   class="main-timer"><b>${user.attributes.realname }</b><span id="show-timer" ></span></p>
            </div>

            <div  class="col-sm-4" style="margin-top: 10px;width: 20% ">
                <a href="${ctx}/logout"><i class="fa fa-sign-out"></i>退出登录</a>
            </div>
        </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="ibox-content m-b-sm border-bottom">
                    <div class="row">
                        <div clss="well">
                            <div class="form-group">
                                <a class="btn btn-success" onclick="$('#addOperator').modal();">增加</a>&nbsp;&nbsp;
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="table-responsive ">
                                    <table class="table table-centerbody table-striped table-condensed text-nowrap" id="editable-sample">
                                        <thead>
                                        <tr>
                                            <th>管理员名称</th>
                                            <th>邮箱</th>
                                            <th>角色</th>
                                            <th>状态</th>
                                            <th>最后登录时间</th>
                                            <th>创建时间</th>
                                            <th>操作</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <#if model?? && model.list?? && (model.list?size > 0) >
                                            <#list model.list as info>
                                            <tr>
                                                <td>${info.userName}</td>
                                                <td>${info.email}</td>
                                                <td>${info.roleName}</td>
                                                <td><#if info.status=='0'>
                                                    锁定
                                                <#elseif info.status=='1'>
                                                    正常
                                                </#if></td>
                                                <td>${info.lastLoginTime}</td>
                                                <td>${info.createTime}</td>
                                                <td>
                                                    <#if info.status=='0'>
                                                        <a onclick="unlock('${info.userId}','${info.userName}')">解锁</a>
                                                    </#if>
                                                </td>

                                            </tr>

                                            </#list>
                                        </#if>
                                        </tbody>
                                    </table>
                                </div>
                            ${page}
                            </div>

                        </div>
                    </div>
                </div>
            </div>

    </div>




</div>

    <div class="modal fade" id="addOperator" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="addroleLabel">添加管理员</h4>
                </div>
                <div class="modal-body">
                    <form id="operator" enctype="multipart/form-data" action="/operator/addOperator"  method="post">
                        <div class="form-group">
                            <label  class="control-label">管理员名称:</label>
                            <input type="text" style="height:30px" class="form-control" name="account" id="account" placeholder="请输入管理员名称"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">登录密码:</label>
                            <input type="text" style="height:30px" class="form-control" name="password" id="password" placeholder="请输入登陆密码"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">确认密码:</label>
                            <input type="text" style="height:30px" class="form-control" name="rePassword" id="rePassword" placeholder="请再次输入密码"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">选择角色:</label>
                            <select name="role" id="role">
                                <option value="1">管理员</option>
                                <option value="2">客服</option>
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                            <button type="submit"  class="btn btn-primary">提交</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>





<!-- 全局 scripts -->
<script src="${ctx}/static/js/jquery-2.1.1.js"></script>
<script src="${ctx}/static/js/bootstrap.js"></script>
<script src="${ctx}/static/js/wuling.js"></script>
<script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>
<script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<!-- 插件 scripts -->
<script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script><!---顶部弹出提示--->
<script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="${ctx}/static/js/jquery.form-3.14.js"></script>
<script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script>
<script src="${ctx}/static/js/My97DatePicker/WdatePicker.js" ></script>
<script type="text/javascript" src="${ctx}/static/js/jquery.common-1.0.0.js" charset="UTF-8"></script>
    <script src="${ctx}/static/js/layer/layer.js" ></script>

</body>

<script>


    /**
     * 禁用账户
     */
    function unlock(id,name){
        var index = layer.confirm("确定解锁用户:"+name+" ？",function(){
            var load = layer.load();
            $.post('/operator/unlockAdmin',{"userId":id,"userName":name,"status":status},function(result){
                layer.close(load);
                if(result && result.status == 200){
                    layer.msg(result.message);
                    setTimeout(function(){
                        //3秒后刷新
                        location.reload();
                    },3000);
                }else{
                    return layer.msg(result.message,so.default),!0;
                }
            },'json');
            layer.close(index);
        });


    }

    $(function(){
        var load;
        $("#operator").ajaxForm({
            success:function (result){
                layer.close(load);
                if(result && result.status != 200){
                    return layer.msg(result.message,function(){}),!1;
                }else{
                    layer.msg(result.message);
                    $("form :password").val('');

                    setTimeout(function(){
                        //3秒后刷新
                        layer.close();
                        location.reload();
                    },2000);
                }
            },
            beforeSubmit:function(){
                //判断参数
                if($.trim($("#account").val()) == ''){
                    layer.msg('请输入管理员名称',function(){});
                    $("#account").parent().removeClass('has-success').addClass('has-error');
                    return !1;
                }else{
                    $("#account").parent().removeClass('has-error').addClass('has-success');
                }
                if($.trim($("#password").val()) == ''){
                    layer.msg('请输入密码',function(){});
                    $("#password").parent().removeClass('has-success').addClass('has-error');
                    return !1;
                }else{
                    $("#password").parent().removeClass('has-error').addClass('has-success');
                }

                if($.trim($("#rePassword").val()) == ''){
                    layer.msg('请再次输入密码',function(){});
                    $("#rePassword").parent().removeClass('has-success').addClass('has-error');
                    return !1;
                }else{
                    $("#rePassword").parent().removeClass('has-error').addClass('has-success');
                }
                if($("#rePassword").val() != $("#password").val()){
                    return layer.msg('2次密码输入不一致。',function(){}),!1;
                }
                if($.trim($("#role").val()) == ''){
                    layer.msg('请选择角色',function(){});
                    $("#role").parent().removeClass('has-success').addClass('has-error');
                    return !1;
                }else{
                    $("#role").parent().removeClass('has-error').addClass('has-success');
                }

                load = layer.load('正在提交！！！');
            },
            dataType:"json",
            clearForm:false
        });

    });


</script>
</html>