<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>会员管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">

    <style>
        .modal-footer {
            padding: 15px;
            text-align: right;
            border-top: 0px solid #e5e5e5;

        }
    </style>

</head>

<body class="fixed-sidebar">
<div id="wrapper">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
                <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4" style="width: 24%">
                    <ol class="breadcrumb">
                        <li style="color: #0d8ddb;font-weight: bold">当前位置:</li>
                        <li><a href="${ctx}/static/index.html">会员管理</a></li>
                        <li class="active">会员查询</li>
                    </ol>
            </div>

            <div id="main-header" class="col-sm-4" style="font-size: 14px;margin-top: 10px;margin-left: 0px;color: #0e9aef">
                <p   class="main-timer"><b>${user.attributes.realname }</b><span id="show-timer" ></span></p>
            </div>

            <div  class="col-sm-4" style="margin-top: 10px;width: 20% ">
                <a href="${ctx}/logout"><i class="fa fa-sign-out"></i>退出登录</a>
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">

                    <form method="post" action="/customerManage/queryCustomer" id="formId" class="form-inline">

                        <div clss="well">
                            <div class="form-group">

                                会员名称:<input type="text" class="form-control" style="width: 150px;height: 30px;border: 1px solid #ccc;border-radius: 4px" id="username"  name="username" value="${customerEntity.username}" placeholder="请输入会员名称">&nbsp;

                            <#--    起始日期:
                                <input name="startTime" class="Wdate" type="text" id="d4311" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01 00:00:00\'}'})"
                                       style="width: 185px;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择开始日期" value="${operationRecord.startTime}"> --
                                <input   id="d4312" class="Wdate" type="text" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2020-10-01 00:00:00'})"

                                         name="overTime"   style="width: 185px;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择结束日期" value="${operationRecord.overTime}">-->


                                <button type="submit" class="btn btn-primary">查询</button>
                                <button type="button"  class="btn  btn-danger" onclick="javascript:$('#username').val('');">清空</button>

                            </div>

                        </div>


                    </form>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="table-responsive ">
                                <table class="table table-centerbody table-striped table-condensed text-nowrap" id="editable-sample">
                                    <thead>
                                    <tr>
                                        <th>编号</th>
                                        <th>账号</th>
                                        <th>电话</th>
                                        <th>账户等级</th>
                                        <th>昵称</th>
                                        <th>当前状态</th>
                                        <th>积分</th>
                                        <th>推荐人</th>
                                        <th>头像地址</th>
                                        <th>操作</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    <#if model?? && model.list?? && (model.list?size > 0) >
                                        <#list model.list as info>
                                        <tr>
                                            <td>${info.id}</td>
                                            <td>${info.username}</td>
                                            <td>${info.phone}</td>
                                            <td>${info.level}</td>
                                            <td>${info.nickName}</td>
                                            <td>
                                                <#if info.status=='Y'>
                                                    正常
                                                    <#elseif info.status=='N'>
                                                    禁用
                                                    <#else >
                                                    异常
                                                </#if>
                                            </td>
                                           <#-- <td>${info.lastLoginTime?datetime('yyyy-MM-dd hh:mm:ss')}</td>
                                            <td>${info.lastLoginIp}</td>
                                            <td>${info.createTime?datetime('yyyy-MM-dd hh:mm:ss')}</td>-->
                                            <td>${info.balance}</td>
                                            <td>${info.referee}</td>
                                            <td>${info.headImg}</td>
                                            <td class="text-right text-nowrap" >
                                                <div class="btn-group ">
                                                    <button style="background-color: #cfdce4" onclick="addScore('${info.username}','${info.balance}');" class="btn btn-white btn-sm edit" >上分</button>
                                                </div>
                                                <div class="btn-group ">
                                                    <button  style="background-color: #cfdce4" onclick="deductScore('${info.username}','${info.balance}');" class="btn btn-white btn-sm edit"  >下分</button>
                                                </div>
                                                <div class="btn-group ">
                                                    <button style="background-color: #cfdce4"  onclick="disable('${info.id}','${info.username}','${info.status}');" class="btn btn-white btn-sm edit" >
                                                        <#if info.status== "Y">
                                                            禁用
                                                            <#else>
                                                            启用
                                                        </#if>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>

                                        </#list>
                                    </#if>
                                    </tbody>
                                </table>
                            </div>
                        ${page}
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>




</div>

    <div class="modal fade" id="addScore" tabindex="-1" role="dialog" aria-labelledby="addScore" style="margin-top: 10%">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="addroleLabel">上分</h4>
                </div>
                <div class="modal-body">
                    <form  enctype="multipart/form-data" id="addScoreForm">
                        <div class="form-group">
                            <label  class="control-label">账户名称:</label>
                            <input type="text" style="height:28px;width: 40%" class="form-control" name="username" id="addusername"  readonly="readonly"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">当前积分:</label>
                            <input type="text" style="height:28px;width: 40%" class="form-control" name="balanceBefore" id="balance" readonly="readonly"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">增加积分:</label>
                            <input type="text" style="height:28px;width: 40%" class="form-control" name="balance" id="addSc" />&nbsp;* 只能增加正整数，例如:100
                        </div>

                        <div class="form-group">
                            <label  class="control-label">备注信息:</label>
                            <input type="text" style="height:28px;width: 40%" class="form-control" name="remark" id="remark" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                            <button type="button"  class="btn btn-primary" onclick="submitForm();">提交</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="deductScore" tabindex="-1" role="dialog" aria-labelledby="deductScore" style="margin-top: 10%">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="addroleLabel">下分</h4>
                </div>
                <div class="modal-body">
                    <form  enctype="multipart/form-data" id="deductScoreForm">
                        <div class="form-group">
                            <label  class="control-label">账户名称:</label>
                            <input type="text" style="height:28px;width: 60%" class="form-control" name="username" id="deductAccount"  readonly="readonly"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">当前积分:</label>
                            <input type="text" style="height:28px;width: 60%" class="form-control" name="balanceBefore"  id="deductSc" readonly="readonly"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">处理中的请求:</label>
                            <select class="form-control" style="height:28px;width: 60%" name="downScoreId" id="downScoreId"></select>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">扣除积分:</label>
                            <input type="text" style="height:28px;width: 60%" class="form-control" name="balance" id="balan" />&nbsp;* 只能扣除正整数，例如:100
                        </div>
                        <div class="form-group">
                            <label  class="control-label">备注信息:</label>
                            <input type="text" style="height:28px;width: 60%" class="form-control" name="remark"/>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                            <button type="button"  class="btn btn-primary" onclick="submitDeductForm();">提交</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-sm" id="userRcode" tabindex="-1" role="dialog" aria-labelledby="deductScore" style="margin-top: 10%">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="userRcodeTitle">用户收款码,请支付金额</h4>
                </div>
                <div class="modal-body">
                    <img id="imgFile" src="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                </div>
            </div>
        </div>
    </div>


<!-- 全局 scripts -->
<script src="${ctx}/static/js/jquery-2.1.1.js"></script>
<script src="${ctx}/static/js/bootstrap.js"></script>
<script src="${ctx}/static/js/wuling.js"></script>
<script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>
<script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<!-- 插件 scripts -->
<script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script><!---顶部弹出提示--->
<script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>
<script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script>
<script src="${ctx}/static/js/My97DatePicker/WdatePicker.js" ></script>
<script type="text/javascript" src="${ctx}/static/js/jquery.common-1.0.0.js" charset="UTF-8"></script>
    <script src="${ctx}/static/js/layer/layer.js" ></script>

</body>

<script>
    /**
     * 上分
     * @param username
     * @param score
     */
    function addScore(username,balance){
        $("#addusername").val(username);
        $("#balance").val(balance);

        $("#addScore").modal();
    }

    /**
     * 下分
     */
    function deductScore(username,balance){
        $("#deductAccount").val(username);
        $("#deductSc").val(balance);
        var param = {"username":username,"status":"P"};
        $.ajax({
            type: "POST",
            url: "/downScore/getDownlogList",
            data: param,
            success: function (data) {
                if (data.status==200) {
                    $("#downScoreId").empty();
                    $.each(data.list,function(i,n){
                        var opts = "<option value=\""+n["id"]+"_"+n["createTimeStr"]+"\">金额:"+n["money"]+"【申请时间:"+n["createTimeStr"]+"】</option>";
                        $("#downScoreId").append(opts);
                    });
                    $("#deductScore").modal();
                }
            },
            error: function(data) {
                layer.msg(data.message);
                layer.close();
            }
        });


    }

    /**
     * 禁用账户
     */
    function disable(id,username,status){
        var index = layer.confirm("确定禁用/启动用户:"+username+" ？",function(){
            var load = layer.load();
            $.post('/customerManage/disableCustomer',{"id":id,"username":username,"status":status},function(result){
                layer.close(load);
                if(result && result.status == 200){
                    layer.msg(result.message);
                    setTimeout(function(){
                        //3秒后刷新
                        location.reload();
                    },1000);
                }else{
                    return layer.msg(result.message,so.default),!0;
                }
            },'json');
            layer.close(index);
        });


    }

    function submitForm(){
        $.ajax({
            type: "POST",
            url: "/customerManage/addScore",
            data: $('#addScoreForm').serialize(),
            success: function (data) {
                if (data.status==200) {
                    layer.msg(data.message);
                    layer.close();
                    setTimeout(function(){
                        //3秒后刷新
                        location.reload();
                    },1000);
                } else {
                    layer.msg(data.message);
                    layer.close();
                    setTimeout(function(){
                        //3秒后刷新
                        location.reload();
                    },1000);
                }
            },
            error: function(data) {
                layer.msg(data.message);
                layer.close();
            }
        });

    }

    function submitDeductForm(){
        if($("#downScoreId").children().length == 0){
            layer.msg("该用户没有发出下分的请求");
            layer.close();
        }else{
            $.ajax({
                type: "POST",
                url: "/customerManage/deductScore",
                data: $('#deductScoreForm').serialize(),
                success: function (data) {
                    if (data.status==200) {
                        layer.msg(data.message);
                        layer.close();
                        setTimeout(function(){
                            $("#userRcodeTitle").text("下分操作成功完成,请扫描二维码,支付金额为【"+data.balance+"】");
                            $("#imgFile").attr("src","${ctx}/upload/"+data.imgUrl);
                            $("#userRcode").modal();
                            $('#userRcode').on('hidden.bs.modal', function (e) {
                                location.reload();
                            })
                        },1000);
                    } else {
                        layer.msg(data.message);
                        layer.close();
                        setTimeout(function(){
                            //3秒后刷新
                            location.reload();
                        },1000);
                    }
                },
                error: function(data) {
                    layer.msg(data.message);
                    layer.close();
                }
            });
        }

    }






</script>
</html>