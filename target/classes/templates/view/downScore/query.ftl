<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>会员管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">

    <style>
        .modal-footer {
            padding: 15px;
            text-align: right;
            border-top: 0px solid #e5e5e5;

        }
    </style>

    <script>
        function resetbtns(btn){
            $("#username1").val("");
            $("#status").val("all");
            var resetArr = $(btn).parents("form").find(":text");
            for(var i=0; i<resetArr.length; i++){
                if(i>0){
                    resetArr.eq(i).val("");
                }
            }
        }
    </script>
</head>

<body class="fixed-sidebar">
<div id="wrapper">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
                <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4" style="width: 24%">
                    <ol class="breadcrumb">
                        <li style="color: #0d8ddb;font-weight: bold">当前位置:</li>
                        <li><a href="${ctx}/static/index.html">会员管理</a></li>
                        <li class="active">下分请求处理</li>
                    </ol>
            </div>

            <div id="main-header" class="col-sm-4" style="font-size: 14px;margin-top: 10px;margin-left: 0px;color: #0e9aef">
                <p   class="main-timer"><b>${user.attributes.realname }</b><span id="show-timer" ></span></p>
            </div>

            <div  class="col-sm-4" style="margin-top: 10px;width: 20% ">
                <a href="${ctx}/logout"><i class="fa fa-sign-out"></i>退出登录</a>
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">

                    <form method="post" action="/downScore/queryDownlist" id="formId" class="form-inline">

                        <div clss="well">
                            <div class="form-group">

                                会员名称:<input type="text" class="form-control" style="width: 150px;height: 30px;border: 1px solid #ccc;border-radius: 4px"  name="username" id="username1" value="${downLogBean.username}" placeholder="请输入会员名称">&nbsp;

                                开奖日期:

                                <input name="startDate" class="Wdate" type="text" id="d4311" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01 00:00:00\'}'})"
                                       style="width: 140px;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择开始日期" value="${downLogBean.startDate!}"> --
                                <input   id="d4312" class="Wdate" type="text" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2020-10-01 00:00:00'})"
                                         name="endDate"   style="width: 140px;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择结束日期" value="${downLogBean.endDate!}">
                                处理状态:<select class="form-control"  name="status" id="status"  style="width: 90px;height: 30px;border: 1px solid #ccc;border-radius: 4px">
                                    <option value="all" id="all">全部</option>
                                    <option value="N" id="status_n">新情求</option>
                                    <option value="P"  id="status_p">处理中</option>
                                    <option value="F"  id="status_f">已完成</option>
                                </select>
                                <button type="submit" class="btn btn-primary">查询</button>
                                <button type="button" onclick="javascript:resetbtns(this)" class="btn  btn-danger">清空</button>

                            </div>

                        </div>


                    </form>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="table-responsive ">
                                <table class="table table-centerbody table-striped table-condensed text-nowrap" id="editable-sample">
                                    <thead>
                                    <tr>
                                        <th width="50" style="text-align:center">编号</th>
                                        <th width="105" style="text-align:center">账号</th>
                                        <th width="80" style="text-align:center">下分数量</th>
                                        <th width="160"  style="text-align:center">申请时间</th>
                                        <th width="85" style="text-align:center">处理状态</th>
                                        <th style="text-align:center">备注</th>
                                        <th width="90" style="text-align:center">操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <#if model?? && model.list?? && (model.list?size > 0) >
                                        <#list model.list as info>
                                        <tr>
                                            <td style="text-align:center">${info.id}</td>
                                            <td style="text-align:center">${info.username}</td>
                                            <td style="text-align:center">${info.money}</td>
                                            <td style="text-align:center">${info.createTimeStr}</td>
                                            <td style="text-align:center">
                                                <#if info.status=='N'>
                                                    <span style="color:dodgerblue"><b>新请求</b></span>
                                                    <#elseif info.status=='P'>
                                                    <span style="color:red"><b>处理中</b></span>
                                                    <#else>
                                                    <span style="color:limegreen"><b>已完成</b></span>
                                                </#if>
                                            </td>
                                            <td style="text-align:center">${info.remark}</td>
                                            <td class="text-right text-nowrap" style="text-align:center">
                                                <#if info.status=='N'>
                                                    <div class="btn-group ">
                                                        <button style="background-color: #cfdce4" onclick="processReq('${info.id}','${info.username}','${info.createTimeStr}');" class="btn btn-white btn-sm edit" >处理请求</button>
                                                    </div>
                                                </#if>
                                            </td>
                                        </tr>

                                        </#list>
                                    </#if>
                                    </tbody>
                                </table>
                            </div>
                        ${page}
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>




</div>
<!-- 全局 scripts -->
<script src="${ctx}/static/js/jquery-2.1.1.js"></script>
<script src="${ctx}/static/js/bootstrap.js"></script>
<script src="${ctx}/static/js/wuling.js"></script>
<script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>
<script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<!-- 插件 scripts -->
<script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script><!---顶部弹出提示--->
<script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>
<script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script>
<script src="${ctx}/static/js/My97DatePicker/WdatePicker.js" ></script>
<script type="text/javascript" src="${ctx}/static/js/jquery.common-1.0.0.js" charset="UTF-8"></script>
    <script src="${ctx}/static/js/layer/layer.js" ></script>

</body>

<script>
    $(function(){
        var statuss = "${downLogBean.status}";
        if(statuss == "N"){
            $("#status_n").attr("selected",true);
        }else if(statuss =="P") {
            $("#status_p").attr("selected",true);
        }else if(statuss =="F") {
            $("#status_f").attr("selected",true);
        }else{
            $("#status").val("all");
        }
    });
    /**
     * 禁用账户
     */
    function processReq(id,username,createTimeStr){
        var index = layer.confirm("将"+username+"的提现请求由【新请求】状态变为【处理中】状态吗？",function(){
            var load = layer.load();
            $.post('/downScore/convertStatus',{"id":id,"username":username,"createTimeStr":createTimeStr},function(result){
                layer.close(load);
                if(result && result.status == 200){
                    layer.msg(result.message);
                    setTimeout(function(){
                        //1秒后刷新
                        location.reload();
                    },1000);
                }else{
                    return layer.msg(result.message,so.default),!0;
                }
            },'json');
            layer.close(index);
        });
    }
</script>
</html>