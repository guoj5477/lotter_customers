<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">




    <style>
       a {
            cursor:pointer;
        }
        .table th, .table td {
            padding: 1px;
            line-height: 20px;
            text-align: left;
            vertical-align: middle;
            border-top: 1px solid #ddd;
        }

        form input, form select, form textarea {
            padding: 4px;
            font-size: 14px;
            border: 1px solid #d5d5d5;
            color: #333;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            padding: 0px;
            line-height: 1.42857143;
            vertical-align: inherit;
            border-top: 1px solid #ddd;
        }
        .title {
            background: #f9f9f9 url(../img/bg-content-title.png);
            border-color: #f8fbfb !important;
            color: #111113;
        }

        .notification {
            position: relative;
            margin: 0 0 10px 0;
            padding: 0;
            border: 1px solid;
            background-position: 10px 11px !important;
            background-repeat: no-repeat !important;
            font-size: 13px;
            width: 21.8%;
        }


    </style>
</head>

<body class="fixed-sidebar">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
            <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4" style="width: 24%">
                    <ol class="breadcrumb">
                        <li style="color: #0d8ddb;font-weight: bold">
                            当前位置:
                        </li>
                        <li>
                            <a href="${ctx}/static/index.html">开奖时间管理</a>
                        </li>
                        <li class="active">
                            <#if lotteryId==1>
                                重庆时时彩

                            <#elseif lotteryId==2>
                                广西快乐十分

                            <#elseif lotteryId==3>
                                幸运农场

                            <#elseif lotteryId=4>
                                江苏快3
                            </#if>
                        </li>

                    </ol>
                </div>

                <div id="main-header" class="col-sm-4" style="font-size: 14px;margin-top: 10px;margin-left: 0px;color: #0e9aef">
                    <p   class="main-timer"><b>${user.attributes.realname }</b><span id="show-timer" ></span></p>
                </div>

                <div  class="col-sm-4" style="margin-top: 10px;width: 20% ">
                    <a href="${ctx}/logout"><i class="fa fa-sign-out"></i>退出登录</a>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="ibox-content m-b-sm border-bottom">
                    <div class="row">
                        <form method="post" action="/dateManage/lotteryList" id="formId" class="form-inline">

                            <div clss="well">
                                <div class="form-group">
                                    <input type="text"  name="lotteryId" value="${lotteryId}" style="display: none">
                                    <input type="text" class="form-control" style="width: 185px;height: 30px;border: 1px solid #ccc;border-radius: 4px"  name="lotteryOrder"  placeholder="输入场次查询">
                                    <button type="submit" class="btn btn-primary">查询</button>
                                    <button type="reset"  class="btn  btn-danger">清空</button>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="table-responsive ">
                                    <table class="table table-centerbody table-striped table-condensed text-nowrap" id="editable-sample">
                                        <thead>
                                            <tr>

                                                <th>彩票名称</th>
                                                <th>场次</th>
                                                <th>开始时间</th>
                                                <th>结束时间</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                            <#if model?? && model.list?? && (model.list?size > 0) >
                                <#list model.list as info>
                                            <tr>
                                                <td style="padding-left: 10px">
                                                    <#if info.lotteryId==1>
                                                        重庆时时彩
                                                    <#elseif info.lotteryId==2>
                                                        广西快乐十分
                                                    <#elseif info.lotteryId==3>
                                                        幸运农场
                                                    <#elseif info.lotteryId==4>
                                                        江苏快3
                                                    </#if>
                                                </td>

                                                <td>${info.lotteryOrder}</td>

                                                <td style="width: 15%;height: 30px"><input  value="${info.action_time}"  class="action_time" style="width: 100%;height: 30px"></td>

                                                <td style="width: 15%;height: 30px"><input  value="${info.stop_time}"  class="stop_time" style="width: 100%;height: 30px"></td>
                                                <td  class="text-right text-nowrap">

                                                    <div class="btn-group ">
                                                        <button style="background-color: #e4e8e5" onclick="updateLotteryTime(this,'${info.lotteryOrder}','${info.lotteryId}')" class="btn btn-white btn-sm edit" >保存修改</button>
                                                    </div>

                                                </td>
                                            </tr>

                                </#list>
                            </#if>
                                        </tbody>
                                    </table>
                                </div>
                              ${page}
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="footer">
                <div class="pull-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2015
                </div>
            </div>
        </div>


    </div>


    <script src="${ctx}/static/js/jquery-2.1.1.js"></script>
    <script src="${ctx}/static/js/bootstrap.js"></script>
    <script src="${ctx}/static/js/wuling.js"></script>
    <script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>
    <script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script>
    <script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script>
    <script src="${ctx}/static/js/My97DatePicker/WdatePicker.js" ></script>
    <script type="text/javascript" src="${ctx}/static/js/jquery.common-1.0.0.js" charset="UTF-8"></script>
    <script src="${ctx}/static/js/layer/layer.js" ></script>
    <script>
        function updateLotteryTime(object,lotteryOrder,lotteryId){
            //获取当前行隐藏输入框中的值
            var action_time=$(object).parents("tr").find(".action_time").val();
            var stop_time=$(object).parents("tr").find(".stop_time").val();
            $.ajax({
                type: 'post',
                url: '/dateManage/updateLotteryTime',
                data:{lotteryOrder:lotteryOrder,action_time:action_time,stop_time:stop_time,lotteryId:lotteryId},
                success:function(response) {

                    if (response.status) {
                        layer.msg(response.message);

                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    } else {
                        layer.msg(response.message);

                    }
                },
                error:function(response){
                    alert("---error:"+response);
                    layer.msg(response.message)
                }
            });

        }
    </script>
</body>
</html>
