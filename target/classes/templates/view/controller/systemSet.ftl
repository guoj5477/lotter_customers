<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">


    <style>


    </style>
</head>

<body class="fixed-sidebar">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
                <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4" style="width: 20%">
                    <ol class="breadcrumb">
                        <li style="color: #0d8ddb;font-weight: bold">
                            当前位置:
                        </li>
                        <li>
                            <a href="${ctx}/static/index.html">游戏控制</a>
                        </li>
                        <li class="active">
                            系统设置
                        </li>

                    </ol>
                </div>

                <div id="main-header" class="col-sm-4" style="font-size: 14px;margin-top: 10px;margin-left: 0px;color: #0e9aef">
                    <p   class="main-timer"><b>${user.attributes.realname }</b><span id="show-timer" ></span></p>
                </div>

                <div  class="col-sm-4" style="margin-top: 10px;width: 20% ">
                    <a href="${ctx}/logout"><i class="fa fa-sign-out"></i>退出登录</a>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="ibox-content m-b-sm border-bottom">
                    <div class="row">
                        <div clss="well">
                            <div class="form-group">
<#--
                                <shiro.hasPermission name="admin">-->
                                    <a class="btn btn-success" onclick="$('#addHomeMessage').modal();">增加</a>&nbsp;&nbsp;
                                    <a class="btn btn-success" href="view/notice/backstageNotice">刷新</a>
                             <#--   </shiro.hasPermission>-->

                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="table-responsive ">
                                    <table class="table table-centerbody table-striped table-condensed text-nowrap" id="editable-sample">
                                        <thead>
                                            <tr>
                                                <th>编号</th>
                                                <th>系统代码</th>
                                                <th>系统名称</th>
                                                <th>春节日期</th>
                                                <th>当前状态</th>
                                                <th>维护开始时间</th>
                                                <th>维护结束时间</th>
                                                <th>维护原因</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                            <#if model??  && (model?size > 0) >
                                <#list model as info>
                                            <tr>
                                                <td style="vertical-align: middle">${info.id}</td>
                                                <td style="vertical-align: middle">${info.sysCode}</td>
                                                <td style="vertical-align: middle">${info.sysName}</td>
                                                <td style="vertical-align: middle">
                                                    <input type="text" id="springFestival" value="${info.springFestival}" class="Wdate"  onclick="WdatePicker()" style="width: 185px;height: 30px;border: 1px solid #ccc;border-radius: 4px"  placeholder="请设置春节时间">
                                                    <button type="button" class="btn btn-primary" onclick="saveSpringFestival('${info.sysCode}');">保存</button>
                                                </td>

                                                <td style="vertical-align: middle">
                                                    <#if info.sysSwitch =='1'>
                                                        正常
                                                    <#elseif info.sysSwitch =='2'>
                                                        维护中...
                                                    <#elseif info.sysSwitch =='3'>
                                                        关闭
                                                    </#if>
                                                </td>

                                                <td style="vertical-align: middle">${info.restStartTime}</td>
                                                <td style="vertical-align: middle">${info.restEndTime}</td>
                                                <td style="vertical-align: middle">${info.restReason}</td>
                                                <td style="vertical-align: middle">
                                                    <#if info.sysSwitch== '1'>
                                                        <button type="button" class="btn btn-primary" onclick="maintenance('${info.sysCode}');">维护</button>
                                                        <button type="button" class="btn btn-primary" onclick="closeSystem('${info.sysCode}')">关闭</button>

                                                    <#elseif info.sysSwitch=='2'>
                                                        <button type="button" class="btn btn-primary" onclick="cancelMaintenance('${info.sysCode}');">正常</button>
                                                        <button type="button" class="btn btn-primary" onclick="closeSystem('${info.sysCode}')">关闭</button>
                                                    <#elseif info.sysSwitch=='3'>
                                                        <button type="button" class="btn btn-primary" onclick="cancelMaintenance('${info.sysCode}');">正常</button>
                                                        <button type="button" class="btn btn-primary" onclick="maintenance('${info.sysCode}');">维护</button>
                                                    </#if>
                                                </td>
                                            </tr>

                                </#list>
                            </#if>
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="SystemMaintenance" tabindex="-1" role="dialog" aria-labelledby="SystemMaintenance">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" >系统维护</h4>
                        </div>

                        <div class="modal-body">
                            <form id="maintenance" enctype="multipart/form-data" action="/game/systemMaintenance"  method="post">

                                <div class="form-group">
                                    <label  class="control-label">维护开始时间:</label>
                                    <input name="restStartTime" class="Wdate" type="text" id="d4311" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01 00:00:00\'}'})"
                                           style="width: 185px;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择开始时间">
                                </div>
                                <div class="form-group">
                                    <label  class="control-label">维护结束时间:</label>
                                    <input   id="d4312" class="Wdate" type="text" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2020-10-01 00:00:00'})"
                                             name="restEndTime"   style="width: 185px;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择结束日期" >
                                </div>
                                <div class="form-group">
                                    <label  class="control-label">维护原因:</label>
                                    <textarea style="height:60px;width: 98%" class="form-control" name="restReason" id="restReason" placeholder="请输入内容"></textarea>
                                </div>
                                <div class="form-group">
                                    <textarea style="height:60px;width: 98%" class="form-control" name="sysCode" id="sysCode" placeholder="请输入内容"></textarea>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                                    <button type="submit"  class="btn btn-primary">提交</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>

            </div>

            <div class="footer">
                <div class="pull-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2015
                </div>
            </div>
        </div>

    </div>

    <script src="${ctx}/static/js/jquery-2.1.1.js"></script>
    <script src="${ctx}/static/js/bootstrap.js"></script>
    <script src="${ctx}/static/js/wuling.js"></script>
    <script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>
    <script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script>
    <script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script>
    <script src="${ctx}/static/js/My97DatePicker/WdatePicker.js" ></script>
    <script src="${ctx}/static/js/layer/layer.js" ></script>
    <script type="text/javascript" src="${ctx}/static/js/jquery.common-1.0.0.js" charset="UTF-8"></script>


    <script>

        $(function(){
            var load;
            $("#maintenance").ajaxForm({
                success:function (result){
                    layer.close(load);
                    if(result && result.status != 200){
                        return layer.msg(result.message,function(){}),!1;
                    }else{
                        layer.msg(result.message);

                        setTimeout(function(){
                            //3秒后刷新
                            layer.close();
                            location.reload();
                        },2000);
                    }
                },
                beforeSubmit:function(){
                    //判断参数
                    if($.trim($("#d4311").val()) == ''){
                        layer.msg('请输入维护开始时间',function(){});
                        $("#d4311").parent().removeClass('has-success').addClass('has-error');
                        return !1;
                    }else{
                        $("#d4311").parent().removeClass('has-error').addClass('has-success');
                    }
                    if($.trim($("#d4312").val()) == ''){
                        layer.msg('请输入维护结束时间',function(){});
                        $("#d4312").parent().removeClass('has-success').addClass('has-error');
                        return !1;
                    }else{
                        $("#d4312").parent().removeClass('has-error').addClass('has-success');
                    }
                    if($.trim($("#restReason").val()) == ''){
                        layer.msg('请输入维护原因',function(){});
                        $("#restReason").parent().removeClass('has-success').addClass('has-error');
                        return !1;
                    }else{
                        $("#restReason").parent().removeClass('has-error').addClass('has-success');
                    }

                    load = layer.load('正在提交！！！');
                },
                dataType:"json",
                clearForm:false
            });

        });

        function updateSwitch(val,obj) {
            if (obj.checked) {
                var flag = 1;
            } else {
                var flag = 0;
            }
            $.post('/game/updateLottSet', {id: val, flag: flag}, function (result) {

                if (result && result.status == 200) {

                    layer.msg(result.message);
                    setTimeout(function () {
                        //3秒后刷新
                        location.reload();
                    }, 3000);
                } else {
                    return layer.msg(result.message, so.default), !0;
                }
            }, 'json');
        }
        // 设置春节日期
        function  saveSpringFestival(sysCode) {

            var springFestival=$("#springFestival").val();
            if(springFestival==""){
                layer.msg("时间不能为空");
                return false;
            }
            $.post('/game/saveSpringFestival', {springFestival:springFestival,sysCode:sysCode}, function (result) {
                if (result && result.status == 200) {
                    layer.msg(result.message);
                    setTimeout(function () {
                        //3秒后刷新
                        location.reload();
                    }, 3000);
                } else {
                    return layer.msg(result.message, so.default), !0;
                }
            }, 'json');
        }
        function maintenance(sysCode){
            $("#sysCode").val(sysCode);
            $('#SystemMaintenance').modal();
        }

        function cancelMaintenance(sysCode){
            if(sysCode==""){
                layer.msg("未获取到数据");
                return false;
            }

            $.post('/game/cancelMaintenance', {sysCode:sysCode}, function (result) {

                if (result && result.status == 200) {

                    layer.msg(result.message);
                    setTimeout(function () {
                        //3秒后刷新
                        location.reload();
                    }, 3000);
                } else {
                    return layer.msg(result.message, so.default), !0;
                }
            }, 'json');
        }
        function  closeSystem(sysCode){
            if(sysCode==""){
                layer.msg("未获取到数据");
                return false;
            }

            $.post('/game/closeSystem', {sysCode:sysCode}, function (result) {

                if (result && result.status == 200) {

                    layer.msg(result.message);
                    setTimeout(function () {
                        //3秒后刷新
                        location.reload();
                    }, 3000);
                } else {
                    return layer.msg(result.message, so.default), !0;
                }
            }, 'json');
   }

  </script>
</body>
</html>
