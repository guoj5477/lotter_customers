<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">
    <style>
        a {
            cursor:pointer;
        }
        .glyphicon-remove:before {
            content: "";
        }
        .table th, .table td {
            padding: 1px;
            line-height: 20px;
            text-align: left;
            vertical-align: middle;
            border-top: 1px solid #ddd;
        }
        form input, form select, form textarea {
            padding: 4px;
            font-size: 14px;
            border: 1px solid #d5d5d5;
            color: #333;
        }
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            padding: 2px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

    </style>
</head>

<body class="fixed-sidebar">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
            <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4" style="width: 24%">
                    <ol class="breadcrumb">
                        <li style="color: #0d8ddb;font-weight: bold">
                            当前位置:
                        </li>
                        <li>
                            <a href="${ctx}/static/index.html">统计报表</a>
                        </li>
                        <li class="active">
                            账变记录
                        </li>

                    </ol>
                </div>

                <div id="main-header" class="col-sm-4" style="font-size: 14px;margin-top: 10px;margin-left: 0px;color: #0e9aef">
                    <p   class="main-timer"><b>${user.attributes.realname }</b><span id="show-timer" ></span></p>
                </div>

                <div  class="col-sm-4" style="margin-top: 10px;width: 20% ">
                    <a href="${ctx}/logout"><i class="fa fa-sign-out"></i>退出登录</a>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="ibox-content m-b-sm border-bottom">
                    <div class="row">
                        <form method="post" action="/report/getRecharge" id="formId" class="form-inline">
                            <div clss="well">
                                <div class="form-group">
                                 <#--   <%--  注单号:<input type="text" class="form-control" style="width: 120px;height: 30px"  name="orderId"  value="${orderModel.orderId}" placeholder="输入注单号">--%>-->
                                        用户名:<input type="text" class="form-control" style="width: 120px;height: 30px;border: 1px solid #ccc;border-radius: 4px"  name="account"  placeholder="输入用户名" value="${rechargeModel.account}">
                                        期号:<input type="text" class="form-control" style="width: 120px;height: 30px;border: 1px solid #ccc;border-radius: 4px"  name="pid"  placeholder="输入期号:" value="${rechargeModel.pid}">
                                        起始日期:
                                        <input name="startTime" class="Wdate" type="text" id="d4311" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01 00:00:00\'}'})"
                                               style="width: 185px;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择开始日期" value="${rechargeModel.startTime}"> --
                                        <input   id="d4312" class="Wdate" type="text" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2020-10-01 00:00:00'})"
                                                 name="overTime"   style="width: 185px;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择结束日期" value="${rechargeModel.overTime}">



                                        状态:<select class="form-control"  name="type" style="width: 150px;height: 30px;border: 1px solid #ccc;border-radius: 4px">
                                        <option value="">请选择状态</option>
                                        <option value="IN" id="IN">增加</option>
                                        <option value="OUT" id="OUT">扣除</option>

                                    </select>

                                        操作类型:
                                        <select class="form-control"  name="operator" style="width: 150px;height: 30px;border: 1px solid #ccc;border-radius: 4px">
                                            <option value="">请选择操作类型</option>
                                            <option value="LOTT_CENTER" id="lott">彩票</option>
                                            <option value="SYSTEM" id="system">系统</option>
                                            <option value="Backstage" id="bg">后台</option>
                                        </select>
                                        <button type="submit" class="btn btn-primary">查询</button>
                                        <button type="reset"  id="reset" class="btn  btn-danger">清空</button>


                                </div>

                            </div>
                        </form>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="table-responsive ">
                                    <table class="table table-centerbody table-striped table-condensed text-nowrap" id="editable-sample">
                                        <thead>
                                            <tr>
                                                <th>编号</th>
                                                <th>单号</th>
                                                <th>期号</th>
                                                <th>用户名</th>
                                                <th>类型</th>
                                                <th>操作</th>
                                                <th>创建时间</th>
                                                <th>金额</th>
                                                <th>备注信息</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                            <#if model?? && model.list?? && (model.list?size > 0) >
                                <#list model.list as info>
                                            <tr>
                                                <td>${info.id}</td>
                                                <td>${info.serial_no}</td>
                                                <td>${info.pid}</td>
                                                <td>${info.account}</td>
                                                <td>
                                                    <#if info.type=='OUT'>
                                                          扣除
                                                     <#else>
                                                         增加
                                                     </#if>
                                                </td>
                                                <td>${info.operator}</td>
                                                <td>
                                                      2017-12-12 00:00:00
                                                </td>
                                                <td>${info.money}</td>
                                                <td>${info.remark}</td>
                                            </tr>

                                </#list>
                            </#if>
                                        </tbody>
                                    </table>
                                </div>
                              ${page}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="pull-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2015
                </div>
            </div>
        </div>


    </div>

    <script src="${ctx}/static/js/jquery-2.1.1.js"></script>
    <script src="${ctx}/static/js/bootstrap.js"></script>
    <script src="${ctx}/static/js/wuling.js"></script>
    <script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>
    <script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script>
    <script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script>
    <script src="${ctx}/static/js/My97DatePicker/WdatePicker.js" ></script>
    <script type="text/javascript" src="${ctx}/static/js/jquery.common-1.0.0.js" charset="UTF-8"></script>

    <script>

        laydate.render({
            elem: '#startTime'
            ,type: 'datetime'
            ,theme: '#393D49'
        });
        laydate.render({
            elem: '#overTime'
            ,type: 'datetime'
            ,theme: '#393D49'
        });

        $(document).ready(function(){
            if(${sessionScope.rechargeModel.type=="IN"}){
                $("#IN").attr("selected",true);
            }else if(${sessionScope.rechargeModel.type=="OUT"}) {
                $("#OUT").attr("selected",true);
            }

            if(${sessionScope.rechargeModel.operator=="LOTT_CENTER"}){
                $("#lott").attr("selected",true);
            }else if(${sessionScope.rechargeModel.operator=="Backstage"}) {
                $("#bg").attr("selected",true);
            }else if(${sessionScope.rechargeModel.operator=="SYSTEM"}) {
                $("#system").attr("selected",true);
            }

            $("#reset").click(function(){
                var resetArr = $(this).parents("form").find(":input");
                for(var i=0; i<resetArr.length; i++){
                    if(i>0){
                        resetArr.eq(i).val("");
                    }
                }
                return false;　　//一定要return false，阻止reset按钮功能，不然值又会变成aa
            });

        });
        function updateLottery(){
            $("#formId").submit();
        }
    </script>
</body>
</html>
