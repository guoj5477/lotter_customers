<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">


    <style>


    </style>
</head>

<body class="fixed-sidebar">
<div id="wrapper">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
                <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4" style="width: 24%">
                    <ol class="breadcrumb">
                        <li style="color: #0d8ddb;font-weight: bold">
                            当前位置:
                        </li>
                    <li>
                        <a href="${ctx}/static/index.html">统计报表</a>
                    </li>
                    <li class="active">
                        后台操作记录
                    </li>

                </ol>
            </div>

            <div id="main-header" class="col-sm-4" style="font-size: 14px;margin-top: 10px;margin-left: 0px;color: #0e9aef">
                <p   class="main-timer"><b>${user.attributes.realname }</b><span id="show-timer" ></span></p>
            </div>

            <div  class="col-sm-4" style="margin-top: 10px;width: 20% ">
                <a href="${ctx}/logout"><i class="fa fa-sign-out"></i>退出登录</a>
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">

                    <form method="get" action="/view/record/operationRecord" id="formId" class="form-inline">

                        <div clss="well">
                            <div class="form-group">

                                管理员:<input type="text" class="form-control" style="width: 150px;height: 30px;border: 1px solid #ccc;border-radius: 4px"  name="operator" value="${operationRecord.operator}" placeholder="请输入管理员名称">&nbsp;

                                起始日期:
                                <input name="startTime" class="Wdate" type="text" id="d4311" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01 00:00:00\'}'})"
                                       style="width: 185px;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择开始日期" value="${operationRecord.startTime}"> --
                                <input   id="d4312" class="Wdate" type="text" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2020-10-01 00:00:00'})"

                                         name="overTime"   style="width: 185px;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择结束日期" value="${operationRecord.overTime}">


                                <button type="submit" class="btn btn-primary">查询</button>
                                <button type="reset"  class="btn  btn-danger">清空</button>

                            </div>

                        </div>


                    </form>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="table-responsive ">
                                <table class="table table-centerbody table-striped table-condensed text-nowrap" id="editable-sample">
                                    <thead>
                                    <tr>
                                        <th>编号</th>
                                        <th>事件</th>
                                        <th>类型</th>
                                        <th>操作人</th>
                                        <th>操作时间</th>
                                        <th>IP地址</th>
                                        <th>备注</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <#if model?? && model.list?? && (model.list?size > 0) >
                                        <#list model.list as info>
                                        <tr>
                                            <td>${info.id}</td>
                                            <td>${info.event}</td>
                                            <td>${info.type}</td>
                                            <td>${info.operator}</td>
                                            <td>${info.operationTime}</td>
                                            <td>${info.ip}</td>
                                            <td>${info.remark}</td>
                                        </tr>

                                        </#list>
                                    </#if>
                                    </tbody>
                                </table>
                            </div>
                        ${page}
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="pull-right">
                10GB of <strong>250GB</strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> Example Company &copy; 2014-2015
            </div>
        </div>
    </div>


</div>




<!-- 全局 scripts -->
<script src="${ctx}/static/js/jquery-2.1.1.js"></script>
<script src="${ctx}/static/js/bootstrap.js"></script>
<script src="${ctx}/static/js/wuling.js"></script>
<script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>

<script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<!-- 插件 scripts -->
<script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script><!---顶部弹出提示--->
<script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script>
    <script src="${ctx}/static/js/My97DatePicker/WdatePicker.js" ></script>
    <script type="text/javascript" src="${ctx}/static/js/jquery.common-1.0.0.js" charset="UTF-8"></script>
<script>


</script>
</body>
</html>