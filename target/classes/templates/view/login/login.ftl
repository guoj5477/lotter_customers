<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>会员系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">

    <link href="${ctx}/static/css/font-awesome.css" rel="stylesheet">
	<link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/loginstyle.css" rel="stylesheet">
    <script type="text/javascript" src="${ctx}/static/js/jquery-2.1.1.js" charset="UTF-8"></script>
    <link rel="icon" href="${ctx}/static/img/favicon-02.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="${ctx}/static/img/favicon-02.ico" type="image/x-icon" />
    <script type="text/javascript" src="${ctx}/static/js/layer/layer.js" charset="UTF-8"></script>

</head>

<body class="gray-bg"  onkeydown="keyLogin()" >

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div class="center-block">
        <div>

            <p class="logo-name">后台管理系统</p>

        </div>

        <!--<p>Login in. To see it in action.</p>-->

        <div class="form-group">
            <input type="text" class="form-control" placeholder="account" required="" id="account" >
        </div>
        <div class="form-group">
            <input type="password" class="form-control" placeholder="Password" required="" id="password">
        </div>

        <div style="text-align: left; margin-left: 10px;" id="vcode">

            <input type="text" name="vcode"   placeholder="请输入验证码" style="width: 140px;height: 42px; margin-left: -8px; margin-right: 8px;">
            <img src="/getGifCode" />
        </div>




        <button type="button" class="btn btn-primary block full-width m-b"  onclick="login()" style="margin-top: 20px"  id="login">登录</button>
        <p class="m-t"> <small>版权所有.xxx&copy; 2017</small></p>
    </div>
</div>

</body>


<script>

    jQuery(document).ready(function() {
		//动态验证码
        $("#vcode").on("click",'img',function(){
            /**动态验证码，改变地址，多次在火狐浏览器下，不会变化的BUG，故这样解决*/
            var i = new Image();
            i.src = '/getGifCode?'  + Math.random();
            $(i).replaceAll(this);
            //$(this).clone(true).attr("src",'${basePath}/open/getGifCode.shtml?'  + Math.random()).replaceAll(this);
        });
	});
    function login(){

        var account=$("#account").val();
        var password=$("#password").val();
        var vcode=$("input[name=vcode]").val();
        if(account==null||account==""){
            return layer.msg('用户名不能为空！',function(){}),!1;
        }
        if(password==null||password==""){
            return layer.msg('密码不能为空！',function(){}),!1;
        }
        if(vcode==null||vcode==""){
            return layer.msg('验证码不能为空！',function(){}),!1;
        }
        if($('[name=vcode]').val().length !=4){
            return layer.msg('验证码的长度为4位！',function(){}),!1;
        }

        $.ajax({
            type:"post",
            url:"/login",
            data:{"username":account,"password":password,"vcode":vcode},
            cache:false,
            success:function(data){

                if(data.message=="success"){
                    document.location ='/index';
                }else {
                    alert(data.message);
                }
            },error:function(data){
                alert(data.message);
            }
        });

    }

    function keyLogin(){
        if(event.keyCode==13) {
            login();
        }
    }


</script>
</html>
