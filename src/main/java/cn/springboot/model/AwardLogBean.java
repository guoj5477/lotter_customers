package cn.springboot.model;

import cn.springboot.model.page.Pagination;

import java.math.BigDecimal;
import java.util.Date;

public class AwardLogBean extends Pagination {
    private String lotteryId;
    private String lotteryQishu;
    private String lotteryCodes;
    private String lotteryCount;
    private String startDate;
    private String endDate;
    private BigDecimal betMoney;
    private Date openTime;
    private BigDecimal awardCount;
    private Date createTime;
    public String getLotteryId() {
        return lotteryId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setLotteryId(String lotteryId) {
        this.lotteryId = lotteryId;
    }

    public String getLotteryQishu() {
        return lotteryQishu;
    }

    public void setLotteryQishu(String lotteryQishu) {
        this.lotteryQishu = lotteryQishu;
    }

    public String getLotteryCodes() {
        return lotteryCodes;
    }

    public void setLotteryCodes(String lotteryCodes) {
        this.lotteryCodes = lotteryCodes;
    }

    public String getLotteryCount() {
        return lotteryCount;
    }

    public void setLotteryCount(String lotteryCount) {
        this.lotteryCount = lotteryCount;
    }

    public BigDecimal getBetMoney() {
        return betMoney;
    }

    public void setBetMoney(BigDecimal betMoney) {
        this.betMoney = betMoney;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public BigDecimal getAwardCount() {
        return awardCount;
    }

    public void setAwardCount(BigDecimal awardCount) {
        this.awardCount = awardCount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}
