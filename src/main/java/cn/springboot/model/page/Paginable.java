package cn.springboot.model.page;


public interface Paginable {


    int getTotalCount();

	int getTotalPage();

	int getPageSize();

	int getPageNo();

	boolean isFirstPage();

	boolean isLastPage();

	int getNextPage();

	int getPrePage();
	}
