package cn.springboot.model;

import cn.springboot.model.page.Pagination;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author eric
 * @date 2017/10/24/024 14:33
 * @todo  帐变记录
 */
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "of")
public @Data class RechargeModel extends Pagination implements Serializable {

    private int id; //编号
    private String serial_no;  //订单号
    private String pid;
    private String account; //用户名称
    private String type;  //类型
    private String operator;  //操作
    private Date create_time;  //创建时间
    private Double money;  //金额
    private String remark;  //备注信息
    private String startTime;
    private String overTime;
}
