package cn.springboot.model;

import cn.springboot.model.page.Pagination;
/**
 * @user eric
 * @date 2018/2/6 15:10
 * @todo
 */
public class OperatorEntity extends Pagination{

    private String userName;
    private String email;
    private String status;
    private String lastLoginTime;
    private String createTime;
    private String roleName;
    private String userId;
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public OperatorEntity() {

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public OperatorEntity(String userName, String email, String status, String lastLoginTime, String createTime) {
        this.userName = userName;
        this.email = email;
        this.status = status;
        this.lastLoginTime = lastLoginTime;
        this.createTime = createTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    @Override
    public String toString() {
        return "OperatorEntity{" +
                "userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", status='" + status + '\'' +
                ", lastLoginTime='" + lastLoginTime + '\'' +
                ", createTime='" + createTime + '\'' +
                '}';
    }
}
