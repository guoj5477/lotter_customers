package cn.springboot.model;





import cn.springboot.model.page.Pagination;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public   class CustomerEntity extends Pagination implements Serializable {
    private Long id;

    private String username;

    private String password;

    private String salt;

    private String phone;

    private BigDecimal balance;
//下分操作对应的请求id
    private String downScoreId;
    private Integer level;
    private String remark;

    public String getDownScoreId() {
        return downScoreId;
    }

    public void setDownScoreId(String downScoreId) {
        this.downScoreId = downScoreId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(String balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    private String balanceBefore;
    /**
     * 推荐人
     */
    private String nickName;

    private String referee;

    private Date lastLoginTime;

    private String lastLoginIp;

    private Date createTime;

    private Integer loginCount;

    private String headImg;

    private String receiveQrcode;

    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getReferee() {
        return referee;
    }

    public void setReferee(String referee) {
        this.referee = referee;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getReceiveQrcode() {
        return receiveQrcode;
    }

    public void setReceiveQrcode(String receiveQrcode) {
        this.receiveQrcode = receiveQrcode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
