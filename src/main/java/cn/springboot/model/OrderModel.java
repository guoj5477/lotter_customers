package cn.springboot.model;


import cn.springboot.model.page.Pagination;

import java.io.Serializable;
import java.util.Date;

/**
 * @author eric
 * @date 2017/9/28/028 9:53
 * @todo  注单查询
 */
public class OrderModel extends Pagination implements Serializable{

    private String  id;
    private String orderId;  //注单号
    private int lotteryId;  //彩票ID
    private int userId; //用户id
    private String account;  //用户名
    private int playId;  //玩法id
    private String actionId;  //期号
    private String betsData;  //下注日期
    private Date betsTime;  //下注时间
    private String betsIp;  //ip
    private double betsOdds;  //下注赔率
    private double betsMoney;  //下注金额
    private double betsBeginMoney;   //下注前金额
    private String  betsAfterMoney;  //下注后金额
    private String port;  //盘口
    private String status;
    private String playName;
    private String startTime; //开始时间
    private String overTime;  //结束时间

    public OrderModel() {
    }

    public OrderModel(int lotteryId, String actionId) {
        this.lotteryId=lotteryId;
        this.actionId=actionId;
    }

    public String getPlayName() {
        return playName;
    }

    public void setPlayName(String playName) {
        this.playName = playName;
    }

    public OrderModel(String id, String orderId, int lotteryId, int userId, String account, int playId, String actionId, String betsData, Date betsTime, String betsIp, double betsOdds, double betsMoney, double betsBeginMoney, String betsAfterMoney, String port, String status, String startTime, String overTime) {
        this.id = id;
        this.orderId = orderId;
        this.lotteryId = lotteryId;
        this.userId = userId;
        this.account = account;
        this.playId = playId;
        this.actionId = actionId;
        this.betsData = betsData;
        this.betsTime = betsTime;
        this.betsIp = betsIp;
        this.betsOdds = betsOdds;
        this.betsMoney = betsMoney;
        this.betsBeginMoney = betsBeginMoney;
        this.betsAfterMoney = betsAfterMoney;
        this.port = port;
        this.status = status;
        this.startTime = startTime;
        this.overTime = overTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(int lotteryId) {
        this.lotteryId = lotteryId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getPlayId() {
        return playId;
    }

    public void setPlayId(int playId) {
        this.playId = playId;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getBetsData() {
        return betsData;
    }

    public void setBetsData(String betsData) {
        this.betsData = betsData;
    }

    public Date getBetsTime() {
        return betsTime;
    }

    public void setBetsTime(Date betsTime) {
        this.betsTime = betsTime;
    }

    public String getBetsIp() {
        return betsIp;
    }

    public void setBetsIp(String betsIp) {
        this.betsIp = betsIp;
    }

    public double getBetsOdds() {
        return betsOdds;
    }

    public void setBetsOdds(double betsOdds) {
        this.betsOdds = betsOdds;
    }

    public double getBetsMoney() {
        return betsMoney;
    }

    public void setBetsMoney(double betsMoney) {
        this.betsMoney = betsMoney;
    }

    public double getBetsBeginMoney() {
        return betsBeginMoney;
    }

    public void setBetsBeginMoney(double betsBeginMoney) {
        this.betsBeginMoney = betsBeginMoney;
    }

    public String getBetsAfterMoney() {
        return betsAfterMoney;
    }

    public void setBetsAfterMoney(String betsAfterMoney) {
        this.betsAfterMoney = betsAfterMoney;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getOverTime() {
        return overTime;
    }

    public void setOverTime(String overTime) {
        this.overTime = overTime;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "OrderModel{" +
                "id=" + id +
                ", orderId='" + orderId + '\'' +
                ", lotteryId=" + lotteryId +
                ", userId=" + userId +
                ", account='" + account + '\'' +
                ", playId=" + playId +
                ", actionId='" + actionId + '\'' +
                ", betsData='" + betsData + '\'' +
                ", betsTime='" + betsTime + '\'' +
                ", betsIp='" + betsIp + '\'' +
                ", betsOdds=" + betsOdds +
                ", betsMoney=" + betsMoney +
                ", betsBeginMoney=" + betsBeginMoney +
                ", betsAfterMoney='" + betsAfterMoney + '\'' +
                ", port='" + port + '\'' +
                ", status=" + status +
                ", startTime='" + startTime + '\'' +
                ", overTime='" + overTime + '\'' +
                '}';
    }
}
