package cn.springboot.model;

import cn.springboot.model.page.Pagination;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author eric
 * @date 2017/9/20/020 12:00
 * @todo
 */

public   class LotteryInfo extends Pagination  implements Serializable{

    private Integer lotteryId;  //彩票id
    private String lotteryName;  //彩票名称
    private Integer lotteryOrder;  //场次
    private String  pid;  //期数
    private String lotteryDate;  //日期
    private String lotteryCode;  //开奖结果
    private String lotteryTime;  //开奖时间
    private String lotteryStatus;  //开奖状态
    private BigDecimal betMoney;  //投注金额
    private BigDecimal winMoney;  //中奖金额
    private BigDecimal resultMoney;  //返点金额
    private String startTime ;//抓取时间
    private String remark; //备注信息

    public String getCancelType() {
        return cancelType;
    }

    public void setCancelType(String cancelType) {
        this.cancelType = cancelType;
    }
    private  String cancelType;//'cancel':取消结算；'reset':重新结算
    private String status; //号码状态

    public String getLottery_pk() {
        return lottery_pk;
    }

    public void setLottery_pk(String lottery_pk) {
        this.lottery_pk = lottery_pk;
    }

    private String lottery_pk;


    public LotteryInfo() {
    }

    public LotteryInfo(Integer lotteryId, String lotteryName, Integer lotteryOrder, String pid, String lotteryDate, String lotteryCode, String lotteryTime, String lotteryStatus, BigDecimal betMoney, BigDecimal winMoney, BigDecimal resultMoney, String startTime, String remark) {
        this.lotteryId = lotteryId;
        this.lotteryName = lotteryName;
        this.lotteryOrder = lotteryOrder;
        this.pid = pid;
        this.lotteryDate = lotteryDate;
        this.lotteryCode = lotteryCode;
        this.lotteryTime = lotteryTime;
        this.lotteryStatus = lotteryStatus;
        this.betMoney = betMoney;
        this.winMoney = winMoney;
        this.resultMoney = resultMoney;
        this.startTime=startTime;
        this.remark = remark;
    }

    public Integer getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(Integer lotteryId) {
        this.lotteryId = lotteryId;
    }

    public String getLotteryName() {
        return lotteryName;
    }

    public void setLotteryName(String lotteryName) {
        this.lotteryName = lotteryName;
    }

    public Integer getLotteryOrder() {
        return lotteryOrder;
    }

    public void setLotteryOrder(Integer lotteryOrder) {
        this.lotteryOrder = lotteryOrder;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getLotteryDate() {
        return lotteryDate;
    }

    public void setLotteryDate(String lotteryDate) {
        this.lotteryDate = lotteryDate;
    }

    public String getLotteryCode() {
        return lotteryCode;
    }

    public void setLotteryCode(String lotteryCode) {
        this.lotteryCode = lotteryCode;
    }

    public String getLotteryTime() {
        return lotteryTime;
    }

    public void setLotteryTime(String lotteryTime) {
        this.lotteryTime = lotteryTime;
    }

    public String getLotteryStatus() {
        return lotteryStatus;
    }

    public void setLotteryStatus(String lotteryStatus) {
        this.lotteryStatus = lotteryStatus;
    }

    public BigDecimal getBetMoney() {
        return betMoney;
    }

    public void setBetMoney(BigDecimal betMoney) {
        this.betMoney = betMoney;
    }

    public BigDecimal getWinMoney() {
        return winMoney;
    }

    public void setWinMoney(BigDecimal winMoney) {
        this.winMoney = winMoney;
    }

    public BigDecimal getResultMoney() {
        return resultMoney;
    }

    public void setResultMoney(BigDecimal resultMoney) {
        this.resultMoney = resultMoney;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "LotteryInfo{" +
                "lotteryId=" + lotteryId +
                ", lotteryName='" + lotteryName + '\'' +
                ", lotteryOrder=" + lotteryOrder +
                ", pid='" + pid + '\'' +
                ", lotteryDate='" + lotteryDate + '\'' +
                ", lotteryCode='" + lotteryCode + '\'' +
                ", lotteryTime='" + lotteryTime + '\'' +
                ", lotteryStatus='" + lotteryStatus + '\'' +
                ", betMoney=" + betMoney +
                ", winMoney=" + winMoney +
                ", resultMoney=" + resultMoney +
                ", startTime='" + startTime + '\'' +
                ", remark='" + remark + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
