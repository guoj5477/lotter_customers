package cn.springboot.model;

import cn.springboot.model.page.Pagination;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


public  class  OperationRecord extends Pagination implements Serializable{
    private int id;
    private String event;
    private String operationTime;
    private String operator;
    private String type;
    private String  ip;
    private String remark;
    private String startTime;
    private String overTime;
    private String userName;
    private String scoreType;
    private BigDecimal scoreBefore;
    private BigDecimal scoreAfter;

    public BigDecimal getScoreBefore() {
        return scoreBefore;
    }

    public void setScoreBefore(BigDecimal scoreBefore) {
        this.scoreBefore = scoreBefore;
    }

    public BigDecimal getScoreAfter() {
        return scoreAfter;
    }

    public void setScoreAfter(BigDecimal scoreAfter) {
        this.scoreAfter = scoreAfter;
    }

    public String getScoreType() {
        return scoreType;
    }

    public void setScoreType(String scoreType) {
        this.scoreType = scoreType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public OperationRecord() {
    }

    public OperationRecord(String event, String type, String operationTime, String operator, String ip, String remark, String userName) {
        this.id = id;
        this.event = event;
        this.operationTime = operationTime;
        this.operator = operator;
        this.type = type;
        this.ip = ip;
        this.remark = remark;
        this.userName = userName;
    }

    public OperationRecord(String event, String type, String operationTime, String operator, String ip, String remark,String startTime,String overTime, String userName) {
        this.id = id;
        this.event = event;
        this.operationTime = operationTime;
        this.operator = operator;
        this.type = type;
        this.ip = ip;
        this.remark = remark;
        this.startTime=startTime;
        this.overTime=overTime;
        this.userName = userName;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(String operationTime) {
        this.operationTime = operationTime;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getOverTime() {
        return overTime;
    }

    public void setOverTime(String overTime) {
        this.overTime = overTime;
    }

    @Override
    public String toString() {
        return "OperationRecord{" +
                "id=" + id +
                ", event='" + event + '\'' +
                ", operationTime='" + operationTime + '\'' +
                ", operator='" + operator + '\'' +
                ", type='" + type + '\'' +
                ", ip='" + ip + '\'' +
                ", remark='" + remark + '\'' +
                ", startTime='" + startTime + '\'' +
                ", overTime='" + overTime + '\'' +
                '}';
    }
}
