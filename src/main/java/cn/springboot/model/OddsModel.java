package cn.springboot.model;

/**
 * @author eric
 * @date 2017/9/12/012 14:32
 * @todo  赔率
 */
public class OddsModel {

    private long id;
    private int lid; //彩票ID
    private String balls;  //球号
    private String rule;  //玩法
    private String status; //状态
    private String name;  //说明
    private double odds;  //赔率

    public OddsModel() {
    }

    public OddsModel(long id, int lid, String balls, String rule, String status, String name, double odds) {
        this.id = id;
        this.lid = lid;
        this.balls = balls;
        this.rule = rule;
        this.status = status;
        this.name = name;
        this.odds = odds;
    }

    public OddsModel(int lid, String balls, String rule, double odds) {
        this.lid = lid;
        this.balls = balls;
        this.rule = rule;
        this.odds = odds;
    }
    public OddsModel(int lid, String balls, String rule, double odds, String name) {
        this.lid = lid;
        this.balls = balls;
        this.rule = rule;
        this.odds = odds;
        this.name=name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getLid() {
        return lid;
    }

    public void setLid(int lid) {
        this.lid = lid;
    }

    public String getBalls() {
        return balls;
    }

    public void setBalls(String balls) {
        this.balls = balls;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getOdds() {
        return odds;
    }

    public void setOdds(double odds) {
        this.odds = odds;
    }

    @Override
    public String toString() {
        return "OddsModel{" +
                "id=" + id +
                ", lid=" + lid +
                ", balls='" + balls + '\'' +
                ", rule='" + rule + '\'' +
                ", status='" + status + '\'' +
                ", name='" + name + '\'' +
                ", odds=" + odds +
                '}';
    }
}
