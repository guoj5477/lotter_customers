package cn.springboot.config.shiro.vo;

import java.util.ArrayList;
import java.util.List;

import cn.springboot.common.constants.Constants;
import cn.springboot.model.auth.Permission;

public enum PermissionEnumUtil {

    首页("首页", "fa-home", "glsy", null, "index", 1, 1),

    会员管理("会员管理", "", "hygl", null, null, 1, 10),
    会员查询("会员查询", "", "hycx", "hygl", "view/customerManage/query", 2, 11),
    下分请求处理("下分请求处理", "", "xfqq", "hygl", "downScore/queryDownlist", 2, 12),

    日志管理("日志管理", "", "rzgl", null, null, 1, 20),
    操作记录("操作记录", "", "czjl", "rzgl", "view/record/operationRecord", 2, 21),
    开奖记录("开奖记录", "", "kjjl", "rzgl", "lottery/queryLotteryRec", 2, 22),

    后台管理("后台管理", "", "htgl", null, null, 1, 30),
    新增管理员("新增管理员", "", "xzgly", "htgl", "view/operator/addOperator", 2, 31),

    开奖管理("开奖管理", "", "kjgl", null, null, 1, 40),
    加拿大28("加拿大28", "", "jnd28", "kjgl", "lottery/queryLotteryCanada28", 2, 41),
    北京28("北京28", "", "bj28", "kjgl", "view/operator/addOperator", 2, 42),

    赔率设置("赔率设置", "", "plsz", null, null, 1, 50),
    加拿大28_pl("加拿大28", "", "jnd28_pl", "plsz", "view/operator/addOperator", 2, 51),
    北京28_pl("北京28", "", "bj28_pl", "plsz", "view/operator/addOperator", 2, 52),

    统计报表("统计报表", "", "tjbb", null, null, 1, 60),
    注单查询("注单查询", "", "zdcx", "tjbb", "report/getBetsOrder", 2, 61);

    private  PermissionEnumUtil(String name, String cssClass, String key, String parentKey, String url, Integer lev, Integer sort) {
        this.name = name;
        this.cssClass = cssClass;
        this.key = key;
        this.lev=lev;
        this.sort = sort;
        this.url = url;
        this.hide = Constants.STATUS_VALID;
        this.parentKey = parentKey;
    }
    
    public static List<Permission> getPermissions(){
        List<Permission> list = new ArrayList<>();
        PermissionEnumUtil[] pers = PermissionEnumUtil.values();
        Permission per = null;
        for (PermissionEnumUtil p : pers) {
            per = new Permission();
            per.setName(p.getName());
            per.setCssClass(p.getCssClass());
            per.setKey(p.getKey());
            per.setHide(p.getHide());
            per.setUrl(p.getUrl());
            per.setLev(p.getLev());
            per.setSort(p.getSort());
            per.setParentKey(p.getParentKey());
            list.add(per);
        }
        return list;
    }

    private String name;
    private String cssClass;
    private String key;
    private Integer hide;
    private String url;
    private Integer lev;
    private Integer sort;
    private String parentKey;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getHide() {
        return hide;
    }

    public void setHide(Integer hide) {
        this.hide = hide;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getLev() {
        return lev;
    }

    public void setLev(Integer lev) {
        this.lev = lev;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getParentKey() {
        return parentKey;
    }

    public void setParentKey(String parentKey) {
        this.parentKey = parentKey;
    }

}
