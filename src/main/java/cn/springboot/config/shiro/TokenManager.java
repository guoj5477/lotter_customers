package cn.springboot.config.shiro;

import cn.springboot.common.util.MD5Utils;
import cn.springboot.model.auth.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;

public class TokenManager {

    /**
     * 登录
     */
    public static User login(User user){
        UsernamePasswordToken token = new UsernamePasswordToken(user.getUsername(),MD5Utils.encode(user.getUsername(),user.getPassword()));
        SecurityUtils.getSubject().login(token);
        return getToken();
    }

    /**
     * 获取当前登录的用户User对象
     * @return
     */
    public static User getToken(){
        User token = (User) SecurityUtils.getSubject().getPrincipal();
        return token ;
    }

    /**
     * 获取当前用户的Session
     * @return
     */
    public static Session getSession(){
        return SecurityUtils.getSubject().getSession();
    }

    /**
     * 把值放入到当前登录用户的Session里
     * @param key
     * @param value
     */
    public static void setVal2Session(Object key ,Object value){
        getSession().setAttribute(key, value);
    }

    /**
     * 从当前登录用户的Session里取值
     * @param key
     * @return
     */
    public static Object getVal2Session(Object key){
        return getSession().getAttribute(key);
    }






}
