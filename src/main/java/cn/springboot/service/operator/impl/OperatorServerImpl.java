package cn.springboot.service.operator.impl;

import cn.springboot.mapper.operator.OperatorMapper;
import cn.springboot.model.OperatorEntity;
import cn.springboot.model.page.Pagination;
import cn.springboot.service.operator.OperatorServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @user eric
 * @date 2018/2/6 15:23
 * @todo
 */
@Service
public class OperatorServerImpl implements OperatorServer {

    @Autowired
    private OperatorMapper operatorMapper;

    @Override
    public Pagination<OperatorEntity> queryOperator(Integer pageNo, Integer pageSize) {
        OperatorEntity entity = new OperatorEntity();
        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
        Pagination page = new Pagination();
        page.setPageNo(pageNo);
        page.setPageSize(pageSize);

        int offset = (page.getPageNo() - 1) * page.getPageSize();

        entity.setPageNo(pageNo);
        entity.setPageSize(pageSize);
        entity.setFilterNo(offset);
        List<OperatorEntity> list=new ArrayList();
        int total=0;
        list = operatorMapper.queryOperators(entity);
        total = operatorMapper.queryOperatorsForTotal(entity).intValue();
        Pagination<OperatorEntity>  pagination=new Pagination<OperatorEntity>();
        pagination.setList(list);
        pagination.setPageSize(pageSize);
        pagination.setPageNo(pageNo);
        pagination.setTotalCount(total);
        return pagination;
    }
}
