package cn.springboot.service.operator;

import cn.springboot.model.OperatorEntity;
import cn.springboot.model.page.Pagination;

import java.util.List;

/**
 * @user eric
 * @date 2018/2/6 15:21
 * @todo
 */
public interface OperatorServer {
   Pagination<OperatorEntity> queryOperator(Integer pageNo, Integer pageSize);

}
