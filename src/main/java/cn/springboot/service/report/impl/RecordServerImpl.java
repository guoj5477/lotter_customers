package cn.springboot.service.report.impl;

import cn.springboot.mapper.report.RecordMapper;
import cn.springboot.model.OperationRecord;
import cn.springboot.model.page.Pagination;
import cn.springboot.service.report.RecordServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecordServerImpl implements RecordServer {

    @Autowired
    private RecordMapper recordMapper;

    @Override
    public Pagination<OperationRecord> queryOperationRecord(OperationRecord operationRecord, Integer pageNo, Integer pageSize) {
        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
        Pagination page = new Pagination();
        page.setPageNo(pageNo);
        page.setPageSize(pageSize);

        int offset = (page.getPageNo() - 1) * page.getPageSize();

        operationRecord.setPageNo(pageNo);
        operationRecord.setPageSize(pageSize);
        operationRecord.setFilterNo(offset);

        List<OperationRecord> list= recordMapper.queryOperationRecord(operationRecord);

        //查询总记录数
        int total=recordMapper.queryOperationRecordTotal(operationRecord).intValue();

        Pagination<OperationRecord> pagination=new Pagination();
        pagination.setList(list);
        pagination.setTotalCount(total);
        pagination.setPageNo(pageNo);
        pagination.setPageSize(pageSize);

        return pagination;
    }

    /**
     * 新增操作记录
     * @param operationRecord
     */
    @Override
    public void insertOperationRecord(OperationRecord operationRecord) {
        recordMapper.insertOperationRecord(operationRecord);
    }

}
