package cn.springboot.service.report;

import cn.springboot.model.OperationRecord;
import cn.springboot.model.page.Pagination;


public interface RecordServer {

    void insertOperationRecord(OperationRecord operationRecord);

    Pagination<OperationRecord> queryOperationRecord(OperationRecord operationRecord, Integer pageNo, Integer pageSize);
}
