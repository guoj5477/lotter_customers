package cn.springboot.service.customer;

import cn.springboot.model.CustomerEntity;
import cn.springboot.model.DownLogBean;
import cn.springboot.model.page.Pagination;

import java.util.List;

public interface DownScoreService {
    Pagination<DownLogBean> findDownlist(DownLogBean customerEntity, Integer pageNo, Integer pageSize);
    void updateDownLogStatus(DownLogBean downLogBean) throws Exception;
    List<DownLogBean> findDownlist(DownLogBean customerEntity);
}
