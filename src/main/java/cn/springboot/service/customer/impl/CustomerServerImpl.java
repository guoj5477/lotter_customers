package cn.springboot.service.customer.impl;

import cn.springboot.common.util.DateUtil;
import cn.springboot.common.util.DateUtils;
import cn.springboot.common.util.IPUtils;
import cn.springboot.controller.DownScoreController;
import cn.springboot.mapper.customer.CustomerMapper;
import cn.springboot.mapper.customer.DownScoreMapper;
import cn.springboot.mapper.report.RecordMapper;
import cn.springboot.model.CustomerEntity;
import cn.springboot.model.DownLogBean;
import cn.springboot.model.OperationRecord;
import cn.springboot.model.auth.User;
import cn.springboot.model.page.Pagination;
import cn.springboot.service.customer.CustomerServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class CustomerServerImpl implements CustomerServer {

    private Logger logger= LoggerFactory.getLogger(CustomerServerImpl.class);
    @Autowired
    private CustomerMapper customerMapper;

    @Autowired
    private RecordMapper recordMapper;
    @Autowired
    private DownScoreMapper downScoreMapper;
    @Override
    public CustomerEntity findCustomerByAccount(CustomerEntity customerEntity) {
        return customerMapper.findCustomerByAccount(customerEntity);
    }

    @Override
    public Boolean register(CustomerEntity customerEntity) {
        Boolean flag;
        try {
            customerMapper.register(customerEntity);
            flag= Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            flag=Boolean.FALSE;
        }
        return flag;

    }

    @Override
    public Pagination<CustomerEntity> findCustomer(CustomerEntity customerEntity, Integer pageNo, Integer pageSize) {

        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
        Pagination page = new Pagination();
        page.setPageNo(pageNo);
        page.setPageSize(pageSize);

        int offset = (page.getPageNo() - 1) * page.getPageSize();

        customerEntity.setPageNo(pageNo);
        customerEntity.setPageSize(pageSize);
        customerEntity.setFilterNo(offset);


        List<CustomerEntity> list= customerMapper.findCustomer(customerEntity);

        //查询总记录数
        int total=customerMapper.findCustomerCount(customerEntity);

        Pagination<CustomerEntity> pagination=new Pagination();
        pagination.setList(list);
        pagination.setTotalCount(total);
        pagination.setPageNo(pageNo);
        pagination.setPageSize(pageSize);

        return pagination;

    }

    @Override
    @Transactional
    public Boolean addScore(CustomerEntity customerEntity, HttpServletRequest request,String operator, String userName) {
        Boolean flag;
        try {
            String event=null;
            String scoreType = "";
            if(customerEntity.getBalance().compareTo(BigDecimal.ZERO)==-1){
                event="玩家:["+customerEntity.getUsername()+"]下分:"+customerEntity.getBalance();
                scoreType = "1";
            }else {
                event="玩家:["+customerEntity.getUsername()+"]上分:"+customerEntity.getBalance();
                scoreType = "0";
            }
            customerMapper.addScore(customerEntity);
            if(customerEntity.getBalance().compareTo(BigDecimal.ZERO)==-1){
                DownLogBean down = new DownLogBean();
                String id = customerEntity.getDownScoreId().substring(0,customerEntity.getDownScoreId().indexOf("_"));
                String createTimeStr = customerEntity.getDownScoreId().substring(customerEntity.getDownScoreId().indexOf("_")+1);
                down.setId(id);
                down.setStatus("F");
                logger.info("##会员:用户名:【"+customerEntity.getUsername()+"】，申请提现时间为:【"+createTimeStr+"】,正在更改处理状态:原状态为:【处理中】#######新状态为【已完成】");
                downScoreMapper.updateDownLogStatus_Finish(down);
            }
            OperationRecord op = new OperationRecord(event,"上下分",DateUtils.getNow("yyyy-MM-dd HH:mm:ss"),operator, IPUtils.getIpAddr(request),customerEntity.getNickName(), userName);
            op.setScoreType(scoreType);
            if(customerEntity.getBalanceBefore() != null && !customerEntity.getBalanceBefore().equals("") && !customerEntity.getBalanceBefore().equals("null")){
                BigDecimal balanceBefore = new BigDecimal(customerEntity.getBalanceBefore().replaceAll(",",""));
                op.setScoreBefore(balanceBefore);
                op.setScoreAfter(customerEntity.getBalance().add(balanceBefore));
            }
            op.setRemark(customerEntity.getRemark());
            recordMapper.insertOperationRecordForCustomer(op);

            flag=Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            flag=Boolean.FALSE;
        }
        return flag;
    }
    @Override
    public Boolean disableCustomer(CustomerEntity customerEntity,HttpServletRequest request,String operator,String event,String type, String userName) {
        Boolean flag;
        try {
            customerMapper.disableCustomer(customerEntity);
            recordMapper.insertOperationRecord(new OperationRecord(event,type,DateUtils.getNow("yyyy-MM-dd HH:mm:ss"),operator, IPUtils.getIpAddr(request),customerEntity.getNickName(), userName));
            flag=Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            flag=Boolean.FALSE;
        }
        return flag;
    }

    @Override
    public User login(String userName) {
        return customerMapper.login(userName);
    }

    @Override
    public void updateOperatorByUsername(User user) {
        customerMapper.updateOperatorByUsername(user);
    }
}
