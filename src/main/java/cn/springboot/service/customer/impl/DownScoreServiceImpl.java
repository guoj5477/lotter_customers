package cn.springboot.service.customer.impl;

import cn.springboot.mapper.customer.CustomerMapper;
import cn.springboot.mapper.customer.DownScoreMapper;
import cn.springboot.mapper.report.RecordMapper;
import cn.springboot.model.CustomerEntity;
import cn.springboot.model.DownLogBean;
import cn.springboot.model.page.Pagination;
import cn.springboot.service.customer.DownScoreService;
import com.sun.scenario.effect.impl.sw.java.JSWBlend_EXCLUSIONPeer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DownScoreServiceImpl implements DownScoreService {
    @Autowired
    private DownScoreMapper downScoreMapper;
    @Override
    public void updateDownLogStatus(DownLogBean logBean) throws Exception{
        try{
            downScoreMapper.updateDownLogStatus_Process(logBean);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Pagination<DownLogBean> findDownlist(DownLogBean customerEntity, Integer pageNo, Integer pageSize){
        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
        Pagination page = new Pagination();
        page.setPageNo(pageNo);
        page.setPageSize(pageSize);

        int offset = (page.getPageNo() - 1) * page.getPageSize();

        customerEntity.setPageNo(pageNo);
        customerEntity.setPageSize(pageSize);
        customerEntity.setFilterNo(offset);


        List<DownLogBean> list = downScoreMapper.findCustomer(customerEntity);

        //查询总记录数
        int total=downScoreMapper.findCustomerCount(customerEntity);

        Pagination<DownLogBean> pagination=new Pagination();
        pagination.setList(list);
        pagination.setTotalCount(total);
        pagination.setPageNo(pageNo);
        pagination.setPageSize(pageSize);

        return pagination;
    }
    @Override
    public List<DownLogBean> findDownlist(DownLogBean customerEntity){
        return downScoreMapper.findCustomer(customerEntity);
    }
}
