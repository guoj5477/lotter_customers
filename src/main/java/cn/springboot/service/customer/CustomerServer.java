package cn.springboot.service.customer;

import cn.springboot.model.CustomerEntity;
import cn.springboot.model.auth.User;
import cn.springboot.model.page.Pagination;

import javax.servlet.http.HttpServletRequest;


public interface CustomerServer {

   CustomerEntity findCustomerByAccount(CustomerEntity customerEntity);

   Boolean register(CustomerEntity customerEntity);

   Pagination<CustomerEntity> findCustomer(CustomerEntity customerEntity, Integer pageNo, Integer pageSize);

   Boolean addScore(CustomerEntity customerEntity, HttpServletRequest request,String operator, String userName);

   Boolean disableCustomer(CustomerEntity customerEntity,HttpServletRequest request,String operator,String event,String type, String userName);

   User login(String userName);

   void updateOperatorByUsername(User user);
}
