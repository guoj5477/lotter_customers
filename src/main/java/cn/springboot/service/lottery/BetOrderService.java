package cn.springboot.service.lottery;

import cn.springboot.model.OrderModel;

import java.util.List;
import cn.springboot.model.page.Pagination;
/**
 * @author eric
 * @date 2017/10/17/017 9:23
 * @todo
 */
public interface BetOrderService {
    Pagination<OrderModel> getBetsOrder(OrderModel orderModel, Integer pageNo, Integer pageSize);

    List<OrderModel> queryBetStatus(OrderModel orderModel);
}
