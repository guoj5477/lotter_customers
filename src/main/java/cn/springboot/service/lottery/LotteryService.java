package cn.springboot.service.lottery;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;

import cn.springboot.model.AwardLogBean;
import cn.springboot.model.LotteryInfo;
import cn.springboot.model.page.Pagination;
import cn.springboot.common.exception.LottException;
public interface LotteryService {

    Pagination<LotteryInfo> queryLotteryByCanadaPCDD(LotteryInfo lotteryInfo,Integer pageNo,Integer pageSize)throws  LottException;
    //查询起始场次id
    public int queryStartPid(int lotteryId);
    /**按期号查询彩票信息**/
    List<LotteryInfo> queryLotteryInfoByPid(LotteryInfo lotteryInfo);

    //删除彩票
 /*   Boolean deleteLotteryById(LotteryOperationRecord lotteryOperationRecord);*/

    //手动开奖
    void insertLotteryData(LotteryInfo lotteryInfo,String operator,HttpServletRequest request, String userName);
    //查询是否已经开奖
    Integer queryLotteryData(LotteryInfo lotteryInfo);

    /**修改彩票开奖结果*/
    void updateLottCode(LotteryInfo lotteryInfo,String operator,HttpServletRequest request, String userName);

    void cancelSettlement(LotteryInfo lotteryInfo,String operator,HttpServletRequest request, String userName);

    List<LotteryInfo>listenerLotteryDate();

    List<LotteryInfo> listenerLotteryDateByNotOpen(Integer  lotteryId);

    Pagination<AwardLogBean> queryLotteryRec(AwardLogBean lotteryInfo, Integer pageNo, Integer pageSize) throws LottException;




}