package cn.springboot.service.lottery.impl;

import cn.springboot.service.lottery.BetOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.springboot.model.OrderModel;
import cn.springboot.model.page.Pagination;
import java.util.List;
import cn.springboot.mapper.lottery.BetOrderDao;
/**
 * @author eric
 * @date 2017/10/17/017 9:23
 * @todo
 */
@Service
public class BetOrderServiceImpl implements BetOrderService {

    @Autowired
    private BetOrderDao betOrderDao;
    @Override
    public Pagination<OrderModel> getBetsOrder(OrderModel orderModel,Integer pageNo,Integer pageSize) {


        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
        Pagination page = new Pagination();
        page.setPageNo(pageNo);
        page.setPageSize(pageSize);

        int offset = (page.getPageNo() - 1) * page.getPageSize();

        orderModel.setPageNo(pageNo);
        orderModel.setPageSize(pageSize);
        orderModel.setFilterNo(offset);

        List<OrderModel> list=betOrderDao.getBetsOrder(orderModel);
        System.out.println(list);
        //查询数据量
        int total=betOrderDao.getBetsOrderCount(orderModel);

        Pagination<OrderModel> pagination=new Pagination();
        pagination.setList(list);
        pagination.setTotalCount(total);
        pagination.setPageNo(pageNo);
        pagination.setPageSize(pageSize);

        return pagination;
    }

    @Override
    public List<OrderModel> queryBetStatus(OrderModel orderModel) {
        return betOrderDao.queryBetStatus(orderModel);
    }
}
