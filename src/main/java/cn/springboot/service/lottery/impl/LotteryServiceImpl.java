package cn.springboot.service.lottery.impl;



import cn.springboot.common.exception.LottException;
import cn.springboot.common.util.DateUtils;
import cn.springboot.common.util.IPUtils;
import cn.springboot.common.util.UUIDUtil;
import cn.springboot.mapper.lottery.LotteryDao;
import cn.springboot.mapper.lottery.OrderCountDao;
import cn.springboot.mapper.lottery.ReportDao;
import cn.springboot.model.AwardLogBean;
import cn.springboot.model.LotteryInfo;
import cn.springboot.model.page.Pagination;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cn.springboot.model.OperationRecord;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

import cn.springboot.service.lottery.LotteryService;

@Service("lotteryManageService")
public class LotteryServiceImpl implements LotteryService {
    @Autowired
    private ReportDao reportDao;
    @Autowired
    private LotteryDao lotteryDao;
    Log log= LogFactory.getLog(LotteryServiceImpl.class);
    @Autowired
    private OrderCountDao orderCountDao;
    @Override
    public Pagination<AwardLogBean> queryLotteryRec(AwardLogBean lotteryInfo, Integer pageNo, Integer pageSize) throws LottException{
        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
        Pagination page = new Pagination();
        page.setPageNo(pageNo);
        page.setPageSize(pageSize);

        int offset = (page.getPageNo() - 1) * page.getPageSize();

        lotteryInfo.setPageNo(pageNo);
        lotteryInfo.setPageSize(pageSize);
        lotteryInfo.setFilterNo(offset);
        List<AwardLogBean> list=new ArrayList();
        int total=0;
        list = lotteryDao.queryLotteryRec(lotteryInfo);
        total = lotteryDao.queryLotteryRecForTotal(lotteryInfo).intValue();
        Pagination<AwardLogBean>  pagination=new Pagination<AwardLogBean>();
        pagination.setList(list);
        pagination.setPageSize(pageSize);
        pagination.setPageNo(pageNo);
        pagination.setTotalCount(total);
        return pagination;
    }
    @Override
    public Pagination<LotteryInfo> queryLotteryByCanadaPCDD(LotteryInfo lotteryInfo, Integer pageNo, Integer pageSize) throws LottException {
        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
        Pagination page = new Pagination();
        page.setPageNo(pageNo);
        page.setPageSize(pageSize);

        int offset = (page.getPageNo() - 1) * page.getPageSize();

        lotteryInfo.setPageNo(pageNo);
        lotteryInfo.setPageSize(pageSize);
        lotteryInfo.setFilterNo(offset);
        List<LotteryInfo> list=new ArrayList();
        int total=0;
        BigDecimal pageTotalBetMoney=new BigDecimal(0);   //本页总投注金额
        BigDecimal pageTotalWinMoney=new BigDecimal(0);  //本页总赢钱金额

        BigDecimal dayTotalBetMoney=new BigDecimal(0);
        BigDecimal dayTotalWinMoney=new BigDecimal(0);

        BigDecimal countMoneyByDay=new BigDecimal(0);   //当天投注总金额
        BigDecimal winMoneyByDay=new BigDecimal(0);  //当天总盈利金额
        if(lotteryInfo.getLotteryId()==null){
            throw  new LottException("彩票ID为空");
        }
        list=lotteryDao.queryLotteryByCanadaPCDD(lotteryInfo);
        total= lotteryDao.queryLotteryTotal(lotteryInfo).intValue();
        //统计输赢值
        /*for(int i=0;i<list.size();i++){
            if(StringUtils.isNotBlank(list.get(i).getPid())){

                BigDecimal countMoney=orderCountDao.getOrderMoney(list.get(i));


                BigDecimal winMoney=orderCountDao.getOrderWinMoney(list.get(i));
                list.get(i).setBetMoney(countMoney);
                list.get(i).setWinMoney(winMoney);
                if(countMoney!=null){
                    pageTotalBetMoney=pageTotalBetMoney.add(countMoney);
                }
                if(winMoney!=null){
                    pageTotalWinMoney=pageTotalWinMoney.add(winMoney);
                }



            }

        }*/
        Pagination<LotteryInfo>  pagination=new Pagination<>();
        pagination.setList(list);
        pagination.setPageSize(pageSize);
        pagination.setPageNo(pageNo);
        pagination.setTotalCount(total);
        pagination.setPageTotalBetMoney(pageTotalBetMoney);
        pagination.setPageTotalWinMoney(pageTotalWinMoney);
        pagination.setDayTotalBetMoney(dayTotalBetMoney);
        pagination.setDayTotalWinMoney(dayTotalWinMoney);

        return pagination;
    }

    @Override
    public int queryStartPid(int lotteryId) {
        return 0;
    }

    @Override
    public List<LotteryInfo> queryLotteryInfoByPid(LotteryInfo lotteryInfo) {
        return null;
    }

    @Override
    @Transactional
    public void insertLotteryData(LotteryInfo lotteryInfo, String operator, HttpServletRequest request,String userName) {

        int lid=lotteryInfo.getLotteryId();
        lotteryInfo.setStartTime(DateUtils.getNow("yyyy-MM-dd HH:mm:ss"));
        String remark="手动开奖:"+lotteryInfo.getRemark();
        lotteryInfo.setRemark(remark);
        lotteryInfo.setLottery_pk(UUIDUtil.getRandom32PK());
        String event="";
        switch (lid){
            case 10:
                //lotteryDao.updateLottCode(lotteryInfo);
                //派彩
                lotteryDao.settleOrderByCan28(lotteryInfo);
                event="加拿大28期号: "+lotteryInfo.getLotteryOrder()+" 手动开奖,开奖号码:"+lotteryInfo.getLotteryCode();
                log.info("管理员["+operator+"];"+event);
                break;
            case 11:
               // lotteryDao.insertLotteryData(lotteryInfo);
                //派彩
                lotteryDao.settleOrderByCan28(lotteryInfo);
                event="北京28期号: "+lotteryInfo.getLotteryOrder()+" 手动开奖,开奖号码:"+lotteryInfo.getLotteryCode();
                log.info("管理员["+operator+"];"+event);
                break;
        }
//        lotteryDao.updateLottCode(lotteryInfo);
        reportDao.insertOperationRecord(new OperationRecord(event,"手动开奖",DateUtils.getNow("yyyy-MM-dd HH:mm:ss"),operator, IPUtils.getIpAddr(request),lotteryInfo.getRemark(),userName));
    }

    @Override
    public Integer queryLotteryData(LotteryInfo lotteryInfo) {
        return lotteryDao.queryLotteryData(lotteryInfo);
    }

    @Override
    public void updateLottCode(LotteryInfo lotteryInfo, String operator, HttpServletRequest request, String userName) {


        //查询修改前的开奖号码

        String lotteryCode=lotteryDao.queryLotteryCodeByPid(lotteryInfo);
        String remark="重新结算:"+lotteryInfo.getRemark();
        lotteryInfo.setRemark(remark);
        String event="";
        int lotteryId=lotteryInfo.getLotteryId();
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("lotteryOrder",lotteryInfo.getLotteryOrder());
        map.put("lotteryCode",lotteryInfo.getLotteryCode());
        map.put("cancelType",lotteryInfo.getCancelType());

        switch (lotteryId){
            case 10:
                //                    TODO
                lotteryDao.cancelByCan28(map);
                lotteryDao.settleOrderByCan28(lotteryInfo);
                event = "加拿大28期号: " + lotteryInfo.getLotteryOrder() + " 修改开奖号码,修改前号码:" + lotteryCode + "修改后号码:" + lotteryInfo.getLotteryCode();
                log.info("管理员[" + operator + "];" + event);
                break;
            case 11:
                //                   TODO
                lotteryDao.cancelByCan28(map);
                lotteryDao.settleOrderByCan28(lotteryInfo);
                event = "北京28期号: " + lotteryInfo.getLotteryOrder() + " 修改开奖号码,修改前号码:" + lotteryCode + "修改后号码:" + lotteryInfo.getLotteryCode();
                log.info("管理员[" + operator + "];" + event);
                break;
        }

//        lotteryDao.updateLottCode(lotteryInfo);

        reportDao.insertOperationRecord(new OperationRecord(event,"修改开奖",DateUtils.getNow("yyyy-MM-dd HH:mm:ss"),operator, IPUtils.getIpAddr(request),lotteryInfo.getRemark(),userName));
    }

    @Override
    public void cancelSettlement(LotteryInfo lotteryInfo, String operator, HttpServletRequest request, String userName) {
        String event="";
        int lotteryId=lotteryInfo.getLotteryId();
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("lotteryOrder",lotteryInfo.getLotteryOrder());
        map.put("lotteryCode",lotteryInfo.getLotteryCode());
        map.put("cancelType",lotteryInfo.getCancelType());
        switch (lotteryId){
            case 10:
                lotteryDao.cancelByCan28(map);
                event="加拿大28期号: "+lotteryInfo.getLotteryOrder()+"取消结算";
                log.info("管理员["+operator+"];"+event);
                break;
            case 11:
                lotteryDao.cancelByCan28(map);
                event="北京28期号: "+lotteryInfo.getLotteryOrder()+"取消结算";
                log.info("管理员["+operator+"];"+event);
                break;
        }

//        lotteryDao.updateLottCodeForCancel(lotteryInfo);
        reportDao.insertOperationRecord(new OperationRecord(event,"取消结算", DateUtils.getNow("yyyy-MM-dd HH:mm:ss"),operator, IPUtils.getIpAddr(request),lotteryInfo.getRemark(), userName));
    }

    @Override
    public List<LotteryInfo> listenerLotteryDate() {
        return null;
    }

    @Override
    public List<LotteryInfo> listenerLotteryDateByNotOpen(Integer lotteryId) {
        return null;
    }
}
