package cn.springboot.service.odds;


import cn.springboot.model.OddsModel;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author eric
 * @date 2017/9/12/012 14:43
 * @todo
 */
public interface OddsService {
    //查询彩票赔率
    List<OddsModel> queryOdds(OddsModel oddsModel);

    //修改彩票赔率
    void updateLotteryOdds(OddsModel oddsModel);

    //批量修改彩票赔率

    void updateLotteryOddsbyList(List<OddsModel> list, String operator, HttpServletRequest request, String event, String userName);
}
