package cn.springboot.service.odds.impl;

import cn.springboot.common.util.DateUtils;
import cn.springboot.common.util.IPUtils;
import cn.springboot.mapper.lottery.ReportDao;
import cn.springboot.mapper.odds.OddsDao;
import cn.springboot.model.OddsModel;
import cn.springboot.model.OperationRecord;
import cn.springboot.service.odds.OddsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.beans.Transient;
import java.util.List;

/**
 * @author eric
 * @date 2017/9/12/012 14:43
 * @todo
 */
@Service
public class OddsServiceImpl implements OddsService {

    @Autowired
    private OddsDao oddsDao;
    @Autowired
    private ReportDao reportDao;

    @Override
    public List<OddsModel> queryOdds(OddsModel oddsModel) {
        return oddsDao.queryOdds(oddsModel);
    }

    @Override
    public void updateLotteryOdds(OddsModel oddsModel) {
        oddsDao.updateLotteryOdds(oddsModel);
    }

    @Override
    @Transient
    public void updateLotteryOddsbyList(List<OddsModel> list, String operator, HttpServletRequest request,String event, String userName) {
        oddsDao.updateLotteryOddsbyList(list);
        reportDao.insertOperationRecord(new OperationRecord(event,"修改赔率", DateUtils.getNow("yyyy-MM-dd HH:mm:ss"),operator, IPUtils.getIpAddr(request),null, userName));

    }
}
