package cn.springboot.controller;


import cn.springboot.config.shiro.TokenManager;
import cn.springboot.config.shiro.vo.Principal;
import cn.springboot.model.OddsModel;
import cn.springboot.model.auth.User;
import cn.springboot.service.odds.OddsService;
import com.alibaba.fastjson.JSONObject;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author eric
 * @date 2017/9/11/011 14:05
 * @todo  赔率设置
 */
@Controller
@RequestMapping(value = "odds")

public class OddsController extends BaseController{
    @Autowired
    private OddsService oddsService;

    private Logger logger= Logger.getLogger(OddsController.class);
    /**
     * 赔率设置
     * @return
     */
    @RequestMapping(value = "/Canada28oddsSet",method = RequestMethod.GET)
    public String Canada28oddsSet(HttpServletRequest request, ModelMap modelMap){
       logger.info("【##】 Start Query CQSSCOddsSet / 查询加拿大28赔率");
        String json = null;
        try {
            int lotteryId=10;
            List<OddsModel> list= oddsService.queryOdds(new OddsModel(lotteryId,null,null,0));
           json = JSONObject.toJSONString(list);
            logger.info("【##】 Result / 返回结果:"+json);
            logger.info("【##】End Query 加拿大28 / 结束查询");
            modelMap.addAttribute("list",list);
        } catch (NumberFormatException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return  "view/odds/canada28Odds";
    }
    @RequestMapping(value = "/bj28OddsSet",method = RequestMethod.GET)
    public String bj28OddsSet(HttpServletRequest request, ModelMap modelMap){
        //获取彩票ID
        logger.info("【##】 Start Query XYNCOddsSet / 查询北京28赔率");
        String json = null;
        try {
            int lotteryId = 11;
            List<OddsModel> list= oddsService.queryOdds(new OddsModel(lotteryId,null,null,0));
            json = JSONObject.toJSONString(list);
            logger.info("【##】 Result / 返回结果:"+json);
            logger.info("【##】End Query 北京28 / 结束查询");
            modelMap.addAttribute("list",list);
        } catch (NumberFormatException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return "view/odds/bj28Odds";
    }
    @RequestMapping(value = "/updateOdds",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> updateOdds(String jsonStr,HttpServletRequest request){
        HttpSession session=request.getSession();
        Principal principal = (Principal)SecurityUtils.getSubject().getPrincipal();
        User user = principal.getUser();
        if(user==null){
            logger.info("## operator is null !");
            resultMap.put("success",false);
            resultMap.put("message","你已退出,请重新登录!");
            return resultMap;
        }
        logger.info("【##】 /"+ user.getUsername()+"/Start Update Odds /修改赔率");
        String js=jsonStr.replaceAll("\\\\","");
        String str=js.substring(1,js.length()-1);
        logger.info(js);
        com.alibaba.fastjson.JSONObject obj = com.alibaba.fastjson.JSONObject.parseObject(js);
        List<Map<String,Object>> jsonArray = (List<Map<String,Object>>) obj.get("jsonList");
        String lid = obj.getString("lid");
        logger.info(jsonArray);
        List <OddsModel>  list=new ArrayList<>();
        int lotteryId=Integer.parseInt(lid);
        try {
            for(int i=0;i<jsonArray.size();i++){
                JSONObject jsonObject2=(JSONObject)jsonArray.get(i);
                if(jsonObject2.getString("lid")!=""&&jsonObject2.getString("odds")!=""){
                    OddsModel mod = new OddsModel();
                    mod.setLid(lotteryId);
                    mod.setOdds(Double.parseDouble(jsonObject2.getString("oddVal")));
                    String name = jsonObject2.getString("name");
                    String ruleName = "";
                    if(lotteryId == 10){
                        //特码
                        if(name.indexOf("HZ_") != -1){
                            ruleName = name.substring("HZ_".length());
                        }else if(name.equals("DX_D")){
                            ruleName = "大";
                        }else if(name.equals("DS_D")){
                            ruleName = "单";
                        }else if(name.equals("JZDX_D")){
                            ruleName = "极大";
                        }else if(name.equals("DXDS_DD")){
                            ruleName = "大单";
                        }else if(name.equals("DXDS_DS")){
                            ruleName = "大双";
                        }else if(name.equals("DX_X")){
                            ruleName = "小";
                        }else if(name.equals("DS_S")){
                            ruleName = "双";
                        }else if(name.equals("JZDX_X")){
                            ruleName = "极小";
                        }else if(name.equals("DXDS_XD")){
                            ruleName = "小单";
                        }else if(name.equals("DXDS_XS")){
                            ruleName = "小双";
                        }else if(name.equals("SB_R")){
                            ruleName = "红波";
                        }else if(name.equals("SB_G")){
                            ruleName = "绿波";
                        }else if(name.equals("SB_B")){
                            ruleName = "蓝波";
                        }else if(name.equals("BZ_0")){
                            ruleName = "豹子";
                        }
                    }else if(lotteryId == 11){
                        //特码
                        if(name.indexOf("HZ_") != -1){
                            ruleName = name.substring("HZ_".length());
                        }else if(name.equals("DX_D")){
                            ruleName = "大";
                        }else if(name.equals("DS_D")){
                            ruleName = "单";
                        }else if(name.equals("JZDX_D")){
                            ruleName = "极大";
                        }else if(name.equals("DXDS_DD")){
                            ruleName = "大单";
                        }else if(name.equals("DXDS_DS")){
                            ruleName = "大双";
                        }else if(name.equals("DX_X")){
                            ruleName = "小";
                        }else if(name.equals("DS_S")){
                            ruleName = "双";
                        }else if(name.equals("JZDX_X")){
                            ruleName = "极小";
                        }else if(name.equals("DXDS_XD")){
                            ruleName = "小单";
                        }else if(name.equals("DXDS_XS")){
                            ruleName = "小双";
                        }else if(name.equals("SB_R")){
                            ruleName = "红波";
                        }else if(name.equals("SB_G")){
                            ruleName = "绿波";
                        }else if(name.equals("SB_B")){
                            ruleName = "蓝波";
                        }else if(name.equals("BZ_0")){
                            ruleName = "豹子";
                        }else if(name.equals("TMBS_0")){
                            ruleName = "包三";
                        }
                    }
                    mod.setRule(ruleName);
                    list.add(mod);
                }
            }
            String event="";
            if(!list.isEmpty()){
                switch (lotteryId){
                    case 10:
                        event="加拿大28-修改赔率";
                        break;
                    case 11:
                        event="北京28-修改赔率";
                        break;
                }
            }
            oddsService.updateLotteryOddsbyList(list,user.getUsername(),request,event,null);
            resultMap.put("status",200);
            resultMap.put("message","修改成功");
            logger.info("【##】 End Update Odds / 修改结束");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            resultMap.put("status",300);
            resultMap.put("message","服务异常");
        }
        return  resultMap;
    }
}
