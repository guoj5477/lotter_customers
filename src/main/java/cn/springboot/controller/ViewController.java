package cn.springboot.controller;


import cn.springboot.model.CustomerEntity;

import cn.springboot.model.OperationRecord;
import cn.springboot.model.OperatorEntity;
import cn.springboot.model.page.Pagination;
import cn.springboot.service.customer.CustomerServer;
import cn.springboot.service.operator.OperatorServer;
import cn.springboot.service.report.RecordServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class ViewController extends BaseController{

    private static final Logger log = LoggerFactory.getLogger(ViewController.class);

    @Autowired
    private CustomerServer customerServer;

    @Autowired
    private RecordServer recordServer;

    @Autowired
    private OperatorServer operatorServer;

    @RequestMapping(value = "view/project/add9", method = RequestMethod.GET)
    String project_add9() {
        log.info("# loding view/project/add9");
        return "view/project/add9";
    }

    @RequestMapping(value = "view/project/edit_list", method = RequestMethod.GET)
    String project_edit_list() {
        log.info("# loding view/project/edit_list");
        return "view/project/edit_list";
    }

    /**
     * 查询所有会员
     * @return
     */
    @RequestMapping(value = "view/customerManage/query", method = RequestMethod.GET)
    String queryCustomer(CustomerEntity customerEntity, String findContent, ModelMap modelMap, Integer pageNo, Integer pageSize, HttpServletRequest request) {

        log.info("## 查询所有会员");
        if(pageSize==null){
            pageSize=20;
        }
        modelMap.put("findContent", findContent);
        Pagination pagination= null;
        try {
            pagination = customerServer.findCustomer(customerEntity,pageNo,pageSize);
            modelMap.put("model",pagination);
            modelMap.put("page",pagination.getPageHtml());
            log.info("## Result :"+pagination.getList());
            log.info("## End Query Operator ");
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return "view/customer/query";
    }

    @RequestMapping(value = "view/record/operationRecord",method = RequestMethod.GET)
    public String operationRecord(OperationRecord operationRecord, String findContent, ModelMap modelMap, Integer pageNo, Integer pageSize, HttpServletRequest request){
        log.info("## 进入查询操作记录 ");
        if(pageSize==null){
            pageSize=25;
        }
        modelMap.put("findContent", findContent);

        try {
            Pagination pagination = recordServer.queryOperationRecord(operationRecord,pageNo,pageSize);
            modelMap.put("model",pagination);
            modelMap.put("page",pagination.getPageHtml());
            log.info("## Result :"+pagination.getList());
            log.info("## End Query Operator Record");
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return  "view/report/operationRecord";
    }

    /**
     * 查询管理员
     * @return
     */
    @RequestMapping(value = "view/operator/queryOperator", method = RequestMethod.GET)
    String queryOperator(CustomerEntity customerEntity, String findContent, ModelMap modelMap) {

        log.info("## 查询管理员");
        modelMap.put("findContent", findContent);
        Pagination pagination= null;
        try {
            pagination = operatorServer.queryOperator(pageNo,pageSize);
            modelMap.put("model",pagination);
            modelMap.put("page",pagination.getPageHtml());
            log.info("## Result :"+pagination.getList());
            log.info("## End Query Operator ");
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return "view/operator/query";
    }


}
