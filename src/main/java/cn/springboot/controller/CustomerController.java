package cn.springboot.controller;


import cn.springboot.common.util.StringUtils;
import cn.springboot.config.shiro.TokenManager;
import cn.springboot.model.CustomerEntity;
import cn.springboot.model.auth.User;
import cn.springboot.model.page.Pagination;
import cn.springboot.service.customer.CustomerServer;
import org.apache.shiro.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.util.Map;

/**
 * 会员管理控制层
 */
@RequestMapping("/customerManage")
@Controller
public class CustomerController extends BaseController {


    private Logger logger= LoggerFactory.getLogger(CustomerController.class);
    @Autowired
    private CustomerServer customerServer;
    /**
     * 会员注册
     */
    @RequestMapping(value = "/register",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> register(String username, String password, String phone, HttpServletRequest request){
        logger.info("【##】开始注册用户..."+username);
        if(StringUtils.isBlank(username)){
            resultMap.put("status",300);
            resultMap.put("message","用户名不能为空");
            return resultMap;
        }
        if(StringUtils.isBlank(password)){
            resultMap.put("status",300);
            resultMap.put("message","密码不能为空");
            return resultMap;
        }
       /*
       CustomerEntity customerEntity=new CustomerEntity.Builder(account, MD5Utils.encode(account,password)).createTime(new Date()).email("").lastLoginIp(IPUtils.getIpAddr(request)).build();
        //查询管理员是否存在
        CustomerEntity ce=customerServer.findCustomerByAccount(new CustomerEntity.Builder("eric","123456").build());
        if(ce!=null){
            resultMap.put("status",300);
            resultMap.put("message","用户名已经存在");
            return resultMap;
        }
        Boolean flag=customerServer.register(customerEntity);
        if(flag){
            resultMap.put("status",200);
            resultMap.put("message","注册成功");
        }else{
            resultMap.put("status",300);
            resultMap.put("message","注册失败");
        }*/
        //用户注册
        return resultMap;
    }

    /**
     *上分
     * @return
     */
    @RequestMapping(value = "/addScore",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> addScore(CustomerEntity customerEntity,HttpServletRequest request){

        Session session= TokenManager.getSession();
        User user=(User) session.getAttribute("user");
        if(user==null){
            resultMap.put("status",300);
            resultMap.put("message","请重新登录");
            return resultMap;
        }

       Boolean flag= customerServer.addScore(customerEntity,request,user.getUsername(),customerEntity.getUsername());
       if(flag){
          resultMap.put("status",200);
          resultMap.put("message","操作成功");
       }else {
           resultMap.put("status",300);
           resultMap.put("message","操作失败");
       }
        return resultMap;
    }

    /**
     * 下分
     * @return
     */
    @RequestMapping(value = "/deductScore",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> deductScore(CustomerEntity customerEntity,HttpServletRequest request){

        try {
            Session session= TokenManager.getSession();
            User user=(User) session.getAttribute("user");
            if(user==null){
                resultMap.put("status",300);
                resultMap.put("message","请重新登录");
                return resultMap;
            }
            BigDecimal score=customerEntity.getBalance().abs().negate();
            //说明是下分
            if(score.compareTo(new BigDecimal("0"))<0){
                CustomerEntity customer = customerServer.findCustomerByAccount(customerEntity);
                String rcode = customer.getReceiveQrcode();
                BigDecimal balance = customer.getBalance();
                if(balance.compareTo(customerEntity.getBalance().abs()) < 0 ){
                    resultMap.put("status",300);
                    resultMap.put("message","请输入【0 - "+balance.setScale(2,BigDecimal.ROUND_DOWN)+"】之间的数字");
                    return resultMap;
                }
                if(rcode== null || rcode.equals("") || rcode.equals("null")){
                    resultMap.put("status",300);
                    resultMap.put("message","用户没有收款二维码");
                    return resultMap;
                }
                logger.info("用户的二维码图片文件名：" + rcode.substring(rcode.lastIndexOf("/") + 1));
                resultMap.put("imgUrl",rcode.substring(rcode.lastIndexOf("/") + 1));
                resultMap.put("balance",customerEntity.getBalance().abs().setScale(2,BigDecimal.ROUND_DOWN));
            }
            customerEntity.setBalance(score);
            Boolean flag= customerServer.addScore(customerEntity,request,user.getUsername(),customerEntity.getUsername());
            if(flag){
                resultMap.put("status",200);
                resultMap.put("message","操作成功");
            }else {
                resultMap.put("status",300);
                resultMap.put("message","操作失败");
            }
        }catch (Exception e){
            e.printStackTrace();
            resultMap.put("status",300);
            resultMap.put("message","操作失败");
        }

        return resultMap;
    }

    /**
     * 禁用管理员
     */
    @RequestMapping(value = "/disableCustomer",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> disableCustomer(CustomerEntity customerEntity,HttpServletRequest request){
        Session session= TokenManager.getSession();
        User user=(User) session.getAttribute("user");
        if(user==null){
            resultMap.put("status",300);
            resultMap.put("message","请重新登录");
            return resultMap;
        }

        String type="";
        String event="";
        if("Y".equals(customerEntity.getStatus())){
            customerEntity.setStatus("N");
             type="禁用";
             event="禁用账户:"+customerEntity.getUsername();
        }else if("N".equals(customerEntity.getStatus())){
            customerEntity.setStatus("Y");
            type="启用";
            event="启用账户:"+customerEntity.getUsername();
        }
        Boolean flag= customerServer.disableCustomer(customerEntity,request,user.getUsername(),event,type,customerEntity.getUsername());
        if(flag){
            resultMap.put("status",200);
            resultMap.put("message","操作成功");
        }else {
            resultMap.put("status",300);
            resultMap.put("message","操作失败");
        }
        return resultMap;
    }

    /**
     * 查询所有会员
     * @return
     */
    @RequestMapping(value = "/queryCustomer")
    String queryCustomer(CustomerEntity customerEntity, String findContent, ModelMap modelMap, Integer pageNo, Integer pageSize, HttpServletRequest request) {

        logger.info("## 查询所有会员");
        if(pageSize==null){
            pageSize=20;
        }
        modelMap.put("findContent", findContent);
        Pagination pagination= null;
        try {
            pagination = customerServer.findCustomer(customerEntity,pageNo,pageSize);
            modelMap.put("model",pagination);
            modelMap.put("customerEntity",customerEntity);
            modelMap.put("page",pagination.getPageHtml());
            logger.info("## Result :"+pagination.getList());
            logger.info("## End Query Operator ");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return "view/customer/query";
    }

}
