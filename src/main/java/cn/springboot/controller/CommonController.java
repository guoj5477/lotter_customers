package cn.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value ="open")
public class CommonController {

    //没有权限
    @RequestMapping(value = "/unauthorized",method= RequestMethod.GET)
    public ModelAndView unauthorized(){
        return new ModelAndView("/unauthorized");

    }

    //踢出
    @RequestMapping(value = "/kickedOut",method= RequestMethod.GET)
    public ModelAndView kickedOut(){
        return new ModelAndView("/kickedOut");

    }




}
