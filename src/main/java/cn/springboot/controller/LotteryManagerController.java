package cn.springboot.controller;

import cn.springboot.common.constants.Constants;
import cn.springboot.common.util.DateUtil;
import cn.springboot.common.util.StringUtils;
import cn.springboot.model.AwardLogBean;
import cn.springboot.model.OrderModel;
import cn.springboot.model.auth.User;
import cn.springboot.service.lottery.BetOrderService;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import cn.springboot.model.page.Pagination;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import cn.springboot.model.LotteryInfo;
import cn.springboot.common.util.DateUtils;
import cn.springboot.common.exception.LottException;
import cn.springboot.service.lottery.LotteryService;
@Controller
@RequestMapping(value = "/lottery")
public class LotteryManagerController extends BaseController{

    @Autowired
    private LotteryService lotteryService;
    @Autowired
    private BetOrderService betOrderService;
    /*
      * 跳转到開獎页面
      */
    @RequestMapping(value = "/queryLotteryCanada28",method = RequestMethod.GET)
    public String queryLotteryCanada28(HttpServletRequest request, String lotteryOrder,String lotteryDate,String findContent, ModelMap modelMap, Integer pageNo) {

        logger.info("【##】 start query Lottery / 进入开奖查询");
        modelMap.put("findContent", findContent);
        Pagination<LotteryInfo> pagination= null;
        //加拿大28
        String  lid = "10";
        LotteryInfo lotteryInfo=new LotteryInfo();
        try {
            if(lid!=null&&lid!=""){
                lotteryInfo.setLotteryId(Integer.parseInt(lid));
            }
            if(lotteryOrder!=null&&lotteryOrder!=""){
                lotteryInfo.setLotteryOrder(new Integer(lotteryOrder));
            }
            if(lotteryDate!=null&&lotteryDate!=""){
                lotteryInfo.setLotteryDate(lotteryDate);
            }else{
                lotteryInfo.setLotteryDate(DateUtils.getNow("yyyy-MM-dd"));
            }
            logger.info("【##】 parameter / 参数：lotteryId:"+lotteryInfo.getLotteryId());
            pagination = lotteryService.queryLotteryByCanadaPCDD(lotteryInfo,pageNo,pageSize);
            String pagehtml = pagination.getPageHtml();
            modelMap.addAttribute("pageHtml",pagehtml);
            logger.info("## Result:"+pagination.getList());
            logger.info("【##】 end query Lottery / 结束开奖查询 ");
        } catch (LottException l){
            logger.error(l.getMessage());
            l.printStackTrace();
        }
        HttpSession session=request.getSession();
        session.setAttribute("lottDate",DateUtils.getNow("yyyy-MM-dd"));
        modelMap.addAttribute("model",pagination);
        modelMap.addAttribute("lotteryId",lid);
        modelMap.addAttribute("lottDate",lotteryInfo.getLotteryDate());
        modelMap.addAttribute("lotteryOrder",lotteryOrder);

        modelMap.addAttribute("queryUrl","/lottery/queryLotteryCanada28");
        return "view/lotteryManage/list";
    }
    /**取消结算*/
    @RequestMapping(value = "/cancelSettlement",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> cancelSettlement(LotteryInfo lotteryInfo,HttpServletRequest request){
        logger.info("【##】 Start Cancel Settlement / 进入取消结算 ");
        HttpSession session=request.getSession();
        User user=(User) session.getAttribute("user");
        if(user==null){
            logger.info("## operator is null !");
            resultMap.put("success",false);
            resultMap.put("message","你已退出,请重新登录!");
            return resultMap;
        }
        logger.info("## parameter：{lotteryId:"+lotteryInfo.getLotteryId()+";pid:"+lotteryInfo.getLotteryOrder()+"}");
        //判断彩票是否取消结算
        List list= betOrderService.queryBetStatus(new OrderModel(lotteryInfo.getLotteryId(),lotteryInfo.getLotteryOrder().toString()));
        if(list.size()>0){
            logger.info("【##】 Lottery is Already Settlement / 本期彩票已经取消结算");
            resultMap.put("success",false);
            resultMap.put("message","本期彩票已经取消结算!");
            return resultMap;
        }

        try {
            lotteryInfo.setCancelType(Constants.CANCEL_TYPE);
            lotteryService.cancelSettlement(lotteryInfo,user.getUsername(),request,null);
            resultMap.put("status",200);
            resultMap.put("message","取消结算成功!");
            logger.info("【##】 Start Cancel Settlement / 结束取消结算");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            resultMap.put("status",300);
            resultMap.put("message","取消结算失败");
        }
        return resultMap;
    }
    /**修改彩票开奖结果，流程:修改lottData数据，将本期已经派奖的订单回滚 ，重新结算本期 ，账变记录，操作记录 */
    @RequestMapping(value = "/updateLottCode",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> updateLottCode(LotteryInfo lotteryInfo,HttpServletRequest request){

        HttpSession session=request.getSession();
        User user=(User) session.getAttribute("user");
        if(user==null){
            logger.info("## operator is null !");
            resultMap.put("success",false);
            resultMap.put("message","你已退出,请重新登录!");
            return resultMap;
        }
        logger.info("【##】 /"+user.getUsername()+"/Start Update Lottery Code / 进入修改彩票开奖结果");
        if(StringUtils.isBlank(lotteryInfo.getLotteryCode())){
            logger.info("【##】 Lottery Code is Null / 开奖号码为空");
            resultMap.put("success",false);
            resultMap.put("message","开奖号码不能为空!");
            return resultMap;

        }
        logger.info("【##】 parameter / 参数 ：{lotteryId:"+lotteryInfo.getLotteryId()+";lotteryCode:"+lotteryInfo.getLotteryCode()+"}");

        //判断彩票是否取消结算
        List list= betOrderService.queryBetStatus(new OrderModel(lotteryInfo.getLotteryId(),lotteryInfo.getLotteryOrder().toString()));
        if(list.size()>0){
            logger.info("【##】 Lottery is Already Settlement / 彩票已经取消结算 ");
            resultMap.put("success",false);
            resultMap.put("message","本期彩票已经取消结算，不能再结算!");
            return resultMap;
        }
        try {
            lotteryInfo.setCancelType(Constants.RESET_TYPE);
            lotteryService.updateLottCode(lotteryInfo,user.getUsername(),request,null);
            resultMap.put("success",true);
            resultMap.put("message","修改开奖成功!");
            logger.info("【##】 End Update Lottery Code / 结束修改彩票开奖 结果");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            resultMap.put("success",false);
            resultMap.put("message","修改开奖错误");
        }
        return resultMap;
    }

    //手动开奖
    @RequestMapping(value = "/updateLotteryInfo", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object>  updateLotteryInfo(LotteryInfo lotteryInfo,HttpServletRequest request){

        HttpSession session=request.getSession();
        User user=(User) session.getAttribute("user");
        if(user==null){
            logger.info("## operator is null !");
            resultMap.put("success",false);
            resultMap.put("message","你已退出,请重新登录!");
            return resultMap;
        }
        logger.info("【##】 /"+user.getUsername()+"/start add Lottery OpenCode / 进入手动开奖 ");
        logger.info("【##】 parameter / 参数 :lotteryId:"+lotteryInfo.getLotteryId()+";Date:"+lotteryInfo.getLotteryDate()+";lotteryCode:"+lotteryInfo.getLotteryCode());
        String date= lotteryInfo.getLotteryDate().replaceAll("-","");
        String pid="";
        try {
            String event="";
            int lotteryId=lotteryInfo.getLotteryId();

            switch (lotteryId){
                case 10:
                    lotteryInfo.setLotteryName("加拿大28");
                    event="加拿大28期号: "+lotteryInfo.getLotteryOrder()+" 手动开奖,开奖号码:"+lotteryInfo.getLotteryCode();
                    break;
                case 11:
                    lotteryInfo.setLotteryName("北京28");
                    event="北京28期号: "+lotteryInfo.getLotteryOrder()+" 手动开奖,开奖号码:"+lotteryInfo.getLotteryCode();
                    break;
            }
            String lottTime= lotteryInfo.getLotteryDate()+" "+ lotteryInfo.getLotteryTime();
            lotteryInfo.setLotteryTime(lottTime);
            //判断彩票是否取消结算
            List list= betOrderService.queryBetStatus(new OrderModel(lotteryInfo.getLotteryId(),lotteryInfo.getLotteryOrder().toString()));
            if(list.size()>0){
                logger.info("【##】 Lottery is Already Settlement / 彩票已经取消结算 ");
                resultMap.put("success",false);
                resultMap.put("message","本期彩票已经取消结算，不能再结算!");
                return resultMap;
            }
            //查询是否已经开奖
            int flag = lotteryService.queryLotteryData(lotteryInfo);
            //查询注单表中是否存在本期开奖
            if(flag>0){
                logger.info("【##】 lottery already open  / 本期彩票已经开奖");
                resultMap.put("success",false);
                resultMap.put("message","本期彩票已经开奖");
                return resultMap;
            }
            if(StringUtils.isBlank(lotteryInfo.getLotteryCode())){
                logger.info("【##】 Lottery Code is Null / 开奖号码为空");
                resultMap.put("success",false);
                resultMap.put("message","开奖号码不能为空");
                return resultMap;
            }


            lotteryService.insertLotteryData(lotteryInfo,user.getUsername(),request,null);
            resultMap.put("success",true);
            resultMap.put("message","手动开奖成功");
            logger.info("【##】 end add Lottery OpenCode / 结束手动开奖 ");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            resultMap.put("success",false);
            resultMap.put("message","手动开奖失败");
        }

        return resultMap;
    }
    /*
      * 跳转到開獎页面
      */
    @RequestMapping(value = "/queryLotteryBj28",method = RequestMethod.GET)
    public String queryLotteryBj28(HttpServletRequest request, String lotteryOrder,String lotteryDate,String findContent, ModelMap modelMap, Integer pageNo) {

        logger.info("【##】 start query Lottery / 进入开奖查询");
        modelMap.put("findContent", findContent);
        Pagination<LotteryInfo> pagination= null;
        //加拿大28
        String  lid = "11";
        LotteryInfo lotteryInfo=new LotteryInfo();
        try {
            if(lid!=null&&lid!=""){
                lotteryInfo.setLotteryId(Integer.parseInt(lid));
            }
            if(lotteryOrder!=null&&lotteryOrder!=""){
                lotteryInfo.setLotteryOrder(new Integer(lotteryOrder));
            }
            if(lotteryDate!=null&&lotteryDate!=""){
                lotteryInfo.setLotteryDate(lotteryDate);
            }else{
                lotteryInfo.setLotteryDate(DateUtils.getNow("yyyy-MM-dd"));
            }
            logger.info("【##】 parameter / 参数：lotteryId:"+lotteryInfo.getLotteryId());
            pagination = lotteryService.queryLotteryByCanadaPCDD(lotteryInfo,pageNo,pageSize);
            String pagehtml = pagination.getPageHtml();
            modelMap.addAttribute("pageHtml",pagehtml);
            logger.info("## Result:"+pagination.getList());
            logger.info("【##】 end query Lottery / 结束开奖查询 ");
        } catch (LottException l){
            logger.error(l.getMessage());
            l.printStackTrace();
        }
        HttpSession session=request.getSession();
        session.setAttribute("lottDate",DateUtils.getNow("yyyy-MM-dd"));
        modelMap.addAttribute("model",pagination);
        modelMap.addAttribute("lotteryId",lid);
        modelMap.addAttribute("lottDate",lotteryInfo.getLotteryDate());
        modelMap.addAttribute("lotteryOrder",lotteryOrder);

        modelMap.addAttribute("queryUrl","/lottery/queryLotteryBj28");
        return "view/lotteryManage/list";
    }

    /*
      * 跳转到開獎页面
      */
    @RequestMapping(value = "/queryLotteryRec",method = RequestMethod.GET)
    public String queryLotteryRec(HttpServletRequest request, AwardLogBean lotteryInfo, String findContent, ModelMap modelMap, Integer pageNo) {

        logger.info("【##】 start query Lottery / 进入开奖记录查询");
        modelMap.put("findContent", findContent);
        Pagination<AwardLogBean> pagination= null;
        try {
            if(lotteryInfo.getLotteryId()==null){
                lotteryInfo.setLotteryId("10");
            }
            logger.info("【##】 parameter / 参数：lotteryId:"+lotteryInfo.getLotteryId());
            pagination = lotteryService.queryLotteryRec(lotteryInfo,pageNo,pageSize);
            String pagehtml = pagination.getPageHtml();
            modelMap.addAttribute("pageHtml",pagehtml);
            logger.info("## Result:"+pagination.getList());
            logger.info("【##】 end query Lottery / 结束开奖记录查询 ");
        } catch (LottException l){
            logger.error(l.getMessage());
            l.printStackTrace();
        }
        HttpSession session=request.getSession();
        session.setAttribute("lottDate",DateUtils.getNow("yyyy-MM-dd"));
        modelMap.addAttribute("model",pagination);
        modelMap.addAttribute("orderModel",lotteryInfo);
        modelMap.addAttribute("queryUrl","/lottery/queryLotteryRec");
        return "view/lotteryManage/award_loglist";
    }
}
