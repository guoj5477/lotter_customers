package cn.springboot.controller;


import cn.springboot.common.util.*;
import cn.springboot.config.shiro.TokenManager;
import com.sun.xml.internal.rngom.parse.host.Base;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cn.springboot.model.auth.User;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
public class LoginController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    @RequestMapping(value = "login", method = RequestMethod.GET)
    String login(Model model) {

        model.addAttribute("user", new User());
        log.info("#去登录");
        return "view/login/login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> login(String username, String password,String vcode,HttpServletRequest request){
        log.info("【##】 User Start Login.. / 用户开始登陆 ..");
        String ip= IPUtils.getIpAddr(request);
       /* if(!isInIpWhiteList(ip)){
            logger.info("【##】 IP["+ip+"] not in  white list, can not login in");
            resultMap.put("message","IP不能访问,请联系管理员！");
            return resultMap;
        }*/
        try {
            //判断验证码是否正确
            if(!VerifyCodeUtils.verifyCode(vcode)){
                log.info("【##】 VCode is Incorrect");
                resultMap.put("message", "验证码不正确！");
                return resultMap;
            }
            logger.info("【##】 Login parameter{userName:"+username+";IP："+ip+";VCode:"+vcode);

            UsernamePasswordToken token = new UsernamePasswordToken(username,password);
            // 获取当前的Subject
            Subject currentUser = SecurityUtils.getSubject();
            currentUser.login(token);

            logger.info("【##】 Login Success !");
        } catch (IncorrectCredentialsException ice) {
            logger.info("【##】 PassWord is Incorrect");
            resultMap.put("message","密码错误");
            return resultMap;
        }catch (UnknownAccountException uae) {
            logger.info("【##】 UserName OR PassWord is Incorrect");
            resultMap.put("message","用戶名或者密碼錯誤");
            return  resultMap;

        } catch (ExcessiveAttemptsException eae) {
            logger.info("【##】 Five consecutive errors are locked");
            resultMap.put("message","连续错误五次，账户被锁定");
            return resultMap;

        }catch (DisabledAccountException e){
            logger.info("## Account has been locked");
            resultMap.put("message","賬戶已經被鎖定");
            return  resultMap;
        }
        resultMap.put("message","success");
        return resultMap;
    }

  /*  @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute("userForm")User user, RedirectAttributes redirectAttributes) {
        log.info("# 登录中 ");
        if (null == user || StringUtils.isBlank(user.getUsername()) || StringUtils.isBlank(user.getPassword())) {
            log.error("# 账号或密码错误");
            return "login";
        }

        //判断验证码是否正确
        if(!VerifyCodeUtils.verifyCode(user.getVcode())){
            log.error("【##】 VCode is Incorrect");
            return "redirect:/login";
        }

        String username=user.getUsername();
        UsernamePasswordToken token = new UsernamePasswordToken(user.getUsername(), user.getPassword());
        // 获取当前的Subject
        Subject currentUser = SecurityUtils.getSubject();
        try {
            // 在调用了login方法后,SecurityManager会收到AuthenticationToken,并将其发送给已配置的Realm执行必须的认证检查
            // 每个Realm都能在必要时对提交的AuthenticationTokens作出反应
            // 所以这一步在调用login(token)方法时,它会走到MyRealm.doGetAuthenticationInfo()方法中,具体验证方式详见此方法
            log.info("对用户[" + username + "]进行登录验证..验证开始");
            currentUser.login(token);
            log.info("对用户[" + username + "]进行登录验证..验证通过");
            //保存用户到session中
            TokenManager.setVal2Session("OperatorModel",user);

        } catch (UnknownAccountException uae) {
            log.error("对用户[" + username + "]进行登录验证..验证未通过,未知账户");
            redirectAttributes.addFlashAttribute("message", "未知账户");
        } catch (IncorrectCredentialsException ice) {
            log.error("对用户[" + username + "]进行登录验证..验证未通过,错误的凭证");
            redirectAttributes.addFlashAttribute("message", "密码不正确");
        } catch (LockedAccountException lae) {
            log.error("对用户[" + username + "]进行登录验证..验证未通过,账户已锁定");
            redirectAttributes.addFlashAttribute("message", "账户已锁定");
        } catch (ExcessiveAttemptsException eae) {
            log.error("对用户[" + username + "]进行登录验证..验证未通过,错误次数过多");
            redirectAttributes.addFlashAttribute("message", "用户名或密码错误次数过多");
        } catch (AuthenticationException ae) {
            // 通过处理Shiro的运行时AuthenticationException就可以控制用户登录失败或密码错误时的情景
            log.error("对用户[" + username + "]进行登录验证..验证未通过,堆栈轨迹如下");
            ae.printStackTrace();
            redirectAttributes.addFlashAttribute("message", "用户名或密码不正确");
        }
        // 验证是否登录成功
        if (currentUser.isAuthenticated()) {
            log.info("用户[" + username + "]登录认证通过(这里可以进行一些认证通过后的一些系统参数初始化操作)");
            return "redirect:/index";
        } else {
            token.clear();
            return "redirect:/login";
        }
    }*/

    /**
     * 获取验证码（Gif版本）
     * @param response
     */
    @RequestMapping(value="/getGifCode",method= RequestMethod.GET)
    public void getGifCode(HttpServletResponse response, HttpServletRequest request){
        try {
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setContentType("image/gif");
            /**
             * gif格式动画验证码
             * 宽，高，位数。
             */
            Captcha captcha = new GifCaptcha(146,42,4);
            //输出
            ServletOutputStream out = response.getOutputStream();
            captcha.out(out);
            out.flush();
            //存入Shiro会话session
            System.out.println( captcha.text().toLowerCase());
            TokenManager.setVal2Session(VerifyCodeUtils.V_CODE, captcha.text().toLowerCase());
        } catch (Exception e) {
            LoggerUtils.fmtError(getClass(),e, "获取验证码异常：%s",e.getMessage());
        }
    }

    @RequestMapping("/logout")
    public String logout() {
        SecurityUtils.getSubject().logout();
        return "view/login/login";
    }

    @RequestMapping("/403")
    public String unauthorizedRole() {
        log.info("------没有权限-------");
        return "403";
    }

}
