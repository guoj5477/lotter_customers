package cn.springboot.controller;

import cn.springboot.common.util.DateUtils;
import cn.springboot.config.shiro.vo.RoleEnumUtil;
import cn.springboot.model.OperatorEntity;
import cn.springboot.model.auth.Role;
import cn.springboot.model.auth.User;
import cn.springboot.service.auth.RoleService;
import cn.springboot.service.auth.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @user eric
 * @date 2018/2/6 15:04
 * @todo
 */
@Controller
@RequestMapping(value = "/operator")
public class OperatorController extends BaseController{

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    /**
     *@user eric
     *@date 2018/2/6 16:40
     *@todo  新增管理员
     */
    @RequestMapping(value = "/addOperator",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> addOperator(String password,String account,int role,HttpServletRequest request){
        logger.info("## Start Add Operator ");
        HttpSession session=request.getSession();
        User om=(User)session.getAttribute("user");
        if(om==null){
            logger.info("## Operator is Null");
            resultMap.put("success",false);
            resultMap.put("message","你已退出,请重新登录!");
            return resultMap;
        }
        logger.info("##管理员【"+om.getUsername()+"】新增管理员:"+account+"当前时间:"+ DateUtils.getNow("yyyy-MM-dd HH:mm:ss"));
        String event="新增管理员:"+account;
        try {
            //查询新增的管理员是否存在
            User user=userService.findUserByName(account);
            if(user!=null){
                logger.info("## Account already exists ");
                resultMap.put("status",300);
                resultMap.put("message","管理员已经存在！");
                return resultMap;
            }
            Role rl;
            if(role==1){
                rl= roleService.findRoleByCode(RoleEnumUtil.超级管理员.getRoleCode());
            }else {
                rl=roleService.findRoleByCode(RoleEnumUtil.普通用户.getRoleCode());
            }

            User admin = new User();
            admin.setUsername(account);
            admin.setEmail("");
            admin.setTrueName("管理员");
            admin.setPassword(password);
            admin.setOrganizeId(rl.getId());
            userService.addUser(admin, rl);

            logger.info("## Add Operator Success");
            resultMap.put("status",200);
            resultMap.put("message","新增管理员成功");
            logger.info("## End Add Operator ");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            resultMap.put("status",300);
            resultMap.put("message","新增管理员失败");
        }
        return resultMap;
    }

    /**
     *@user eric
     *@date 2018/2/6 16:40
     *@todo  解锁管理员
     */
    @RequestMapping(value = "/unlockAdmin",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> unlockAdmin(OperatorEntity operatorEntity, HttpServletRequest request){
        logger.info("## Start Add Operator ");
        HttpSession session=request.getSession();
        User om=(User)session.getAttribute("user");
        if(om==null){
            logger.info("## Operator is Null");
            resultMap.put("success",false);
            resultMap.put("message","你已退出,请重新登录!");
            return resultMap;
        }
        logger.info("##管理员【"+om.getUsername()+"】解锁管理员:"+operatorEntity.getUserName()+"当前时间:"+ DateUtils.getNow("yyyy-MM-dd HH:mm:ss"));
        String event="新增管理员:"+operatorEntity.getUserName();
        try {

            User admin = new User();
            admin.setStatus(1);
            admin.setId(operatorEntity.getUserId());
            userService.updateUserForUnLock(admin);
            logger.info("## Add Operator Success");
            resultMap.put("status",200);
            resultMap.put("message","操作成功");
            logger.info("## End Add Operator ");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            resultMap.put("status",300);
            resultMap.put("message","操作失败");
        }
        return resultMap;
    }

}
