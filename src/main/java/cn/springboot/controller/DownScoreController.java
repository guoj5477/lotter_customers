package cn.springboot.controller;


import cn.springboot.common.util.StringUtils;
import cn.springboot.config.shiro.TokenManager;
import cn.springboot.model.CustomerEntity;
import cn.springboot.model.DownLogBean;
import cn.springboot.model.auth.User;
import cn.springboot.model.page.Pagination;
import cn.springboot.service.customer.CustomerServer;
import cn.springboot.service.customer.DownScoreService;
import org.apache.shiro.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 会员管理控制层
 */
@RequestMapping("/downScore")
@Controller
public class DownScoreController extends BaseController {
    private Logger logger= LoggerFactory.getLogger(DownScoreController.class);

    @Autowired
    private DownScoreService downScore;
    /**
     * 查询下分请求信息
     * @return
     */
    @RequestMapping(value = "/queryDownlist")
    String queryDownlist(DownLogBean downLogBean, String findContent, ModelMap modelMap, Integer pageNo, Integer pageSize, HttpServletRequest request) {

        logger.info("## 查询下分请求");
        if(pageSize==null){
            pageSize=20;
        }
        modelMap.put("findContent", findContent);
        Pagination pagination= null;
        try {
            pagination = downScore.findDownlist(downLogBean,pageNo,pageSize);
            modelMap.put("model",pagination);
            modelMap.put("downLogBean",downLogBean);
            modelMap.put("page",pagination.getPageHtml());
            logger.info("## Result :"+pagination.getList());
            logger.info("## End Query Operator ");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return "view/downScore/query";
    }

    /**
     * 查询下分请求信息
     * @return
     */
    @RequestMapping(value = "/getDownlogList")
    @ResponseBody
    public Map<String,Object> getDownlogList(DownLogBean downLogBean) {
        List<DownLogBean> list = downScore.findDownlist(downLogBean);
        logger.info("## 查询下分请求");
        resultMap.put("status",200);
        resultMap.put("list",list);
        return resultMap;
    }
    /**
     * 更换请求状态
     * @return
     */
    @RequestMapping(value = "/convertStatus")
    @ResponseBody
    public Map<String,Object> convertStatus(DownLogBean downLogBean, String findContent, ModelMap modelMap, Integer pageNo, Integer pageSize, HttpServletRequest request) {
        logger.info("## Start Add Operator ");
        HttpSession session=request.getSession();
        User om=(User)session.getAttribute("user");
        if(om==null){
            logger.info("## Operator is Null");
            resultMap.put("success",false);
            resultMap.put("message","你已退出,请重新登录!");
            return resultMap;
        }
        logger.info("##会员:用户名:【"+downLogBean.getUsername()+"】，申请提现时间为:【"+downLogBean.getCreateTimeStr()+"】,正在更改处理状态:原状态为:【新请求】#######新状态为【处理中】");
        try {
            downScore.updateDownLogStatus(downLogBean);
            resultMap.put("status",200);
            resultMap.put("message","操作成功,核对无误后请去会员管理功能下完成下分的操作");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            resultMap.put("status",300);
            resultMap.put("message",e.getMessage());
        }
        return resultMap;
    }

}
