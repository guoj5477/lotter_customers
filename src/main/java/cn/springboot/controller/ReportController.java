package cn.springboot.controller;

import cn.springboot.model.OperationRecord;
import cn.springboot.model.OrderModel;
import cn.springboot.model.page.Pagination;
import cn.springboot.service.lottery.BetOrderService;
import cn.springboot.service.report.RecordServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/report")
public class ReportController extends  BaseController{

    private Logger log=LoggerFactory.getLogger(ReportController.class);
    @Autowired
    private BetOrderService betOrderService;
    @Autowired
    private RecordServer recordServer;

    @RequestMapping(value = "/operationRecord",method = RequestMethod.GET)
    public String operationRecord(OperationRecord operationRecord, String findContent, ModelMap modelMap, Integer pageNo, Integer pageSize, HttpServletRequest request){
        logger.info("## Start Query OperationRecord");
        if(pageSize==null){
            pageSize=25;
        }
        modelMap.put("findContent", findContent);
        Pagination pagination= null;
        try {
            pagination = recordServer.queryOperationRecord(operationRecord,pageNo,pageSize);
            //过去表单中输入的参数
            if(operationRecord!=null){
                HttpSession session=request.getSession();
                session.setAttribute("operationRecord",operationRecord);
            }
            modelMap.put("model",pagination);
            modelMap.put("page",pagination.getPageHtml());

            logger.info("## Result :"+pagination.getList());
            logger.info("## End Query Operator ");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return "view/report/operationRecord";
    }

    /**
     * 注单查询
     */
    @RequestMapping(value = "/getBetsOrder")
    public String getBetsOrder(OrderModel orderModel, HttpServletRequest request, String findContent, ModelMap modelMap, Integer pageNo, Integer pageSize){
        logger.info("【##】 Start Query BetsOrder / 注单查询 ");
        modelMap.put("findContent", findContent);
        if(pageSize==null){
            pageSize=25;
        }
        //过去表单中输入的参数
        Pagination pagination= null;
        try {
            if(orderModel!=null){
                HttpSession session=request.getSession();
                session.setAttribute("orderModel",orderModel);
            }
            pagination = betOrderService.getBetsOrder(orderModel,pageNo,pageSize);
            logger.info("【##】 Result / 返回结果 :"+pagination.getList());
            logger.info("【##】 End Query BetsOrder");
            String pagehtml = pagination.getPageHtml();
            modelMap.addAttribute("pageHtml",pagehtml);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        modelMap.put("model",pagination);
        return "view/report/betsReport";

    }


}
