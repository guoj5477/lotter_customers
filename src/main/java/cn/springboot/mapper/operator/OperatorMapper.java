package cn.springboot.mapper.operator;

import cn.springboot.model.OperatorEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @user eric
 * @date 2018/2/6 15:22
 * @todo
 */
@Mapper
public interface OperatorMapper {

    List<OperatorEntity> queryOperator();
    List<OperatorEntity> queryOperators(OperatorEntity entity);
    Long queryOperatorsForTotal(OperatorEntity entity);
}
