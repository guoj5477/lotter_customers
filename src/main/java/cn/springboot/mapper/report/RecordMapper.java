package cn.springboot.mapper.report;

import cn.springboot.model.OperationRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RecordMapper {

    void insertOperationRecord(OperationRecord operationRecord);

    //查询后台操作记录总数
    Integer queryOperationRecordTotal(OperationRecord operationRecord);

    List<OperationRecord> queryOperationRecord(OperationRecord operationRecord);

    void insertOperationRecordForCustomer(OperationRecord op);
}
