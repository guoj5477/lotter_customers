package cn.springboot.mapper.odds;

import cn.springboot.model.OddsModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author eric
 * @date 2017/9/12/012 14:12
 * @todo
 */
@Mapper
public interface OddsDao {

    //查询彩票赔率
    List<OddsModel> queryOdds(OddsModel oddsModel);

    //修改彩票赔率
    void updateLotteryOdds(OddsModel oddsModel);

    //批量修改彩票赔率

    void updateLotteryOddsbyList(List<OddsModel> list);
}
