package cn.springboot.mapper.customer;

import cn.springboot.model.CustomerEntity;
import cn.springboot.model.DownLogBean;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface DownScoreMapper {
    List<DownLogBean> findCustomer(DownLogBean customerEntity);

    Integer findCustomerCount(DownLogBean customerEntity);
    void updateDownLogStatus_Process(DownLogBean customerEntity);
    void updateDownLogStatus_Finish(DownLogBean customerEntity);
}
