package cn.springboot.mapper.customer;

import cn.springboot.model.CustomerEntity;
import cn.springboot.model.auth.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CustomerMapper {

   CustomerEntity findCustomerByAccount(CustomerEntity customerEntity);

   void register(CustomerEntity customerEntity);

   List<CustomerEntity> findCustomer(CustomerEntity customerEntity);

  Integer findCustomerCount(CustomerEntity customerEntity);

   void addScore(CustomerEntity customerEntity);

    void disableCustomer(CustomerEntity customerEntity);

    User login(String userName);

    void updateOperatorByUsername(User user);
}
