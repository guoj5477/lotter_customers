package cn.springboot.mapper.lottery;

import java.util.List;
import cn.springboot.model.OrderModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author eric
 * @date 2017/10/17/017 9:22
 * @todo
 */
@Mapper
public interface BetOrderDao {

    List<OrderModel> getBetsOrder(OrderModel orderModel);

    Integer getBetsOrderCount(OrderModel orderModel);

    List<OrderModel> queryBetStatus(OrderModel orderModel);

}
