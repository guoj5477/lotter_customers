package cn.springboot.mapper.lottery;


import cn.springboot.model.AwardLogBean;
import cn.springboot.model.LotteryInfo;
import org.apache.ibatis.annotations.Mapper;
import java.util.Map;
import java.util.List;

@Mapper
public interface LotteryDao {

    List<AwardLogBean> queryLotteryRec(AwardLogBean lotteryInfo);
    Long queryLotteryRecForTotal(AwardLogBean lotteryInfo);
    List<LotteryInfo> queryLotteryByCanadaPCDD(LotteryInfo lotteryInfo);

    Long queryLotteryTotal(LotteryInfo lotteryInfo);
    /**加拿大28取消结算*/
    void cancelByCan28(Map<String,Object> map);
    /**  加拿大28手动派奖 **/
    void settleOrderByCan28(LotteryInfo lotteryInfo);
    //通过彩票id和期号查询开奖号码
    String queryLotteryCodeByPid(LotteryInfo lotteryInfo);
    /**
     * 查询是否开奖
     * @param lotteryInfo
     * @return
     */
    Integer queryLotteryData(LotteryInfo lotteryInfo);
    //手动开奖
    void insertLotteryData(LotteryInfo lotteryInfo);
    /**修改彩票开奖结果*/
    void updateLottCode(LotteryInfo lotteryInfo);

    /**取消开奖结果*/
    void updateLottCodeForCancel(LotteryInfo lotteryInfo);
}
