package cn.springboot.mapper.lottery;


import cn.springboot.model.LotteryInfo;
import org.apache.ibatis.annotations.Mapper;

import java.math.BigDecimal;

/**
 * @author eric
 * @date 2017/10/12/012 10:11
 * @todo
 */
@Mapper
public interface OrderCountDao {
    /**根据期号查询某期彩票的投注金额*/
    BigDecimal getOrderMoney(LotteryInfo lotteryInfo);

    /**根据期号查询某期彩票的中奖金额*/
    BigDecimal getOrderWinMoney(LotteryInfo lotteryInfo);


}
