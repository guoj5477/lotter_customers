package cn.springboot.common.split;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author eric
 * @date 2017/9/28/028 13:58
 * @todo
 */
public class DDStrategy implements Strategy{
    @Override
    public String convert(String tableName) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        StringBuilder sb=new StringBuilder(tableName);
        sb.append("_");
        sb.append(sdf.format(new Date()));
        return sb.toString();
    }
}
