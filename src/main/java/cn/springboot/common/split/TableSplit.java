package cn.springboot.common.split;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author eric
 * @date 2017/9/25/025 17:49
 * @todo
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface TableSplit {
    //是否分表
     boolean split() default true;
    //标识
     String value();

    //获取分表策略,如YYYY or yyyyMM
   /*  String strategy();*/

}