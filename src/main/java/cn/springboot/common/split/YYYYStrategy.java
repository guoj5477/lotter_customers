package cn.springboot.common.split;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 按年分表
 */
public class YYYYStrategy implements Strategy {

    @Override
    public String convert(String tableName) {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY");
        StringBuilder sb=new StringBuilder(tableName);
        sb.append("_");
        sb.append(sdf.format(new Date()));
        return sb.toString();
    }

}