package cn.springboot.common.split;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author eric
 * @date 2017/9/26/026 10:40
 * @todo  按月进行分表
 */
public class MMStrategy implements Strategy{

    @Override
    public String convert(String tableName) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        StringBuilder sb=new StringBuilder(tableName);
        sb.append("_");
        sb.append(sdf.format(new Date()));
        return sb.toString();
    }
}
