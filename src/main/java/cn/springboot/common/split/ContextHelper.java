package cn.springboot.common.split;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author eric
 * @date 2017/9/25/025 17:53
 * @todo
 */
@Component
public class ContextHelper implements ApplicationContextAware {
    private static ApplicationContext context;


    @Override
    public void setApplicationContext(ApplicationContext context)
            throws BeansException {
        ContextHelper.context=context;
    }

    public static StrategyManager getStrategyManager(){
        return context.getBean(StrategyManager.class);
    }
}