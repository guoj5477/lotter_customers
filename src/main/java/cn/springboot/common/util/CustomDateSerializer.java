package cn.springboot.common.util;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 日期的工具类
 */
@Service
public class CustomDateSerializer extends JsonSerializer<Date> {

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	public void serialize(Date date, JsonGenerator generator,
			SerializerProvider provider) throws IOException,
            JsonProcessingException {
		String dateTime = dateFormat.format(date);
		generator.writeString(dateTime);
	}

}
