package cn.springboot.common.util;

/**
 * @author eric
 * @date 2017/10/12/012 10:16
 * @todo
 */
public class SplitUtils {

    public static String getTableName(int index){

        String tableName="";

        switch (index){
            case 1:
                tableName="lott_bets_cqssc";
                break;
            case 2:
                tableName="lott_bets_gx10";
                break;
            case 3:
                tableName="lott_bets_xync";
                break;
            case 4:
                tableName="lott_bets_jsk3";
                break;
            default:
                tableName="lott_bets_cqssc";
                break;

        }
        return tableName;

    }
}
