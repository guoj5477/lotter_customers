package cn.springboot.common.exception;

public class LottException extends  Exception{
	public LottException() {
		super();
	}

	public LottException(String message) {
		super(message);
	}

	public LottException(String message, Throwable cause) {
		super(message, cause);
	}

}
