<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/gritter/jquery.gritter.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/multiselect/tree-multiselect.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">
    <link href="${ctx}/static/css/Semantic/semantic.css" rel="stylesheet" type="text/css">
</head>

<body class="fixed-sidebar">
    <div id="wrapper">
        <!----左侧导航开始----->
        <nav class="navbar-default navbar-static-side animated fadeInLeft" role="navigation" id="leftnav"></nav>
        <!----左侧导航结束----->

        <!---右侧内容区开始---->
        <div id="page-wrapper" class="gray-bg">
            <!---顶部状态栏 star-->
        <#--    <div class="row ">
              <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>-->
            <!---顶部状态栏 end-->
            <!-----内容区域---->

            <h4 class="ui horizontal divider header" style="width: 60%;"><i class="tag icon"></i> 登录信息 </h4>
            <table class="ui definition table" style="width:60%;font-size: 15px">
                <tbody>
                <tr>
                    <td class="two wide column">账户</td>
                    <td>
                        <shiro:user>
                            <shiro:principal property="account"/>
                        </shiro:user>
                    </td>
                </tr>
                <tr>
                    <td class="two wide column">最后登录时间：</td>
                    <td>
                        <shiro:user>
                            <shiro:principal property="lastLoginTime"/>
                        </shiro:user>
                    </td>
                </tr>
                <tr>
                    <td>最后登录地址：</td>
                    <td>   <shiro:user>
                        <shiro:principal property="lastLoginIp"/>
                    </shiro:user>
                    </td>
                </tr>


                </tbody>
            </table>


            <h4 class="ui horizontal divider header" style="width:60%;"><i class="bar chart icon"></i> 公告 </h4>

            <span id="message">


</span>

            <!-----内容结束----->

            <!----版权信息----->
            <div class="footer">
                <div class="pull-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2015
                </div>
            </div>

        </div>
        <!---右侧内容区结束----->


        <!---右侧下方小聊天框---->
        <div class="small-chat-box fadeInRight animated">

            <div class="heading" draggable="true">
                <small class="chat-date pull-right">
                    02.19.2015
                </small>
                Small chat
            </div>

            <div class="content">

                <div class="left">
                    <div class="author-name">
                        Monica Jackson <small class="chat-date">
                        10:02 am
                    </small>
                    </div>
                    <div class="chat-message active">
                        Lorem Ipsum is simply dummy text input.
                    </div>

                </div>
                <div class="right">
                    <div class="author-name">
                        Mick Smith
                        <small class="chat-date">
                            11:24 am
                        </small>
                    </div>
                    <div class="chat-message">
                        Lorem Ipsum is simpl.
                    </div>
                </div>
                <div class="left">
                    <div class="author-name">
                        Alice Novak
                        <small class="chat-date">
                            08:45 pm
                        </small>
                    </div>
                    <div class="chat-message active">
                        Check this stock char.
                    </div>
                </div>
                <div class="right">
                    <div class="author-name">
                        Anna Lamson
                        <small class="chat-date">
                            11:24 am
                        </small>
                    </div>
                    <div class="chat-message">
                        The standard chunk of Lorem Ipsum
                    </div>
                </div>
                <div class="left">
                    <div class="author-name">
                        Mick Lane
                        <small class="chat-date">
                            08:45 pm
                        </small>
                    </div>
                    <div class="chat-message active">
                        I belive that. Lorem Ipsum is simply dummy text.
                    </div>
                </div>
            </div>
            <div class="form-chat">
                <div class="input-group input-group-sm"><input type="text" class="form-control"> <span class="input-group-btn"> <button
                        class="btn btn-primary" type="button">Send
                </button> </span></div>
            </div>

        </div>
        <div id="small-chat">

            <span class="badge badge-warning pull-right">5</span>
            <a class="open-small-chat">
                <i class="fa fa-comments"></i>

            </a>
        </div>
    </div>

<!-- 全局 scripts -->
<script src="${ctx}/static/js/jquery-2.1.1.js"></script>
<script src="${ctx}/static/js/bootstrap.js"></script>
<script src="${ctx}/static/js/wuling.js"></script>
<script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>
<script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="${ctx}/static/js/plugins/multiselect/tree-multiselect.min.js"></script>
    <script type="text/javascript" src="${ctx}/static/css/Semantic/semantic.js"  charset="UTF-8"></script>
<!-- 插件 scripts -->
<script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script>
<!---顶部弹出提示--->

    <script>

        $(document).ready(function(){
            $.post('/message/getHomeMessage', function (result) {
                if (result!=null) {
                    var table="";
                    for(var i=0;i<result.model.length&&i<3;i++){
                        table+=" <table class=\"ui definition table\" style=\"width:60%;font-size: 15px\">\n" +
                                "                        <tbody>\n" +
                                "                        <tr>\n" +
                                "                        <td class=\"two wide column\">标题</td>\n" +
                                "                        <td id=\"title\">"+result.model[i].title+"</td>\n" +
                                "                        </tr>\n" +
                                "                        <tr>\n" +
                                "                        <td class=\"two wide column\">内容</td>\n" +
                                "                        <td id=\"content\">"+result.model[i].content+"</td>\n" +
                                "                        </tr>\n" +
                                "                        <tr>\n" +
                                "                        <td>发布人：</td>\n" +
                                "                    <td id=\"operator\">"+result.model[i].operator+"</td>\n" +
                                "                        </tr>\n" +
                                "                        <tr>\n" +
                                "                        <td>发布时间:</td>\n" +
                                "                    <td id=\"time\">"+result.model[i].createTime+"</td>\n" +
                                "                        </tr>\n" +
                                "\n" +
                                "                        </tbody>\n" +
                                "                        </table>";

                    }
                }
                $("#message").html(table);
            }, 'json');
        });
    </script>

    <style>
        .ui.table td.two.wide {
            width: 20%;
        }
    </style>

</body>
</html>
