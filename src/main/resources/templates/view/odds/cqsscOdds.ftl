<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">
    <link href="${ctx}/static/css/bet.css" rel="stylesheet">
    <link href="${ctx}/static/css/table.css" rel="stylesheet">



</head>

<body class="fixed-sidebar">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
            <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <ol class="breadcrumb">
                        <li>
                            <a href="${ctx}/static/index.html">赔率设置</a>
                        </li>
                        <li class="active">
                            重庆时时彩
                        </li>

                    </ol>
                </div>
            </div>

            <div id="bet_panel" class="bet_panel input_panel" >
                <table class="table_lm"  style="width: 60.4%"   valign="center" id="total">
                    <tbody><tr class="head"><th colspan="15">总和-龙虎和</th></tr>
                    <tr>
                        <th class="GZDX_D name" id="rule1" title=" 总和大"><input type="hidden" id="k_ZDX_D" value="1,2,3,4,5">总和大</th>
                        <td class="GZDX_D odds" id="od1"></td>
                        <td class="GZDX_D amount ha"><input id="odds1" class="ba"></td>


                        <th class="GZDX_X name" id="rule2" title=" 总和小"><input type="hidden" id="k_ZDX_X" value="1,2,3,4,5">总和小</th>
                        <td class="GZDX_X odds" id="od2"></td>
                        <td class="GZDX_X amount ha"><input id="odds2" class="ba"></td>


                        <th class="GZDS_D name" id="rule3" title=" 总和单"><input type="hidden" id="k_ZDS_D" value="1,2,3,4,5">总和单</th>
                        <td class="GZDS_D odds" id="od3"></td>
                        <td class="GZDS_D amount ha"><input id="odds3" class="ba"></td>


                        <th class="GZDS_S name" id="rule4" title=" 总和双"><input type="hidden" id="k_ZDS_S" value="1,2,3,4,5">总和双</th>
                        <td class="GZDS_S odds" id="od4"></td>
                        <td class="GZDS_S amount ha"><input id="odds4" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GLH_L name" id="rule5" title=" 龙"><input type="hidden" id="k_LH_L" value="1,5">龙</th>
                        <td class="GLH_L odds" id="od5"></td>
                        <td class="GLH_L amount ha"><input id="odds5" class="ba"></td>


                        <th class="GLH_H name" id="rule6" title=" 虎"><input type="hidden" id="k_LH_H" value="1,5">虎</th>
                        <td class="GLH_H odds" id="od6"></td>
                        <td class="GLH_H amount ha"><input id="odds6" class="ba"></td>


                        <th class="GLH_T name" id="rule7" title=" 和"><input type="hidden" id="k_LH_T" value="1,5">和</th>
                        <td class="GLH_T odds" id="od7"></td>
                        <td class="GLH_T amount ha"><input id="odds7" class="ba"></td>
                        <th id="e_EP0" class="GEP0 name empty"></th><td class="GEP0 odds empty"></td><td class="GEP0 amount ha"></td>
                    </tr>
                    </tbody></table>
                <div class="split_panel">

                    <table id="ball1">
                        <tbody>

                        <tr class="head"><th colspan="3">第一球</th></tr>
                        <tr>
                            <th class="GDX1_D name" id="rule8" title="第一球 大"><input type="hidden" id="k_DX1_D" value="1">大</th>
                            <td class="GDX1_D odds" id="od8"></td>
                            <td class="GDX1_D amount ha"><input id="odds8" class="ba"></td>
                        </tr>
                        <tr>
                            <th class="GDX1_X name" id="rule9" title="第一球 小"><input type="hidden" id="k_DX1_X" value="1">小</th>
                            <td class="GDX1_X odds" id="od9"></td>
                            <td class="GDX1_X amount ha"><input id="odds9" class="ba"></td>
                        </tr>
                        <tr>
                            <th class="GDS1_D name" id="rule10" title="第一球 单"><input type="hidden" id="k_DS1_D" value="1">单</th>
                            <td class="GDS1_D odds" id="od10"></td>
                            <td class="GDS1_D amount ha"><input id="odds10" class="ba"></td>
                        </tr>
                        <tr>
                            <th class="GDS1_S name" id="rule11" title="第一球 双"><input type="hidden" id="k_DS1_S" value="1">双</th>
                            <td class="GDS1_S odds" id="od11"></td>
                            <td class="GDS1_S amount ha"><input id="odds11" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GB1_0 name" id="rule12" title="第一球 0"><input type="hidden" id="k_B1_0" value="1"><span class="b0">0</span></th>
                            <td class="GB1_0 odds" id="od12"></td>
                            <td class="GB1_0 amount ha"><input id="odds12" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GB1_1 name" id="rule13" title="第一球 1"><input type="hidden" id="k_B1_1" value="1"><span class="b1">1</span></th>
                            <td class="GB1_1 odds" id="od13"></td>
                            <td class="GB1_1 amount ha"><input id="odds13" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GB1_2 name" id="rule14" title="第一球 2"><input type="hidden" id="k_B1_2" value="1"><span class="b2">2</span></th>
                            <td class="GB1_2 odds" id="od14"></td>
                            <td class="GB1_2 amount ha"><input id="odds14" class="ba"></td>
                        </tr>
                        <tr>
                            <th class="GB1_3 name" id="rule15" title="第一球 3"><input type="hidden" id="k_B1_3" value="1"><span class="b3">3</span></th>
                            <td class="GB1_3 odds" id="od15"></td>
                            <td class="GB1_3 amount ha"><input id="odds15" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GB1_4 name" id="rule16" title="第一球 4"><input type="hidden" id="k_B1_4" value="1"><span class="b4">4</span></th>
                            <td class="GB1_4 odds" id="od16"></td>
                            <td class="GB1_4 amount ha"><input id="odds16" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GB1_5 name" id="rule17" title="第一球 5"><input type="hidden" id="k_B1_5" value="1"><span class="b5">5</span></th>
                            <td class="GB1_5 odds" id="od17"></td>
                            <td class="GB1_5 amount ha"><input id="odds17" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GB1_6 name" id="rule18" title="第一球 6"><input type="hidden" id="k_B1_6" value="1"><span class="b6">6</span></th>
                            <td class="GB1_6 odds" id="od18"></td>
                            <td class="GB1_6 amount ha"><input id="odds18" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GB1_7 name" id="rule19" title="第一球 7"><input type="hidden" id="k_B1_7" value="1"><span class="b7">7</span></th>
                            <td class="GB1_7 odds" id="od19"></td>
                            <td class="GB1_7 amount ha"><input id="odds19" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GB1_8 name" id="rule20" title="第一球 8"><input type="hidden" id="k_B1_8" value="1"><span class="b8">8</span></th>
                            <td class="GB1_8 odds" id="od20"></td>
                            <td class="GB1_8 amount ha"><input id="odds20" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GB1_9 name" id="rule21" title="第一球 9"><input type="hidden" id="k_B1_9" value="1"><span class="b9">9</span></th>
                            <td class="GB1_9 odds" id="od21"></td>
                            <td class="GB1_9 amount ha"><input id="odds21" class="ba" ></td>
                        </tr>
                        </tbody></table>


                    <table id="ball2">
                        <tbody><tr class="head"><th colspan="3">第二球</th></tr>
                        <tr>
                            <th class="GDX2_D name" id="rule22" title="第二球 大"><input type="hidden" id="k_DX2_D" value="2">大</th>
                            <td class="GDX2_D odds" id="od22"></td>
                            <td class="GDX2_D amount ha"><input id="odds22" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GDX2_X name" id="rule23" title="第二球 小"><input type="hidden" id="k_DX2_X" value="2">小</th>
                            <td class="GDX2_X odds" id="od23"></td>
                            <td class="GDX2_X amount ha"><input id="odds23" class="ba"></td>
                        </tr>
                        <tr>
                            <th class="GDS2_D name" id="rule24" title="第二球 单"><input type="hidden" id="k_DS2_D" value="2">单</th>
                            <td class="GDS2_D odds" id="od24"></td>
                            <td class="GDS2_D amount ha"><input id="odds24" class="ba"></td>
                        </tr>
                        <tr>
                            <th class="GDS2_S name" id="rule25" title="第二球 双"><input type="hidden" id="k_DS2_S" value="2">双</th>
                            <td class="GDS2_S odds" id="od25"></td>
                            <td class="GDS2_S amount ha"><input id="odds25" class="ba"></td>
                        </tr>
                        <tr>
                            <th class="GB2_0 name" id="rule26" title="第二球 0"><input type="hidden" id="k_B2_0" value="2"><span class="b0">0</span></th>
                            <td class="GB2_0 odds" id="od26"></td>
                            <td class="GB2_0 amount ha"><input id="odds26" class="ba"></td>
                        </tr>
                        <tr>
                            <th class="GB2_1 name" id="rule27" title="第二球 1"><input type="hidden" id="k_B2_1" value="2"><span class="b1">1</span></th>
                            <td class="GB2_1 odds" id="od27"></td>
                            <td class="GB2_1 amount ha"><input id="odds27" class="ba"></td>
                        </tr>
                        <tr>
                            <th class="GB2_2 name" id="rule28" title="第二球 2"><input type="hidden" id="k_B2_2" value="2"><span class="b2">2</span></th>
                            <td class="GB2_2 odds" id="od28"></td>
                            <td class="GB2_2 amount ha"><input id="odds28" class="ba"></td>
                        </tr>
                        <tr>
                            <th class="GB2_3 name" id="rule29" title="第二球 3"><input type="hidden" id="k_B2_3" value="2"><span class="b3">3</span></th>
                            <td class="GB2_3 odds" id="od29"></td>
                            <td class="GB2_3 amount ha"><input id="odds29" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GB2_4 name" id="rule30" title="第二球 4"><input type="hidden" id="k_B2_4" value="2"><span class="b4">4</span></th>
                            <td class="GB2_4 odds" id="od30"></td>
                            <td class="GB2_4 amount ha"><input id="odds30" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GB2_5 name" id="rule31" title="第二球 5"><input type="hidden" id="k_B2_5" value="2"><span class="b5">5</span></th>
                            <td class="GB2_5 odds" id="od31"></td>
                            <td class="GB2_5 amount ha"><input id="odds31" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GB2_6 name" id="rule32" title="第二球 6"><input type="hidden" id="k_B2_6" value="2"><span class="b6">6</span></th>
                            <td class="GB2_6 odds" id="od32"></td>
                            <td class="GB2_6 amount ha"><input id="odds32" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GB2_7 name" id="rule33" title="第二球 7"><input type="hidden" id="k_B2_7" value="2"><span class="b7">7</span></th>
                            <td class="GB2_7 odds" id="od33"></td>
                            <td class="GB2_7 amount ha"><input id="odds33" class="ba"></td>
                        </tr>
                        <tr>
                            <th class="GB2_8 name" id="rule34" title="第二球 8"><input type="hidden" id="k_B2_8" value="2"><span class="b8">8</span></th>
                            <td class="GB2_8 odds" id="od34"></td>
                            <td class="GB2_8 amount ha"><input id="odds34" class="ba" ></td>
                        </tr>
                        <tr>
                            <th class="GB2_9 name" id="rule35" title="第二球 9"><input type="hidden" id="k_B2_9" value="2"><span class="b9">9</span></th>
                            <td class="GB2_9 odds" id="od35"></td>
                            <td class="GB2_9 amount ha"><input id="odds35" class="ba"></td>
                        </tr>
                        </tbody>
                    </table>
                    <table id="ball3">
                        <tbody><tr class="head"><th colspan="3">第三球</th></tr>
                        <tr><th class="GDX3_D name" id="rule36" title="第三球 大"><input type="hidden" id="k_DX3_D" value="3">大</th>

                            <td class="GDX3_D odds" id="od36"></td>
                            <td class="GDX3_D amount ha"><input id="odds36" class="ba"></td>
                        </tr>
                        <tr><th class="GDX3_X name" id="rule37" title="第三球 小"><input type="hidden" id="k_DX3_X" value="3">小</th>

                            <td class="GDX3_X odds" id="od37"></td>
                            <td class="GDX3_X amount ha"><input id="odds37" class="ba"></td>
                        </tr>
                        <tr><th class="GDS3_D name" id="rule38" title="第三球 单"><input type="hidden" id="k_DS3_D" value="3">单</th>

                            <td class="GDS3_D odds" id="od38"></td>
                            <td class="GDS3_D amount ha"><input id="odds38" class="ba"></td>
                        </tr>
                        <tr><th class="GDS3_S name" id="rule39" title="第三球 双"><input type="hidden" id="k_DS3_S" value="3">双</th>

                            <td class="GDS3_S odds" id="od39"></td>
                            <td class="GDS3_S amount ha"><input id="odds39" class="ba"></td>
                        </tr>
                        <tr><th class="GB3_0 name" id="rule40" title="第三球 0"><input type="hidden" id="k_B3_0" value="3"><span class="b0">0</span></th>

                            <td class="GB3_0 odds" id="od40"></td>
                            <td class="GB3_0 amount ha"><input id="odds40" class="ba"></td>
                        </tr>
                        <tr><th class="GB3_1 name" id="rule41" title="第三球 1"><input type="hidden" id="k_B3_1" value="3"><span class="b1">1</span></th>

                            <td class="GB3_1 odds" id="od41"></td>
                            <td class="GB3_1 amount ha"><input id="odds41" class="ba"></td>
                        </tr>
                        <tr><th class="GB3_2 name" id="rule42" title="第三球 2"><input type="hidden" id="k_B3_2" value="3"><span class="b2">2</span></th>

                            <td class="GB3_2 odds" id="od42"></td>
                            <td class="GB3_2 amount ha"><input id="odds42" class="ba"></td>
                        </tr>
                        <tr><th class="GB3_3 name" id="rule43" title="第三球 3"><input type="hidden" id="k_B3_3" value="3"><span class="b3">3</span></th>

                            <td class="GB3_3 odds" id="od43"></td>
                            <td class="GB3_3 amount ha"><input id="odds43" class="ba"></td>
                        </tr>
                        <tr><th class="GB3_4 name" id="rule44" title="第三球 4"><input type="hidden" id="k_B3_4" value="3"><span class="b4">4</span></th>

                            <td class="GB3_4 odds" id="od44"></td>
                            <td class="GB3_4 amount ha"><input id="odds44" class="ba"></td>
                        </tr>
                        <tr><th class="GB3_5 name" id="rule45" title="第三球 5"><input type="hidden" id="k_B3_5" value="3"><span class="b5">5</span></th>

                            <td class="GB3_5 odds" id="od45"></td>
                            <td class="GB3_5 amount ha"><input id="odds45" class="ba"></td>
                        </tr>
                        <tr><th class="GB3_6 name" id="rule46" title="第三球 6"><input type="hidden" id="k_B3_6" value="3"><span class="b6">6</span></th>

                            <td class="GB3_6 odds" id="od46"></td>
                            <td class="GB3_6 amount ha"><input id="odds46" class="ba"></td>
                        </tr>
                        <tr><th class="GB3_7 name" id="rule47" title="第三球 7"><input type="hidden" id="k_B3_7" value="3"><span class="b7">7</span></th>

                            <td class="GB3_7 odds" id="od47"></td>
                            <td class="GB3_7 amount ha"><input id="odds47" class="ba"></td>
                        </tr>
                        <tr><th class="GB3_8 name" id="rule48" title="第三球 8"><input type="hidden" id="k_B3_8" value="3"><span class="b8">8</span></th>

                            <td class="GB3_8 odds" id="od48"></td>
                            <td class="GB3_8 amount ha"><input id="odds48"  class="ba"></td>
                        </tr>
                        <tr><th class="GB3_9 name" id="rule49" title="第三球 9"><input type="hidden" id="k_B3_9" value="3"><span class="b9">9</span></th>

                            <td class="GB3_9 odds" id="od49"></td>
                            <td class="GB3_9 amount ha"><input id="odds49"  class="ba"></td>
                        </tr>
                        </tbody></table>
                    <table id="ball4">
                        <tbody><tr class="head"><th colspan="3">第四球</th></tr>
                        <tr><th class="GDX4_D name" id="rule50" title="第四球 大"><input type="hidden" id="k_DX4_D" value="4">大</th>

                            <td class="GDX4_D odds" id="od50"></td>
                            <td class="GDX4_D amount ha"><input id="odds50" class="ba"></td>
                        </tr>
                        <tr><th class="GDX4_X name" id="rule51" title="第四球 小"><input type="hidden" id="k_DX4_X" value="4">小</th>

                            <td class="GDX4_X odds" id="od51"></td>
                            <td class="GDX4_X amount ha"><input id="odds51" class="ba"></td>
                        </tr>
                        <tr><th class="GDS4_D name" id="rule52" title="第四球 单"><input type="hidden" id="k_DS4_D" value="4">单</th>

                            <td class="GDS4_D odds" id="od52"></td>
                            <td class="GDS4_D amount ha"><input id="odds52" class="ba"></td>
                        </tr>
                        <tr><th class="GDS4_S name" id="rule53" title="第四球 双"><input type="hidden" id="k_DS4_S" value="4">双</th>

                            <td class="GDS4_S odds" id="od53"></td>
                            <td class="GDS4_S amount ha"><input id="odds53" class="ba"></td>
                        </tr>
                        <tr><th class="GB4_0 name" id="rule54" title="第四球 0"><input type="hidden" id="k_B4_0" value="4"><span class="b0">0</span></th>

                            <td class="GB4_0 odds" id="od54"></td>
                            <td class="GB4_0 amount ha"><input id="odds54" class="ba"></td>
                        </tr>
                        <tr><th class="GB4_1 name" id="rule55" title="第四球 1"><input type="hidden" id="k_B4_1" value="4"><span class="b1">1</span></th>

                            <td class="GB4_1 odds" id="od55"></td>
                            <td class="GB4_1 amount ha"><input id="odds55" class="ba"></td>
                        </tr>
                        <tr><th class="GB4_2 name" id="rule56" title="第四球 2"><input type="hidden" id="k_B4_2" value="4"><span class="b2">2</span></th>

                            <td class="GB4_2 odds" id="od56"></td>
                            <td class="GB4_2 amount ha"><input id="odds56" class="ba"></td>
                        </tr>
                        <tr><th class="GB4_3 name" id="rule57" title="第四球 3"><input type="hidden" id="k_B4_3" value="4"><span class="b3">3</span></th>

                            <td class="GB4_3 odds" id="od57"></td>
                            <td class="GB4_3 amount ha"><input id="odds57" class="ba"></td>
                        </tr>
                        <tr><th class="GB4_4 name" id="rule58" title="第四球 4"><input type="hidden" id="k_B4_4" value="4"><span class="b4">4</span></th>

                            <td class="GB4_4 odds" id="od58"></td>
                            <td class="GB4_4 amount ha"><input id="odds58" class="ba"></td>
                        </tr>
                        <tr><th class="GB4_5 name" id="rule59" title="第四球 5"><input type="hidden" id="k_B4_5" value="4"><span class="b5">5</span></th>

                            <td class="GB4_5 odds" id="od59"></td>
                            <td class="GB4_5 amount ha"><input id="odds59" class="ba"></td>
                        </tr>
                        <tr><th class="GB4_6 name" id="rule60" title="第四球 6"><input type="hidden" id="k_B4_6" value="4"><span class="b6">6</span></th>

                            <td class="GB4_6 odds" id="od60"></td>
                            <td class="GB4_6 amount ha"><input id="odds60" class="ba"></td>
                        </tr>
                        <tr><th class="GB4_7 name" id="rule61" title="第四球 7"><input type="hidden" id="k_B4_7" value="4"><span class="b7">7</span></th>

                            <td class="GB4_7 odds" id="od61"></td>
                            <td class="GB4_7 amount ha"><input id="odds61" class="ba"></td>
                        </tr>
                        <tr><th class="GB4_8 name" id="rule62" title="第四球 8"><input type="hidden" id="k_B4_8" value="4"><span class="b8">8</span></th>

                            <td class="GB4_8 odds" id="od62"></td>
                            <td class="GB4_8 amount ha"><input id="odds62" class="ba"></td>
                        </tr>
                        <tr><th class="GB4_9 name" id="rule63" title="第四球 9"><input type="hidden" id="k_B4_9" value="4"><span class="b9">9</span></th>

                            <td class="GB4_9 odds" id="od63"></td>
                            <td class="GB4_9 amount ha"><input id="odds63" class="ba"></td>
                        </tr>
                        </tbody>
                    </table>
                    <table id="ball5">
                        <tbody><tr class="head"><th colspan="3">第五球</th></tr>
                        <tr><th class="GDX5_D name" id="rule64" title="第五球 大"><input type="hidden" id="k_DX5_D" value="5">大</th>

                            <td class="GDX5_D odds" id="od64"></td>
                            <td class="GDX5_D amount ha"><input id="odds64" class="ba"></td>
                        </tr>
                        <tr><th class="GDX5_X name" id="rule65" title="第五球 小"><input type="hidden" id="k_DX5_X" value="5">小</th>

                            <td class="GDX5_X odds" id="od65"></td>
                            <td class="GDX5_X amount ha"><input id="odds65" class="ba"></td>
                        </tr>
                        <tr><th class="GDS5_D name" id="rule66" title="第五球 单"><input type="hidden" id="k_DS5_D" value="5">单</th>

                            <td class="GDS5_D odds" id="od66"></td>
                            <td class="GDS5_D amount ha"><input id="odds66" class="ba"></td>
                        </tr>
                        <tr><th class="GDS5_S name" id="rule67" title="第五球 双"><input type="hidden" id="k_DS5_S" value="5">双</th>

                            <td class="GDS5_S odds" id="od67"></td>
                            <td class="GDS5_S amount ha"><input id="odds67" class="ba"></td>
                        </tr>
                        <tr><th class="GB5_0 name" id="rule68" title="第五球 0"><input type="hidden" id="k_B5_0" value="5"><span class="b0">0</span></th>

                            <td class="GB5_0 odds" id="od68"></td>
                            <td class="GB5_0 amount ha"><input id="odds68" class="ba"></td>
                        </tr>
                        <tr><th class="GB5_1 name" id="rule69" title="第五球 1"><input type="hidden" id="k_B5_1" value="5"><span class="b1">1</span></th>

                            <td class="GB5_1 odds" id="od69"></td>
                            <td class="GB5_1 amount ha"><input id="odds69" class="ba"></td>
                        </tr>
                        <tr><th class="GB5_2 name" id="rule70" title="第五球 2"><input type="hidden" id="k_B5_2" value="5"><span class="b2">2</span></th>

                            <td class="GB5_2 odds" id="od70"></td>
                            <td class="GB5_2 amount ha"><input id="odds70" class="ba"></td>
                        </tr>
                        <tr><th class="GB5_3 name" id="rule71" title="第五球 3"><input type="hidden" id="k_B5_3" value="5"><span class="b3">3</span></th>

                            <td class="GB5_3 odds" id="od71"></td>
                            <td class="GB5_3 amount ha"><input id="odds71" class="ba"></td>
                        </tr>
                        <tr><th class="GB5_4 name" id="rule72" title="第五球 4"><input type="hidden" id="k_B5_4" value="5"><span class="b4">4</span></th>

                            <td class="GB5_4 odds" id="od72"></td>
                            <td class="GB5_4 amount ha"><input id="odds72" class="ba"></td>
                        </tr>
                        <tr><th class="GB5_5 name" id="rule73" title="第五球 5"><input type="hidden" id="k_B5_5" value="5"><span class="b5">5</span></th>

                            <td class="GB5_5 odds" id="od73"></td>
                            <td class="GB5_5 amount ha"><input id="odds73" class="ba"></td>
                        </tr>
                        <tr><th class="GB5_6 name" id="rule74" title="第五球 6"><input type="hidden" id="k_B5_6" value="5"><span class="b6">6</span></th>

                            <td class="GB5_6 odds" id="od74"></td>
                            <td class="GB5_6 amount ha"><input id="odds74" class="ba"></td>
                        </tr>
                        <tr><th class="GB5_7 name" id="rule75" title="第五球 7"><input type="hidden" id="k_B5_7" value="5"><span class="b7">7</span></th>

                            <td class="GB5_7 odds" id="od75"></td>
                            <td class="GB5_7 amount ha"><input id="odds75" class="ba"></td>
                        </tr>
                        <tr><th class="GB5_8 name" id="rule76" title="第五球 8"><input type="hidden" id="k_B5_8" value="5"><span class="b8">8</span></th>

                            <td class="GB5_8 odds" id="od76"></td>
                            <td class="GB5_8 amount ha"><input id="odds76" class="ba"></td>
                        </tr>
                        <tr><th class="GB5_9 name" id="rule77" title="第五球 9"><input type="hidden" id="k_B5_9" value="5"><span class="b9">9</span></th>

                            <td class="GB5_9 odds" id="od77"></td>
                            <td class="GB5_9 amount ha"><input id="odds77" class="ba"></td>
                        </tr>
                        </tbody></table>
                </div>
                <div style="width: 60.4%" class="split_panel">
                    <table class="table_ts mt10" id="firstThree"  style="width: 33%">
                        <tbody>
                        <tr class="head"><th colspan="20">前三</th></tr>
                        <tr>
                            <th class="GTS1_0 name" id="rule78" title="前三 豹子"><input type="hidden" id="k_TS1_0" value="1,2,3">豹子</th>
                            <td class="GTS1_0 odds" id="od78"></td>
                            <td class="GTS1_0 amount ha"><input id="odds78" class="ba"></td>
                        </tr>

                        <tr>
                            <th class="GTS1_1 name" id="rule79" title="前三 顺子"><input type="hidden" id="k_TS1_1" value="1,2,3">顺子</th>
                            <td class="GTS1_1 odds" id="od79"></td>
                            <td class="GTS1_1 amount ha"><input id="odds79" class="ba"></td>
                        </tr>

                        <tr>
                            <th class="GTS1_2 name" id="rule80" title="前三 对子"><input type="hidden" id="k_TS1_2" value="1,2,3">对子</th>
                            <td class="GTS1_2 odds" id="od80"></td>
                            <td class="GTS1_2 amount ha"><input id="odds80" class="ba"></td>
                        </tr>

                        <tr>
                            <th class="GTS1_3 name" id="rule81" title="前三 半顺"><input type="hidden" id="k_TS1_3" value="1,2,3">半顺</th>
                            <td class="GTS1_3 odds" id="od81"></td>
                            <td class="GTS1_3 amount ha"><input id="odds81" class="ba"></td>
                        </tr>

                        <tr>
                            <th class="GTS1_4 name" id="rule82" title="前三 杂六"><input type="hidden" id="k_TS1_4" value="1,2,3">杂六</th>
                            <td class="GTS1_4 odds" id="od82"></td>
                            <td class="GTS1_4 amount ha"><input id="odds82" class="ba"></td>
                        </tr>
                        </tbody >
                    </table>
                    <table id="centerThree" style="width: 33%">
                        <tbody >
                        <tr class="head"><th colspan="20">中三</th></tr>
                        <tr>
                            <th class="GTS2_0 name" id="rule83" title="中三 豹子"><input type="hidden" id="k_TS2_0" value="2,3,4">豹子</th>
                            <td class="GTS2_0 odds" id="od83"></td>
                            <td class="GTS2_0 amount ha"><input id="odds83" class="ba"></td>
                        </tr>

                        <tr>
                            <th class="GTS2_1 name" id="rule84" title="中三 顺子"><input type="hidden" id="k_TS2_1" value="2,3,4">顺子</th>
                            <td class="GTS2_1 odds" id="od84"></td>
                            <td class="GTS2_1 amount ha"><input id="odds84" class="ba"></td>
                        </tr>

                        <tr>
                            <th class="GTS2_2 name" id="rule85" title="中三 对子"><input type="hidden" id="k_TS2_2" value="2,3,4">对子</th>
                            <td class="GTS2_2 odds" id="od85"></td>
                            <td class="GTS2_2 amount ha"><input id="odds85" class="ba"></td>
                        </tr>

                        <tr>
                            <th class="GTS2_3 name" id="rule86" title="中三 半顺"><input type="hidden" id="k_TS2_3" value="2,3,4">半顺</th>
                            <td class="GTS2_3 odds" id="od86"></td>
                            <td class="GTS2_3 amount ha"><input id="odds86" class="ba"></td>
                        </tr>

                        <tr>
                            <th class="GTS2_4 name" id="rule87" title="中三 杂六"><input type="hidden" id="k_TS2_4" value="2,3,4">杂六</th>
                            <td class="GTS2_4 odds" id="od87"></td>
                            <td class="GTS2_4 amount ha"><input id="odds87" class="ba"></td>
                        </tr>
                        </tbody>
                    </table>
                    <table id="lastThree" style="width: 33%">
                        <tbody>
                        <tr class="head"><th colspan="20">后三</th></tr>

                        <tr>
                            <th class="GTS3_0 name" id="rule88" title="后三 豹子"><input type="hidden" id="k_TS3_0" value="3,4,5">豹子</th>
                            <td class="GTS3_0 odds" id="od88" style="width: 56px"></td>
                            <td class="GTS3_0 amount ha" style="width: 94px"><input id="odds88"  class="ba"></td>
                        </tr>

                        <tr>
                            <th class="GTS3_1 name" id="rule89" title="后三 顺子"><input type="hidden" id="k_TS3_1" value="3,4,5">顺子</th>
                            <td class="GTS3_1 odds" style="width: 56px" id="od89"></td>
                            <td class="GTS3_1 amount ha" ><input id="odds89" class="ba"></td>
                        </tr>

                        <tr>
                            <th class="GTS3_2 name" id="rule90" title="后三 对子"><input type="hidden" id="k_TS3_2" value="3,4,5">对子</th>
                            <td class="GTS3_2 odds" id="od90"></td>
                            <td class="GTS3_2 amount ha"><input id="odds90" class="ba"></td>
                        </tr>

                        <tr>
                            <th class="GTS3_3 name" id="rule91" title="后三 半顺"><input type="hidden" id="k_TS3_3" value="3,4,5">半顺</th>
                            <td class="GTS3_3 odds" id="od91"></td>
                            <td class="GTS3_3 amount ha"><input id="odds91" class="ba"></td>
                        </tr>

                        <tr>
                            <th class="GTS3_4 name" id="rule92" title="后三 杂六"><input type="hidden" id="k_TS3_4" value="3,4,5">杂六</th>
                            <td class="GTS3_4 odds" id="od92"></td>
                            <td class="GTS3_4 amount ha"><input id="odds92" class="ba"></td>
                        </tr>
                        </tbody>
                    </table>


                    <div class="control bcontrol">
                        <div class="buttons">
                            <a class="sel_btn" onclick="bet()" href="javascript:">提交</a>
                            <a class="sel_btn" href="view/odds/cqsscOdds?lotteryId=1">重置/刷新</a>

                        </div>
                    </div>




                </div>

            </div>



            <!----版权信息----->
            <div class="footer">
                <div class="pull-right">
                    一只 <strong>咸鱼</strong>
                </div>
                <div>
                    <strong>作者:Eric</strong>  版权所有 &copy; 2017-未来
                </div>
            </div>
        </div>

    </div>

    <script src="${ctx}/static/js/jquery-2.1.1.js"></script>
    <script src="${ctx}/static/js/bootstrap.js"></script>
    <script src="${ctx}/static/js/wuling.js"></script>
    <script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>

    <script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <!-- 插件 scripts -->
    <script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script><!---顶部弹出提示--->
    <script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>  <!---表单验证--->
    <script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script> <!---validate 自定义方法--->
    <script>
        $(document).ready(function(){

            console.log(${json});
            var json=${json};
            for(var i=0;i<json.length;i++){
                for(var j=1;j<=92;j++){
                    var id="rule"+j;
                    var tile=$("#"+id+"").children("span").text();
                    var balls=$("#"+id+"").children("input").val();
                    var text=$("#"+id+"").text();

                    var tt=$("#"+id+"").children("div").children("span").text();

                    if((json[i].rule==tile&&json[i].balls==balls)||(json[i].rule==text&&json[i].balls==balls)){
                        $("#od"+j+"").text(json[i].odds);
                        $("#odds"+j+"").val(json[i].odds);
                    }
                }
            }
        });


        function bet(){

            //把table的数据转换成json格式
            var total = document.getElementById("total");
            var one = document.getElementById("ball1");
            var two = document.getElementById("ball2");
            var three = document.getElementById("ball3");
            var four = document.getElementById("ball4");
            var five = document.getElementById("ball5");

            var firstThree = document.getElementById("firstThree");
            var centerThree = document.getElementById("centerThree");
            var lastThree = document.getElementById("lastThree");

            var jsonT = "[";
            for (var i = 1; i < one.rows.length; i++) {
                jsonT += '{"rule":"' + one.rows[i].cells[0].innerText + '","odds":"' + one.rows[i].cells[2].firstChild.value + '","lid":1,"balls":"1"},'
            }
            for (var i = 1; i < two.rows.length; i++) {
                jsonT += '{"rule":"' + two.rows[i].cells[0].innerText + '","odds":"' + two.rows[i].cells[2].firstChild.value + '","lid":1,"balls":"2"},'
            }
            for (var i = 1; i < three.rows.length; i++) {
                jsonT += '{"rule":"' + three.rows[i].cells[0].innerText + '","odds":"' + three.rows[i].cells[2].firstChild.value + '","lid":1,"balls":"3"},'
            }
            for (var i = 1; i < four.rows.length; i++) {
                jsonT += '{"rule":"' + four.rows[i].cells[0].innerText + '","odds":"' + four.rows[i].cells[2].firstChild.value + '","lid":1,"balls":"4"},'
            }
            for (var i = 1; i < five.rows.length; i++) {
                jsonT += '{"rule":"' + five.rows[i].cells[0].innerText + '","odds":"' + five.rows[i].cells[2].firstChild.value + '","lid":1,"balls":"5"},'
            }

            for (var i = 1; i < firstThree.rows.length; i++) {
                jsonT += '{"rule":"' + firstThree.rows[i].cells[0].innerText + '","odds":"' + firstThree.rows[i].cells[2].firstChild.value + '","lid":1,"balls":"1,2,3"},'
            }
            for (var i = 1; i < centerThree.rows.length; i++) {
                jsonT += '{"rule":"' + centerThree.rows[i].cells[0].innerText + '","odds":"' + centerThree.rows[i].cells[2].firstChild.value + '","lid":1,"balls":"2,3,4"},'
            }
            for (var i = 1; i < lastThree.rows.length; i++) {
                jsonT += '{"rule":"' + lastThree.rows[i].cells[0].innerText + '","odds":"' + lastThree.rows[i].cells[2].firstChild.value + '","lid":1,"balls":"3,4,5"},'
            }





            for (var i = 1; i < total.rows.length; i++) {
                if(i==1){
                    jsonT += '{"rule":"' + total.rows[i].cells[0].innerText + '","odds":"' + total.rows[i].cells[2].firstChild.value + '","lid":1,"balls":"1,2,3,4,5"},'
                    jsonT += '{"rule":"' + total.rows[i].cells[3].innerText + '","odds":"' + total.rows[i].cells[5].firstChild.value + '","lid":1,"balls":"1,2,3,4,5"},'
                    jsonT += '{"rule":"' + total.rows[i].cells[6].innerText + '","odds":"' + total.rows[i].cells[8].firstChild.value + '","lid":1,"balls":"1,2,3,4,5"},'
                    jsonT += '{"rule":"' + total.rows[i].cells[9].innerText + '","odds":"' + total.rows[i].cells[11].firstChild.value + '","lid":1,"balls":"1,2,3,4,5"},'
                }else {
                    jsonT += '{"rule":"' + total.rows[i].cells[0].innerText + '","odds":"' + total.rows[i].cells[2].firstChild.value + '","lid":1,"balls":"1,5"},'
                    jsonT += '{"rule":"' + total.rows[i].cells[3].innerText + '","odds":"' + total.rows[i].cells[5].firstChild.value + '","lid":1,"balls":"1,5"},'
                    jsonT += '{"rule":"' + total.rows[i].cells[6].innerText + '","odds":"' + total.rows[i].cells[8].firstChild.value + '","lid":1,"balls":"1,5"},'
                }

            }
            jsonT= jsonT.substr(0, jsonT.length - 1);

            jsonT += "]";
            console.log(jsonT);
            $.ajax({
                type: 'post',
                url: '/odds/updateOdds',
                data:{"jsonStr":JSON.stringify(jsonT)},
                success:function(response) {

                    if (response.status==200) {
                        $.dialog.tips("更新成功！");

                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    } else {
                        $.dialog.error(response.message);
                    }
                },
                error:function(response){
                    alert("---error:"+response);
                    $.dialog.error(response.message);
                }
            });

        }

    </script>

    <style>
        .split_panel {
            width:50%
        }
        .split_panel table {
            float: left;
            width: 30%;
            margin: 10px 2.25px 10px 0px;
        }

        .title {
            background: #f9f9f9 url(../img/bg-content-title.png);
            border-color: #fbfbfb !important;
            color: #111113;
            width: 20%;
        }

        .sel_btn {
            height: 25px;
            line-height: 25px;
            padding: 0 11px;
            background: #a1a9b1;
            border: 1px #abb8bb solid;
            border-radius: 3px;
            /* color: #fff; */
            display: inline-block;
            text-decoration: none;
            font-size: 14px;
            outline: none;
        }

        .navbar-fixed-top {
            height: 65px;
            margin-left: 5px;
        }
    </style>

</body>
</html>
