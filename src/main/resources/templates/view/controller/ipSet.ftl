<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">


    <style>


    </style>
</head>

<body class="fixed-sidebar">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
            <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4" style="width: 20%">
                    <ol class="breadcrumb">
                        <li style="color: #0d8ddb;font-weight: bold">
                            当前位置:
                        </li>
                        <li>
                            <a href="${ctx}/static/index.html">游戏设置</a>
                        </li>
                        <li class="active">
                            IP白名单
                        </li>

                    </ol>
                </div>

                <div id="main-header" class="col-sm-4" style="font-size: 14px;margin-top: 10px;margin-left: 0px;color: #0e9aef">
                    <p   class="main-timer"><b>${user.attributes.realname }</b><span id="show-timer" ></span></p>
                </div>

                <div  class="col-sm-4" style="margin-top: 10px;width: 20% ">
                    <a href="${ctx}/logout"><i class="fa fa-sign-out"></i>退出登录</a>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="ibox-content m-b-sm border-bottom">
                    <div clss="well">
                        <div class="form-group">

                                <a class="btn btn-success" onclick="$('#addIpWhite').modal();">增加</a>&nbsp;&nbsp;
                                <a class="btn btn-success" href="/game/getIpWhite">刷新</a>


                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="table-responsive ">
                                    <table class="table table-centerbody table-striped table-condensed text-nowrap" id="editable-sample">
                                        <thead>
                                            <tr>
                                                <th >IP</th>
                                                <th >备注</th>
                                                <th >操作人</th>
                                                <th >操作时间</th>
                                                <th >操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                            <#if model?? && (model?size > 0) >
                                <#list model as info>
                                            <tr>
                                                <td >
                                                ${info.ip}
                                                </td>
                                                <td  >
                                                ${info.remark}
                                                </td>
                                                <td>
                                                ${info.operator}
                                                </td>
                                                <td>
                                               <#-- ${info.operateTime}-->
                                                    2017
                                                </td>
                                                <td>
                                                    <i class="glyphicon glyphicon-remove"></i><a href="javascript:delIpWhite('${info.ip}');">删除</a>
                                                </td>
                                            </tr>

                                </#list>
                            </#if>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="addIpWhite" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" >添加IP白名单</h4>
                        </div>
                        <div class="modal-body">
                            <form id="ipWhiteForm" enctype="multipart/form-data" action="/game/addIpWhite"  method="post">
                                <div class="form-group">
                                    <label  class="control-label">IP地址:</label>
                                    <input type="text" style="height:30px" class="form-control" name="ip" id="ip" placeholder="请输入IP地址"/>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label">备注信息</label>
                                    <input type="text" style="height:30px" class="form-control" name="remark" id="remark" placeholder="请输入备注信息"/>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                                    <button type="submit"  class="btn btn-primary">提交</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="pull-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2015
                </div>
            </div>
        </div>


    </div>


    <script src="${ctx}/static/js/jquery-2.1.1.js"></script>
    <script src="${ctx}/static/js/bootstrap.js"></script>
    <script src="${ctx}/static/js/wuling.js"></script>
    <script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>
    <script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script>
    <script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script>
    <script src="${ctx}/static/js/My97DatePicker/WdatePicker.js" ></script>
    <script src="${ctx}/static/js/layer/layer.js" ></script>
    <script type="text/javascript" src="${ctx}/static/js/jquery.common-1.0.0.js" charset="UTF-8"></script>


    <script>
        $(function(){
            var load;
            $("#ipWhiteForm").ajaxForm({
                success:function (result){
                    layer.close(load);
                    if(result && result.status != 200){
                        return layer.msg(result.message,function(){}),!1;
                    }else{
                        layer.msg(result.message);
                        $("form :password").val('');

                        setTimeout(function(){
                            //3秒后刷新
                            layer.close();
                            location.reload();
                        },2000);
                    }
                },
                beforeSubmit:function(){
                    //判断参数
                    if($.trim($("#ip").val()) == ''){
                        layer.msg('请输入Ip地址',function(){});
                        $("#ip").parent().removeClass('has-success').addClass('has-error');
                        return !1;
                    }else{
                        $("#account").parent().removeClass('has-error').addClass('has-success');
                    }



                    load = layer.load('正在提交！！！');
                },
                dataType:"json",
                clearForm:false
            });

        });

        function delIpWhite(ip){
            var index = layer.confirm("确定删除这个IP？",function(){
                var load = layer.load();
                $.post('/game/delIpWhite',{ip:ip},function(result){
                    layer.close(load);
                    if(result && result.status == 200){

                        layer.msg(result.message);
                        setTimeout(function(){
                            //3秒后刷新
                            location.reload();
                        },3000);
                    }else{
                        return layer.msg(result.message,so.default),!0;
                    }
                },'json');
                layer.close(index);
            });
        }

  </script>
</body>
</html>
