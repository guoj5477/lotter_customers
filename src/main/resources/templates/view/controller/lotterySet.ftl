<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">
    <link href="${ctx}/static/css/Semantic/semantic.css" rel="stylesheet">



    <style>


    </style>
</head>

<body class="fixed-sidebar">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
            <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4" style="width: 24%">
                    <ol class="breadcrumb">
                        <li style="color: #0d8ddb;font-weight: bold">
                            当前位置:
                        </li>
                        <li>
                            <a href="${ctx}/static/index.html">彩票设置</a>
                        </li>
                        <li class="active">
                            封盘设置
                        </li>

                    </ol>
                </div>

                <div id="main-header" class="col-sm-4" style="font-size: 14px;margin-top: 10px;margin-left: 0px;color: #0e9aef">
                    <p   class="main-timer"><b>${user.attributes.realname }</b><span id="show-timer" ></span></p>
                </div>

                <div  class="col-sm-4" style="margin-top: 10px;width: 20% ">
                    <a href="${ctx}/logout"><i class="fa fa-sign-out"></i>退出登录</a>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="table-responsive ">
                                    <table class="table table-centerbody table-striped table-condensed text-nowrap" id="editable-sample">
                                        <thead>
                                            <tr>
                                                <th>编号</th>
                                                <th>名称</th>
                                                <th>状态</th>
                                                <th>分类</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                            <#if model??  && (model?size > 0) >
                                <#list model as context>
                                            <tr>
                                                <td style="vertical-align: middle">${context.id}</td>
                                                <td style="vertical-align: middle">

                                                    <#if context.code == 'cqssc'>
                                                        重庆时时彩

                                                    <#elseif context.code == 'gxklsf'>
                                                        广西快乐十分

                                                    <#elseif context.code == 'xync'>
                                                        幸运农场

                                                    <#elseif context.code == 'jsks'>
                                                        江苏快三
                                                    </#if>
                                                </td>
                                                <td style="vertical-align: middle">

                                                        <#if context.flag == '1'>
                                                            开启

                                                        <#else>
                                                            关闭
                                                        </#if>

                                                </td>
                                                <td style="vertical-align: middle">
                                                    <#if context.type == 'gate'>
                                                        封盘
                                                    </#if>
                                                </td>

                                                <td>
                                                    <div class="ui toggle checkbox">

                                                            <#if context.flag == '1'>
                                                                <input type="checkbox" checked="checked" name="SysSwitch" onclick="updateSwitch(${context.id},this)">
                                                                <label>开关</label>
                                                            <#else >
                                                                <input type="checkbox"  name="SysSwitch" onclick="updateSwitch(${context.id},this)">
                                                                <label>开关</label>
                                                            </#if>
                                                    </div>
                                                </td>
                                            </tr>

                                </#list>
                            </#if>
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>



            <div class="footer">
                <div class="pull-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2015
                </div>
            </div>
        </div>


    </div>


    <script src="${ctx}/static/js/jquery-2.1.1.js"></script>
    <script src="${ctx}/static/js/bootstrap.js"></script>
    <script src="${ctx}/static/js/wuling.js"></script>
    <script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>
    <script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script>
    <script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script>
    <script src="${ctx}/static/js/My97DatePicker/WdatePicker.js" ></script>
    <script src="${ctx}/static/css/Semantic/semantic.js" ></script>
    <script src="${ctx}/static/js/layer/layer.js" ></script>
    <script type="text/javascript" src="${ctx}/static/js/jquery.common-1.0.0.js" charset="UTF-8"></script>


    <script>
        function updateSwitch(val,obj) {

            if (obj.checked) {
                var flag = 1;

            } else {
                var flag = 0;
            }

            $.post('/game/updateLottSet', {id: val, flag: flag}, function (result) {

                if (result && result.status == 200) {

                    layer.msg(result.message);
                    setTimeout(function () {
                        //3秒后刷新
                        location.reload();
                    }, 3000);
                } else {
                    return layer.msg(result.message, so.default), !0;
                }
            }, 'json');
        }
    </script>
</body>
</html>
