<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">


    <style>


    </style>
</head>

<body class="fixed-sidebar">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
            <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4" style="width: 24%">
                    <ol class="breadcrumb">
                        <li style="color: #0d8ddb;font-weight: bold">
                            当前位置:
                        </li>
                        <li>
                            <a href="${ctx}/static/index.html">活动与信息管理</a>
                        </li>
                        <li class="active">
                            彩票公告
                        </li>

                    </ol>
                </div>

                <div id="main-header" class="col-sm-4" style="font-size: 14px;margin-top: 10px;margin-left: 0px;color: #0e9aef">
                    <p   class="main-timer"><b>${user.attributes.realname }</b><span id="show-timer" ></span></p>
                </div>

                <div  class="col-sm-4" style="margin-top: 10px;width: 20% ">
                    <a href="${ctx}/logout"><i class="fa fa-sign-out"></i>退出登录</a>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="ibox-content m-b-sm border-bottom">
                    <div class="row">
                        <div clss="well">
                            <div class="form-group">
<#--
                                <shiro.hasPermission name="admin">-->
                                    <a class="btn btn-success" onclick="$('#addHomeMessage').modal();">增加</a>&nbsp;&nbsp;
                                    <a class="btn btn-success" href="view/notice/backstageNotice">刷新</a>
                             <#--   </shiro.hasPermission>-->

                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="table-responsive ">
                                    <table class="table table-centerbody table-striped table-condensed text-nowrap" id="editable-sample">
                                        <thead>
                                            <tr>
                                                <th>编号</th>
                                                <th>标题</th>
                                                <th>内容</th>
                                                <th>发布人</th>
                                                <th>发布时间</th>
                                                <th>当前状态</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                            <#if model?? && model.list?? && (model.list?size > 0) >
                                <#list model.list as info>
                                            <tr>
                                                <td>${info.id}</td>
                                                <td>${info.title}</td>
                                                <td>${info.content}</td>
                                                <td>${info.operator}</td>
                                                <td>${info.createTime}</td>
                                                <td>
                                                    <#if info.flag=='Y'>
                                                        显示
                                                    <#else>
                                                        隐藏
                                                    </#if>
                                                </td>
                                                <td>
                                                    <a  href="javascript:updateFlag('${info.id}','${info.flag}');">显示/隐藏</a>
                                                </td>
                                            </tr>

                                </#list>
                            </#if>
                                        </tbody>
                                    </table>
                                </div>
                              ${page}
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="addHomeMessage" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="addroleLabel">添加后台公告</h4>
                        </div>
                        <div class="modal-body">
                            <form id="home" enctype="multipart/form-data" action="/message/insertMessage?port=2"  method="post">
                                <div class="form-group">
                                    <label  class="control-label">标题:</label>
                                    <input type="text" style="height:30px;width: 98%" class="form-control" name="title" id="title" placeholder="请输入标题"/>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label">公告内容</label>

                                    <textarea style="height:60px;width: 98%" class="form-control" name="content" id="content" placeholder="请输入内容"></textarea>
                                </div>
                                <div style="font-size: 14px;font-weight: bold;">
                                    是否置顶:<input id="Y" type="radio"  value="1" style="margin: 0px 5px 5px 10px;" checked="checked" name="isTop" />是<input id="N" style="margin: 0px 5px 5px 10px;" value="0" type="radio"  name="isTop"/>否
                                </div>

                                <div class="modal-footer" style="background-color: #ffffff">
                                    <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                                    <button type="submit"  class="btn btn-primary">发布</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

            <div class="footer">
                <div class="pull-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2015
                </div>
            </div>
        </div>


    </div>


    <script src="${ctx}/static/js/jquery-2.1.1.js"></script>
    <script src="${ctx}/static/js/bootstrap.js"></script>
    <script src="${ctx}/static/js/wuling.js"></script>
    <script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>
    <script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script>
    <script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script>
    <script src="${ctx}/static/js/My97DatePicker/WdatePicker.js" ></script>
    <script src="${ctx}/static/js/layer/layer.js" ></script>
    <script type="text/javascript" src="${ctx}/static/js/jquery.common-1.0.0.js" charset="UTF-8"></script>


    <script>
        $(function(){
            var load;
            $("#home").ajaxForm({
                success:function (result){
                    layer.close(load);
                    if(result && result.status != 200){
                        return layer.msg(result.message,function(){}),!1;
                    }else{
                        layer.msg(result.message);
                        setTimeout(function(){
                            //3秒后刷新
                            layer.close();
                            location.reload();
                        },2000);
                    }
                },
                beforeSubmit:function(){
                    //判断参数
                    if($.trim($("#title").val()) == ''){
                        layer.msg('请输入标题',function(){});
                        $("#title").parent().removeClass('has-success').addClass('has-error');
                        return !1;
                    }else{
                        $("#title").parent().removeClass('has-error').addClass('has-success');
                    }
                    if($.trim($("#content").val()) == ''){
                        layer.msg('请输入内容',function(){});
                        $("#content").parent().removeClass('has-success').addClass('has-error');
                        return !1;
                    }else{
                        $("#content").parent().removeClass('has-error').addClass('has-success');
                    }
                    load = layer.load('正在提交！！！');
                },
                dataType:"json",
                clearForm:false
            });

        });

        function updateFlag(id,flag){

            $.post('/message/updateMessageFlag', {id:id,flag:flag}, function (result) {
                if (result && result.status == 200) {
                    layer.msg(result.message);
                    setTimeout(function () {
                        /* $("#close").click();*/
                        location.reload();
                    }, 3000);
                }

                return layer.msg(result.message, so.default), !1;


            }, 'json');


        }




    </script>
</body>
</html>
