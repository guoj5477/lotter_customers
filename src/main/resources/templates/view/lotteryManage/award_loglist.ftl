<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">
<script>
    function resetbtns(btn){
        var resetArr = $(btn).parents("form").find(":input");
        for(var i=0; i<resetArr.length; i++){
            if(i>0){
                resetArr.eq(i).val("");
            }


        }
    }
</script>
</head>

<body class="fixed-sidebar">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
            <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4" style="width: 24%">
                    <ol class="breadcrumb">
                        <li style="color: #0d8ddb;font-weight: bold">
                            当前位置:
                        </li>
                        <li>
                            <a href="${ctx}/static/index.html">统计报表</a>
                        </li>
                        <li class="active">
                            中奖日志
                        </li>

                    </ol>
                </div>

                <div id="main-header" class="col-sm-4" style="font-size: 14px;margin-top: 10px;margin-left: 0px;color: #0e9aef">
                    <p   class="main-timer"><b>${user.attributes.realname }</b><span id="show-timer" ></span></p>
                </div>

                <div  class="col-sm-4" style="margin-top: 10px;width: 20% ">
                    <a href="${ctx}/logout"><i class="fa fa-sign-out"></i>退出登录</a>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="ibox-content m-b-sm border-bottom">
                    <div class="row">
                        <form method="get" action="/lottery/queryLotteryRec" id="formId" class="form-inline">
                            <div clss="well">
                                <div class="form-group">
                                    彩票名称:
                                        <select class="form-control"  name="lotteryId" id="lotteryId"  style="width: 110px;height: 30px;border: 1px solid #ccc;border-radius: 4px">
                                            <option value="10" id="canada28">加拿大28</option>
                                            <option value="11"  id="bj28">北京28</option>
                                        </select>
                                        期号:<input type="text" class="form-control" style="width: 80px;height: 30px;border: 1px solid #ccc;border-radius: 4px"  name="lotteryQishu"  value="${orderModel.lotteryQishu!}" placeholder="输入期号">

                                        开奖日期:

                                        <input name="startDate" class="Wdate" type="text" id="d4311" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01 00:00:00\'}'})"
                                               style="width: 140px;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择开始日期" value="${orderModel.startDate!}"> --
                                        <input   id="d4312" class="Wdate" type="text" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2020-10-01 00:00:00'})"
                                                 name="endDate"   style="width: 140px;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择结束日期" value="${orderModel.endDate!}">
                                    <button type="submit" class="btn btn-primary">查询</button>
                                    <button type="button" onclick="javascript:resetbtns(this)" class="btn  btn-danger">清空</button>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="table-responsive ">
                                    <table class="table table-centerbody table-striped table-condensed text-nowrap" id="editable-sample">
                                        <thead>
                                            <tr>
                                                <th>彩票名称</th>
                                                <th>场次/期号</th>
                                                <th>中奖号码</th>
                                                <th>下注人数</th>
                                                <th>总奖金金额</th>
                                                <th>开奖时间</th>
                                                <th>中奖人数</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                            <#if model?? && model.list?? && (model.list?size > 0) >
                                <#list model.list as info>
                                            <tr>
                                                <td>
                                                    <#if info.lotteryId==10>
                                                        加拿大28
                                                    <#elseif info.lotteryId==11>
                                                        北京28
                                                    </#if>
                                                </td>

                                                <td>${info.lotteryQishu}</td>
                                                <td>${info.lotteryCodes}</td>
                                                <td>${info.lotteryCount}</td>
                                                <td>${info.betMoney}</td>
                                                <td>${info.openTime?string("yyyy-MM-dd HH:mm:ss")}
                                                </td>
                                                <td>${info.awardCount}</td>
                                            </tr>

                                </#list>
                            </#if>
                                        </tbody>
                                    </table>
                                </div>
                            ${pageHtml}
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="footer">
                <div class="pull-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2015
                </div>
            </div>
        </div>


    </div>


    <script src="${ctx}/static/js/jquery-2.1.1.js"></script>
    <script src="${ctx}/static/js/bootstrap.js"></script>
    <script src="${ctx}/static/js/wuling.js"></script>
    <script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>
    <script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script>
    <script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script>
    <script src="${ctx}/static/js/My97DatePicker/WdatePicker.js" ></script>
    <script type="text/javascript" src="${ctx}/static/js/jquery.common-1.0.0.js" charset="UTF-8"></script>
    <script src="../css/layui/layui.js"></script>
    <script>
        $(document).ready(function(){
            var lotteryId = "${orderModel.lotteryId}";
            if(lotteryId == "10"){
                $("#canada28").attr("selected",true);
            }else if(lotteryId =="11") {
                $("#bj28").attr("selected",true);
            }
        });
    </script>
</body>
</html>
