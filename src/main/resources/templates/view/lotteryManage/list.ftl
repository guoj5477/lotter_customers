<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/css/default.theme.css" media="screen"></link>
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">
    <link href="${ctx}/static/css/main.css" rel="stylesheet">



    <style>
        a {
            cursor:pointer;
        }
        .modal-header {
            padding: 2px 16px;
            border-bottom: 1px solid #eee;
        }
        .modal-footer {
            border-top:0px solid #e5e5e5;
        }

        .col-md-10 {
            width: 100%;
        }
        .glyphicon-remove:before {
            content: "";
        }

        .form-control {
            width: 40%;
        }
        .form-group {
            margin-bottom: 2px;
        }
        .title {
            background: #f9f9f9 url(../img/bg-content-title.png);
            border-color: #f8fbfb !important;
            color: #111113;
        }

        .notification {
            position: relative;
            margin: 0 0 10px 0;
            padding: 0;
            border: 1px solid;
            background-position: 10px 11px !important;
            background-repeat: no-repeat !important;
            font-size: 13px;
            width: 21.8%;
        }
    </style>
</head>

<body class="fixed-sidebar">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
            <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
              <#--  <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                </div>-->

                <div class="col-sm-4" style="width: 20%">
                    <ol class="breadcrumb">
                        <li style="color: #0d8ddb;font-weight: bold">
                            当前位置:
                        </li>
                        <li>
                            <a href="${ctx}/static/index.html">开奖管理</a>
                        </li>
                        <li class="active">
                            <#if model?? && model.list?? && (model.list?size > 0) >
                                <#list model.list as info>
                                    <#if info_index = 0>
                                        <#if info.lotteryId = '10'>
                                            加拿大28
                                        </#if>
                                        <#if info.lotteryId = '11'>
                                            北京28
                                        </#if>
                                    </#if>
                                </#list>
                            </#if>
                        </li>

                    </ol>
                </div>

                <div id="main-header" class="col-sm-4" style="font-size: 14px;margin-top: 5px;margin-left: 0px">
                    <p   class="main-timer"><b>${user.attributes.realname }</b><span id="show-timer" ></span></p>
                </div>

                <div  class="col-sm-4" style="margin-top: 10px;width: 20% ">
                    <a href="${ctx}/logout"><i class="fa fa-sign-out"></i>退出登录</a>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="ibox-content m-b-sm border-bottom">
                    <div class="row">
                          <#--  <div class="col-md-5">
                                <div class="input-group" >
                                    <input type="text" class="input-sm form-control" name="end" value="" placeholder="输入账号搜索">
                                    <span class="input-group-btn"><button type="button" class="btn btn-sm btn-primary "> 查询</button></span>
                                </div>
                            </div>-->

                              <form method="get" action="${queryUrl}" id="formId" class="form-inline">

                              <div clss="well">
                                  <div class="form-group" style="margin-left: 15px">
                                      <#if model.list?exists>
                                          <input type="text"  name="lotteryId"   value="${lotteryId}" style="display: none">
                                      </#if>
                                          彩票日期:<input type="text" name="lotteryDate"  id="lotteryDate"  value="${lottDate}" class="Wdate"  onclick="WdatePicker({maxDate:'%y-%M-%d'})" style="width: 185px;height: 30px;border: 1px solid #ccc;border-radius: 4px"  placeholder="请选择开奖日期">
                                      期号:<input type="text" name="lotteryOrder"  id="lotteryOrder"   value="${lotteryOrder}" style="width: 120px;height: 30px;border: 1px solid #ccc;border-radius: 4px" style="width: 100px;height: 35px"  placeholder="请输入期号">

                                      <button type="submit" class="btn btn-primary">查询</button>
                                      <button type="button" onclick="resetBtns()"  id="reset" class="btn  btn-danger">清空</button>

                                  </div>

                              </div>
                              </form>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="table-responsive ">
                                    <table class="table table-centerbody table-striped table-condensed text-nowrap" id="editable-sample">
                                        <thead>
                                            <tr>
                                                <th>编号</th>
                                                <th>彩票名称</th>
                                                <th>彩票日期</th>
                                                <th>期数</th>
                                                <th>开奖结果</th>
                                                <th>开奖时间</th>
                                                <th>开奖状态</th>
                                                <th>投注额</th>
                                                <th>中奖金额</th>
                                                <th class="text-right">操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                            <#if model?? && model.list?? && (model.list?size > 0) >
                                <#list model.list as info>
                                            <tr>
                                                <td>${info.lotteryId}</td>
                                                <td>
                                                    <#if info.lotteryId = '10'>
                                                        加拿大28
                                                    </#if>
                                                    <#if info.lotteryId = '11'>
                                                        北京28
                                                    </#if>
                                                </td>
                                                <td>${lottDate}</td>
                                                <td>${info.lotteryOrder?replace(",","" )}</td>
                                                <td>${info.lotteryCode}</td>
                                                <td>${info.lotteryTime}</td>
                                                <td>${info.status}</td>
                                                <td>${info.betMoney}</td>
                                                <td>${info.winMoney}</td>
                                                <td class="text-right text-nowrap">
                                                    <div class="btn-group ">
                                                        <button  onclick="updateLotteryCode('${info.lotteryId}','${info.lotteryOrder}','${lottDate}','${info.lotteryTime}','${info.lotteryCode}','${info.status}')" class="btn btn-white btn-sm edit" >重新开奖</button>
                                                    </div>
                                                    <div class="btn-group ">
                                                        <button  onclick="addLotteryCode('${info.lotteryId}','${info.lotteryOrder}','${lottDate}','${info.lotteryTime}','${info.lotteryCode}','${info.status}')" class="btn btn-white btn-sm edit"  ><i class="fa fa-pencil"></i>  手动开奖</button>
                                                    </div>
                                                    <div class="btn-group ">
                                                        <button  onclick="cancelSettlement('${info.lotteryId}','${info.lotteryOrder}','${info.lotteryCode}','${info.status}')" class="btn btn-white btn-sm edit" ><i class="fa fa-pencil"></i>  取消开奖</button>
                                                    </div>
                                                </td>
                                            </tr>

                                </#list>
                            </#if>
                                        </tbody>
                                    </table>
                                </div>
                            ${pageHtml}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-----内容结束----->

            <!----版权信息----->
            <div class="footer">
                <div class="pull-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2015
                </div>
            </div>
        </div>
        <!---右侧内容区结束----->

    </div>

    <div class="modal fade" id="addLotteryCode" tabindex="-1" role="dialog" aria-labelledby="addroleLabel" style="margin-top: 10%">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="addroleLabel">手动开奖</h4>
                </div>
                <div class="modal-body">
                    <form  enctype="multipart/form-data" id="lottFrom">
                        <div class="form-group">
                            <label  class="control-label">彩票id:</label>
                            <input type="text" style="height:28px;width: 40%" class="form-control" name="lotteryId" id="lid_add"  readonly="readonly"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">场次:</label>
                            <input type="text" style="height:28px;width: 40%" class="form-control" name="lotteryOrder" id="lotteryOrder_add" readonly="readonly"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">彩票日期:</label>
                            <input type="text" style="height:28px;width: 40%" class="form-control" name="lotteryDate" id="lotteryDate_add" readonly="readonly"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">开奖时间:</label>
                            <input type="text" style="height:28px;width: 40%" class="form-control" name="lotteryTime" id="lotteryTime_add" readonly="readonly"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">开奖结果:</label>
                            <input type="text" style="height:28px;width: 40%"  class="form-control" name="lotteryCode" id="lotteryCode_add"  placeholder="用,分开，例如:2,3,4(注意区分输入法中英文)"/>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                            <button type="button"  class="btn btn-primary" onclick="submitForm();">提交</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>



    <div class="modal fade" id="updateLotteryCode" tabindex="-1" role="dialog" aria-labelledby="updateLotteryCodeLabel" style="margin-top: 10%">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="updateLotteryCodeLabel">修改开奖</h4>
                </div>
                <div class="modal-body">
                    <form  enctype="multipart/form-data" id="lotteryForm">
                        <div class="form-group">
                            <label  class="control-label">彩票id:</label>
                            <input type="text" style="height:28px;width: 40%" class="form-control" name="lotteryId" id="lottId_update"  readonly="readonly"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">期号:</label>
                            <input type="text" style="height:28px;width: 40%" class="form-control" name="lotteryOrder" id="lottOrder_update" readonly="readonly"/>
                        </div>
                        <#--<div class="form-group">
                            <label  class="control-label">期号:</label>
                            <input type="text" style="height:28px;width: 40%" class="form-control" name="pid" id="action_no" readonly="readonly"/>
                        </div>-->
                        <div class="form-group">
                            <label  class="control-label">彩票日期:</label>
                            <input type="text" style="height:28px;width: 40%" class="form-control" name="lotteryDate" id="loDate_update" readonly="readonly"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">开奖时间:</label>
                            <input type="text" style="height:28px;width: 40%" class="form-control" name="lotteryTime" id="lottTime_update" readonly="readonly"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">开奖结果:</label>
                            <input type="text" style="height:28px;width: 40%"  class="form-control" name="lotteryCode" id="lottCode_update"  placeholder="用,分开，例如:2,3,4(注意区分输入法中英文)"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">备注:</label>
                            <input type="text" style="height:28px;width: 40%"  class="form-control" name="remark" id="lottRemark_update" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                            <button type="button"  class="btn btn-primary" onclick="submitFormByUpdateLottCode();">提交</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


    <!-- 全局 scripts -->
    <script src="${ctx}/static/js/jquery-2.1.1.js"></script>
    <script src="${ctx}/static/js/bootstrap.js"></script>
    <script src="${ctx}/static/js/wuling.js"></script>
    <script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>

    <script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <!-- 插件 scripts -->
    <script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script><!---顶部弹出提示--->
    <script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>  <!---表单验证--->
    <script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script> <!---validate 自定义方法--->
    <script type="text/javascript" src="${ctx}/static/js/jquery.common-1.0.0.js" charset="UTF-8"></script>
    <script src="${ctx}/static/js/My97DatePicker/WdatePicker.js" ></script>
    <script src="${ctx}/static/js/layer/layer.js" ></script>
    <script>
        laydate.render({
        elem: '#lottDate'
        ,type: 'date'
        ,theme: '#393D49'
    });

    $(document).ready(function(){

 /*      if(){
            $("#Y").attr("selected",true);
        }else if() {
            $("#N").attr("selected",true);
        }*/

        $("#reset").click(function(){
            var resetArr = $(this).parents("form").find(":input");
            for(var i=1; i<resetArr.length; i++){
                resetArr.eq(i).val("");
            }

            return false;　　//一定要return false，阻止reset按钮功能，不然值又会变成aa
        });



    });
    function resetBtns(){
        $("#lotteryDate").val("");
        $("#lotteryOrder").val("");
    }
    function submitForm(){
        $.ajax({
            type: "POST",
            url: "/lottery/updateLotteryInfo",
            data: $('#lottFrom').serialize(),
            success: function (data) {
                if (data.success) {
                    layer.msg(data.message);
                    layer.close();
                    setTimeout(function(){
                        //3秒后刷新
                        location.reload();
                    },3000);
                } else {
                    layer.msg(data.message);
                    layer.close();
                    setTimeout(function(){
                        //3秒后刷新
                        location.reload();
                    },3000);
                }
            },
            error: function(data) {
                layer.msg(data.message);
                layer.close();
            }
        });

    }


    function submitFormByUpdateLottCode() {

        $.ajax({
            type: "POST",
            url: "/lottery/updateLottCode",
            data: $('#lotteryForm').serialize(),
            success: function (data) {
                if (data.success) {
                    layer.msg(data.message);
                    layer.close();
                    setTimeout(function(){
                        //3秒后刷新
                        location.reload();
                    },3000);
                } else {
                    layer.msg(data.message);
                    layer.close();
                    setTimeout(function(){
                        //3秒后刷新
                        location.reload();
                    },3000);
                }
            },
            error: function(data) {
                layer.msg(data.message);
                layer.close();
            }
        });


    }

    function updateLotteryTime(object,lotteryOrder,lotteryDate,lotteryId){
        //获取当前行隐藏输入框中的值
        var lotteryTime=$(object).parents("tr").find(".lotteryTime").val();
        $.ajax({
            type: 'post',
            url: '/dateManage/updateLotteryTime',
            data:{lotteryOrder:lotteryOrder,lotteryDate:lotteryDate,lotteryId:lotteryId,lotteryTime:lotteryTime},
            success:function(response) {

                if (response.status==200) {
                    layer.msg(response.message);

                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                } else {
                    layer.msg(response.message);

                }
            },
            error:function(response){
                alert("---error:"+response);
                layer.msg(response.message)
            }
        });

    }

    function addLotteryCode(lotteryId,lotteryOrder,lotteryDate,lotteryTime,lotteryCode,status){
        if(status == '取消结算'){
            layer.msg("本期彩票已经取消结算!");
            return false;
        }
        if(lotteryCode!=null&&lotteryCode!=""){
            layer.msg("本期彩票已经开奖!");
            return false;
        }
        $("#lotteryDate_add").val(lotteryDate);
        $("#lid_add").val(lotteryId);
        $("#lotteryOrder_add").val(lotteryOrder.replace(/,/g, ""));
        $("#lotteryTime_add").val(lotteryTime);

        $('#addLotteryCode').modal();

    }
    function updateLotteryCode(lotteryId,lotteryOrder,lotteryDate,lotteryTime,lotteryCode,status){
        if(status == '取消结算'){
            layer.msg("本期彩票已经取消结算!");
            return false;
        }
        if(lotteryCode==null||lotteryCode==""){
            layer.msg("本期彩票未开奖!");
            return false;
        }

        $("#loDate_update").val(lotteryDate);
        $("#lottId_update").val(lotteryId);
        $("#lottOrder_update").val(lotteryOrder.replace(/,/g, ""));
        $("#lottTime_update").val(lotteryTime);
        $("#lottCode_update").val(lotteryCode);
       // $("#action_no").val(pid)

        $('#updateLotteryCode').modal();

    }

    function cancelSettlement(lotteryId,lotteryOrder,lotteryCode,status){
        if(status == '取消结算'){
            layer.msg("本期彩票已经取消结算!");
            return false;
        }
        if(lotteryCode==null||lotteryCode==""){
            layer.msg("本期彩票未开奖!");
            return false;
        }
        var index = layer.confirm("确定取消本期结算？",function(){
            var load = layer.load();
            $.post('/lottery/cancelSettlement',{lotteryId:lotteryId,lotteryOrder:lotteryOrder.replace(/,/g, ""),lotteryCode:lotteryCode},function(result){
                layer.close(load);
                if(result && result.status == 200){

                    layer.msg(result.message);
                    setTimeout(function(){
                        //3秒后刷新
                        location.reload();
                    },3000);
                }else{
                    return layer.msg(result.message,so.default),!0;
                }
            },'json');
            layer.close(index);
        });

   }


    </script>
</body>
</html>
