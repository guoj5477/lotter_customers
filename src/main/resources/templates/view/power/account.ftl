<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理系统</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${ctx}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${ctx}/static/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${ctx}/static/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/css/style.css" rel="stylesheet">


    <style>


    </style>
</head>

<body class="fixed-sidebar">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="leftnav">
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row ">
            <nav class="navbar navbar-fixed-top" role="navigation" id="topnav"></nav>
            </div>
            <div class="row  border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <ol class="breadcrumb">
                        <li>
                            <a href="${ctx}/static/index.html">游戏设置</a>
                        </li>
                        <li class="active">
                            IP白名单
                        </li>

                    </ol>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="ibox-content m-b-sm border-bottom">
                    <form method="post" action="" id="formId" class="form-inline">
                        <div clss="well">
                            <div class="form-group">
                            <#-- <shiro.hasPermission name="admin">-->
                                <a class="btn btn-success" onclick="$('#addOperator').modal();">增加</a>&nbsp;&nbsp;
                                <a class="btn btn-success" href="/operator/queryOperatorList">刷新</a>
                            <#--   </shiro.hasPermission>-->
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="table-responsive ">
                                    <table class="table table-centerbody table-striped table-condensed text-nowrap" id="editable-sample">
                                        <thead>
                                            <tr>
                                                <th>管理员名称</th>
                                                <th>角色</th>
                                                <th>最后登录时间</th>
                                                <th>最后登录地址</th>
                                                <th>在线状态</th>
                                                <th>是否锁定</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                            <#if model??  && (model?size > 0) >
                                <#list model as operator>
                                            <tr>
                                                <td>${operator.account}</td>
                                                <td>${operator.role}</td>
                                                <td>${operator.lastLoginTime}</td>
                                                <td>${operator.lastLoginIp}</td>

                                                <td style="color: #0e90d2">
                                                    <#if operator.onlineStatus==1>
                                                        在线
                                                    <#else>
                                                        离线
                                                    </#if>
                                                </td>

                                               <td>
                                                <#if operator.status==1>
                                                    正常
                                                <#elseif operator.status==0>
                                                    已禁用
                                                </#if>
                                                </td>


                                                <td>
                                                   <#-- <shiro:hasAnyRoles name="admin,boos">-->
                                                        <i class="glyphicon glyphicon-remove"></i><a href="javascript:deleteOperator(${operator.operatorId});">删除</a>
                                                  <#--  </shiro:hasAnyRoles>&nbsp;&nbsp;-->

                                                 <#--   <shiro:hasAnyRoles name="admin,boos">-->
                                                        <#if operator.status==1>
                                                            <i class="glyphicon glyphicon-remove"></i><a  href="javascript:operatorLocked(${operator.operatorId},${operator.status});">禁用</a>
                                                        <#elseif operator.status==0>
                                                            <i class="glyphicon glyphicon-remove"></i><a  href="javascript:operatorLocked(${operator.operatorId},${operator.status});">启用</a>
                                                        </#if>

                                                  <#--  </shiro:hasAnyRoles>&nbsp;&nbsp;-->

                                                    <#if operator.onlineStatus==1>
                                                        <i class="glyphicon glyphicon-remove"></i><a  href="javascript:kickOutOperator(${operator.operatorId},${operator.onlineStatus});">踢出</a>
                                                    </#if>

                                                </td>
                                            </tr>

                                </#list>
                            </#if>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="addOperator" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="addroleLabel">添加管理员</h4>
                        </div>
                        <div class="modal-body">
                            <form id="operator" enctype="multipart/form-data" action="/operator/addOperator"  method="post">
                                <div class="form-group">
                                    <label  class="control-label">管理员名称:</label>
                                    <input type="text" style="height:30px" class="form-control" name="account" id="account" placeholder="请输入管理员名称"/>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label">登录密码:</label>
                                    <input type="text" style="height:30px" class="form-control" name="password" id="password" placeholder="请输入登陆密码"/>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label">确认密码:</label>
                                    <input type="text" style="height:30px" class="form-control" name="rePassword" id="rePassword" placeholder="请再次输入密码"/>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label">选择角色:</label>
                                    <select name="role" id="role">
                                        <option>请选择角色</option>
                                        <option value="1">管理员</option>
                                        <option value="2">客服</option>
                                    </select>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                                    <button type="submit"  class="btn btn-primary">提交</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="pull-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2015
                </div>
            </div>
        </div>


    </div>


    <script src="${ctx}/static/js/jquery-2.1.1.js"></script>
    <script src="${ctx}/static/js/bootstrap.js"></script>
    <script src="${ctx}/static/js/wuling.js"></script>
    <script src="${ctx}/static/js/plugins/pace/pace.min.js"></script>
    <script src="${ctx}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${ctx}/static/js/plugins/toastr/toastr.min.js" async></script>
    <script src="${ctx}/static/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="${ctx}/static/js/plugins/validate/validate-cn.js" ></script>
    <script src="${ctx}/static/js/My97DatePicker/WdatePicker.js" ></script>
    <script src="${ctx}/static/js/layer/layer.js" ></script>

    <script>
        $(function(){
            var load;
            $("#operator").ajaxForm({
                success:function (result){
                    layer.close(load);
                    if(result && result.status != 200){
                        return layer.msg(result.message,function(){}),!1;
                    }else{
                        layer.msg(result.message);
                        $("form :password").val('');

                        setTimeout(function(){
                            //3秒后刷新
                            layer.close();
                            location.reload();
                        },2000);
                    }
                },
                beforeSubmit:function(){
                    //判断参数
                    if($.trim($("#account").val()) == ''){
                        layer.msg('请输入管理员名称',function(){});
                        $("#account").parent().removeClass('has-success').addClass('has-error');
                        return !1;
                    }else{
                        $("#account").parent().removeClass('has-error').addClass('has-success');
                    }
                    if($.trim($("#password").val()) == ''){
                        layer.msg('请输入密码',function(){});
                        $("#password").parent().removeClass('has-success').addClass('has-error');
                        return !1;
                    }else{
                        $("#password").parent().removeClass('has-error').addClass('has-success');
                    }

                    if($.trim($("#rePassword").val()) == ''){
                        layer.msg('请再次输入密码',function(){});
                        $("#rePassword").parent().removeClass('has-success').addClass('has-error');
                        return !1;
                    }else{
                        $("#rePassword").parent().removeClass('has-error').addClass('has-success');
                    }
                    if($("#rePassword").val() != $("#password").val()){
                        return layer.msg('2次密码输入不一致。',function(){}),!1;
                    }
                    if($.trim($("#role").val()) == ''){
                        layer.msg('请选择角色',function(){});
                        $("#role").parent().removeClass('has-success').addClass('has-error');
                        return !1;
                    }else{
                        $("#role").parent().removeClass('has-error').addClass('has-success');
                    }

                    load = layer.load('正在提交！！！');
                },
                dataType:"json",
                clearForm:false
            });

        });

        function deleteOperator(operatorId){
            var index = layer.confirm("确定删除这个管理员？",function(){
                var load = layer.load();
                $.post('/operator/deleteOperator',{operatorId:operatorId},function(result){
                    layer.close(load);
                    if(result && result.status == 200){

                        layer.msg(result.message);
                        setTimeout(function(){
                            //3秒后刷新
                            location.reload();
                        },3000);
                    }else{
                        return layer.msg(result.message,so.default),!0;
                    }
                },'json');
                layer.close(index);
            });
        }

        function operatorLocked(operatorId,status) {



            $.post('/operator/operatorLocked', {operatorId:operatorId,status:status}, function (result) {


                if (result && result.status == 200) {
                    layer.msg(result.message);
                    setTimeout(function () {
                        /* $("#close").click();*/
                        location.reload();
                    }, 3000);
                }

                return layer.msg(result.message, so.default), !1;


            }, 'json');


        }

        //踢出管理员
        function kickOutOperator(operatorId,status) {



            $.post('/operator/changeSessionStatus', {operatorId:operatorId,status:status}, function (result) {
                if (result && result.status == 200) {

                    layer.msg(result.message);
                    setTimeout(function () {
                        /* $("#close").click();*/
                        location.reload();
                    }, 3000);
                }

                return layer.msg(result.message, so.default), !1;


            }, 'json');


        }

    </script>
</body>
</html>
